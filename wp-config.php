<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_jobst' );

/** MySQL database username */
define( 'DB_USER', 'homestead' );

/** MySQL database password */
define( 'DB_PASSWORD', 'secret' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '<TFSUQ%7hXCgk:8,08RTfhtAbV=K;f{qjdO.jCnrQo@T5s 2`DQzBygr2MZEe)4s' );
define( 'SECURE_AUTH_KEY',  'E~;1(i0I:7H#I~g1C@n?j}SY_}e?gAKaLp 1SfHV7f/{2{uVN&kXd10SB<l+kd]m' );
define( 'LOGGED_IN_KEY',    'yH*($WMp1`9H|QUe$rxVAR> eMk61hZI!:Mb0OAZ3XvhHA9?Lw+mVt,@~tc$W$_Q' );
define( 'NONCE_KEY',        'aWw6b>HEM`}bZAw<--!(PB-x@rH&}%u2s.`%hZtP/01=b=c-FMS1EoQCa7Fv+J38' );
define( 'AUTH_SALT',        'Gk@NQ4=zEkcP%B[K1rm(KW!;-d)bA~]Ny8w3HEM>Lc#};&>U$&nLEZ`be:*)kfIt' );
define( 'SECURE_AUTH_SALT', 'nx.,t>C=T>+SPwGU(K)#H-P?DyDTt%7whoLbD`ffe-Tr;IJpba[.3%z0TXZ|]^<f' );
define( 'LOGGED_IN_SALT',   'b$5$-A}vX>T<)?*c,yR/<H^,+#{(J#%h D,dGE;/#>!oRe+:xmuM7-q}AQb7QqP7' );
define( 'NONCE_SALT',       'lGGI}h+S4*NoEs5)X88f<a9U@d)0)JBpBv>^Xr+GuGv4/hTO~D_ )3O;pSwIYQg]' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'bj_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define( 'SCRIPT_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
