<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>
	<div id="primary" class="content-area tax-pv_category">
		<main id="main" class="site-main">

			<?php
			printf( '<h1 class="entry-title">%s</h1>', __( '¿Dónde comprar?', 'baumchild' ) );

			$child_terms       = array();
			$id_destacado      = array( 157 ); // ID term "Destacado"
			$current_term_id   = get_queried_object()->term_id;
			$current_term_name = get_queried_object()->name;
			$terms             = get_terms( array(
					'taxonomy'   => 'pv_category',
					'hide_empty' => false,
					'exclude'    => $id_destacado
			) );

			$zonas_array = get_posts(
					array(
							'posts_per_page' => - 1,
							'post_type'      => 'punto-venta',
							'post_status'    => 'publish',
							'tax_query'      => array(
									array(
											'taxonomy' => 'pv_category',
											'field'    => 'id',
											'terms'    => $current_term_id,
									)
							)
					)
			);

			if ( $terms ) {
				// Include select for zone
				include_once 'template-parts/cpt/section-select-zone.php';

				// Include featured zone
				include_once 'template-parts/cpt/section-featured-zone.php';

				// Include gallery
				include_once 'template-parts/cpt/section-gallery.php';

				// Include filters by zone
				include_once 'template-parts/cpt/section-filterable-zones.php';
			} else {
				get_template_part( 'template-parts/content/content', 'none' );
			}
			?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
