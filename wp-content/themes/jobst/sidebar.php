<?php
/**
 * The template for the sidebar containing the main widget area
 */
?>

<?php if (is_active_sidebar('sidebar-principal')) : ?>
    <aside id="secondary" class="sidebar widget-area" role="complementary">
        <button class="filter-toggle-button"  id="toggle-filtros">
            <?php echo __('Filtros', 'baumchild') ?> <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/theme/filter-results-button.svg" alt="Filtros">
        </button>

        <?php // echo do_shortcode('[woof_products]'); ?>
        <?php dynamic_sidebar('sidebar-principal'); ?>
    </aside><!-- .sidebar .widget-area -->
<?php endif; ?>
