jQuery(function($) {
  var config = {
    /* Carrusel de Categorias del Menu Principal */
    slider_column_carrusel: {
      object_class: ".column-carousel > ul.mega-sub-menu",
      instancia: null,
      targetPrev: ".btn-prev",
      targetNext: ".btn-next",
      args: {
        item: 5,
        auto: false,
        enableDrag: true,
        enableTouch: true,
        controls: false,
        slideMargin: 0,
        freeMove: true,
        loop: false,
        pager: false,
        autoWidth: false,
        pauseOnHover: true,
        prevHtml: '<i class="fas fa-chevron-left"></i>',
        nextHtml: '<i class="fas fa-chevron-right"></i>',
        slideMove: 1,
		adaptiveHeight: true,
        responsive: [
          {
            breakpoint: 992,
            settings: {
              item: 4
            }
          },
          {
            breakpoint: 860,
            settings: {
              item: 3
            }
          }
        ],
        onSliderLoad: function(el) {
          carouselWrapper(".column-carousel .lSSlideOuter");
          $(".column-carousel").removeClass("cS-hidden");
          console.log("%c onSliderLoad ", "background: yellow; color: black"); // ----- Console log
          $(".column-carousel")
            .closest(".mega-menu-item")
            .on("open_panel", function() {
              $(window).trigger("resize");
              console.log(
                "%c trigger resize del Carrusel ",
                "background: green; color: white"
              ); // ----- Console log
            });
        },
        onBeforeStart: function(el) {
          clonarCategorias();
          console.log("%c onBeforeStart ", "background: #000; color: yellow"); // ----- Console log
        }
      }
    },
    /* Carrusel de Marcas del Home */
    carrusel_marcas: {
      object_class: ".wpb_image_grid_ul",
      instancia: null,
      args: {
        item: 5,
        prevHtml: '<i class="fa fa-angle-left"></i>',
        nextHtml: '<i class="fa fa-angle-right"></i>',
        enableDrag: true,
        enableTouch: true,
        pager: false,
        loop: true,
        responsive: [
          {
            breakpoint: 992,
            settings: {
              item: 3
            }
          },
          {
            breakpoint: 768,
            settings: {
              item: 2
            }
          },
          {
            breakpoint: 481,
            settings: {
              item: 1
            }
          }
        ],
        onSliderLoad: function(el) {
          if (
            $(config.carrusel_marcas.object_class).closest(".cS-hidden")
              .length > 0
          ) {
            $(config.carrusel_marcas.object_class)
              .closest(".cS-hidden")
              .removeClass("cS-hidden");
          }
          console.log(
            "%c onSliderLoad carrusel_marcas",
            "background: cyan; color: black"
          ); // ----- Console log
        }
      }
    },
    /* Carrusel deHistorias del Home */
    carrusel_historias: {
      object_class: "#post-grid-about .vc_pageable-slide-wrapper",
      instancia: null,
      args: {
        item: 2.7,
        //auto: true,
        //speed: 3500,
        //slideMove: 1,
        enableDrag: true,
        enableTouch: true,
        prevHtml: '<i class="fa fa-angle-left"></i>',
        nextHtml: '<i class="fa fa-angle-right"></i>',
        pager: false,
        loop: false,
        responsive: [
          {
            breakpoint: 992,
            settings: {
              item: 2.7
            }
          },
          {
            breakpoint: 768,
            settings: {
              item: 2
            }
          },
          {
            breakpoint: 481,
            settings: {
              item: 1.2
            }
          }
        ],
        onSliderLoad: function(el) {
          /*
                     if ($(config.carrusel_historias.object_class).closest('.cS-hidden').length > 0) {
                     $(config.carrusel_historias.object_class).closest('.cS-hidden').removeClass('cS-hidden');
                     }
                     */
          //console.log('%c onSliderLoad de Mabel => cS-hidden', 'background: cyan; color: black'); // ----- Console log
        }
      }
	}
  };

  $.each(config, function(index, element) {
    if (element.object_class != null && $(element.object_class).length > 0) {
      element.instancia = $(element.object_class).lightSlider(element.args);
      if (
        typeof element.targetPrev != "undefined" &&
        element.targetPrev != null &&
        $(element.targetPrev).length > 0
      ) {
        $(document).on("click", element.targetPrev, function() {
          element.instancia.goToPrevSlide();
        });
      }

      if (
        typeof element.targetNext != "undefined" &&
        element.targetNext != null &&
        $(element.targetNext).length > 0
      ) {
        $(document).on("click", element.targetNext, function() {
          element.instancia.goToNextSlide();
        });
      }
    }
  });

  function carouselWrapper(el) {
    if ($(el).length > 0) {
      $(el).wrap('<div class="main-carousel-wrapper"></div>');
    }
  }

  function clonarCategorias() {
    var aux = $("#mega-menu-primary .mega-item-productos");
    var menuProductosTop = $(aux).clone();
    $(menuProductosTop)
      .attr("id", "_productos-mobile")
      .removeClass("mega-item-productos item-productos")
      .addClass("_productos-mobile");
    $(".mega-navs", $(menuProductosTop)).remove();
    $("li", $(menuProductosTop)).removeAttr("id");
    $(menuProductosTop).insertAfter(aux);
  }

	$(document).ready(function () {
		$(".listing-zonas-categories").lightSlider({
				item: 7,
				enableDrag: false,
				enableTouch: false,
				pager: false,
				loop: false,
				controls: false,
				//slideMargin: 60,
				responsive: [
					{
						breakpoint: 769,
						settings: {
							item: 4.4,
							enableDrag: true,
							enableTouch: true,
						}
					},
					{
						breakpoint: 481,
						settings: {
							item: 2.4,
							enableDrag: true,
							enableTouch: true,
						}
					}
				]
			}
		);

		$("#post-grid-tips .vc_pageable-slide-wrapper").lightSlider({
			item: 2,
			enableDrag: true,
			enableTouch: true,
			prevHtml: '<i class="fa fa-angle-left"></i>',
			nextHtml: '<i class="fa fa-angle-right"></i>',
			pager: false,
			loop: false,
			slideMargin: 0,
			responsive : [
				{
					breakpoint: 1400,
					settings: {
						item: 1.10,
					}
				},
				{
					breakpoint: 769,
					settings: {
						item: 1.1,
						pager: false,
					}
				},
				{
					breakpoint: 420,
					settings: {
						item: 1.05,
						pager: true,
					}
				}
			],
			onAfterSlide: function (el) {
				let current = el.getCurrentSlideCount();
				let total = el.getTotalSlideCount();
				let next = $('#post-grid-tips .lSNext');
				let prev = $('#post-grid-tips .lSPrev');

				if ( current === total ) {
					next.fadeOut();
				} else {
					next.fadeIn();
				}

				if (current === 1) {
					prev.fadeOut();
				} else {
					prev.fadeIn();
				}
			},
			onSliderLoad: function (el) {
				$('#post-grid-tips .lSPrev').fadeOut();
			}
		});
	});
});
