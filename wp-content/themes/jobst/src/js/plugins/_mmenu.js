jQuery(function ($) {
	var mmenu_args = {
		extensions: [
			"fx-panels-slide-100",
			"pagedim-black",
			"position-right",
			"position-front"
		],
		close: false,
		navbar: false,
		onClick: {
			setSelected: true
		},
		keyboardNavigation: {
			enable: true,
			enhance: true
		}
	};

	var contactenos = $("#contactenos")
		.mmenu(mmenu_args)
		.data("mmenu"); // Mmenu -> Empl

	$(".mm-menu").each(function () {
		$(this)
			.find(".mm-panel")
			.append(
				'<span class="et_close_search_field mmenu-close-button"><i class="fas fa-times"></i></span>'
			);
	});

	// Contactenos
	$(document).on("click", "#site-navigation-sec .menu-item-contacto a",
		function (e) {
			e.preventDefault();
			contactenos.open();
		}
	);

	$(document).on("click", ".contact-button button", function (e) {
		e.preventDefault();
		contactenos.open();
	});

	$(document).on("click", ".button-contact a", function (e) {
		e.preventDefault();
		contactenos.open();
	});

	$(document).on("click", ".contact-button a", function (e) {
		e.preventDefault();
		contactenos.open();
	});

	$(document).on("click", "#contactenos .mmenu-close-button", function () {
		contactenos.close();
		return false;
	});

	// Cotizar
	var cotizar = $("#cotizar")
		.mmenu(mmenu_args)
		.data("mmenu"); // Mmenu
	// console.log('cotizar:', $(".cotizar button"));


	$(document).on("click", ".cotizar button", function (e) {
		e.preventDefault();
		cotizar.open();
	});

	$(document).on("click", "#cotizar .mmenu-close-button", function () {
		cotizar.close();
		return false;
	});

	//fix scroll transition
	$(".mm-tabstart").addClass("et_smooth_scroll_disabled");
});
