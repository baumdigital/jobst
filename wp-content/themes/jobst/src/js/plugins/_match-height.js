jQuery(function($) {
  $(window).on("load resize", function() {
    var wwindow = window.innerWidth;
    /* ========== LISTADO de productos ========== */
    // Image container
    setTimeout(function() {
      if ($(".products").length > 0) {
        $("ul.products").each(function() {
          if (wwindow > 349) {
            if ($(this).find("li").length > 1) {
              $(this)
                .find("li.product .product-image-container")
                .matchHeight({ byRow: true });
            } else {
              var height = $(this)
                .find("li.product .product-image-container")
                .height();
              $(this)
                .find("li.product .product-image-container")
                .height(height);
            }
          } else {
            $(this)
              .find("li.product .product-image-container")
              .matchHeight({ remove: false });
          }
        });
      }

      MatchMe("li.product", ".product-title-container", 349, true);
    }, 800);

    $(document).on("success_modal", function() {
      setTimeout(function() {
        /* ========== DETALLE de producto ========== */
        MatchMe("#wpis-gallery", "#wpis-gallery .slick-slide", 100, false);

        MatchMe(".wpis-slider-for", ".wpis-slider-for .slick-slide", 300, true);

      }, 100);
    });

    $(document).on("added_to_cart", function() {
      setTimeout(function() {
        MatchMe(".xoo-cp-container", ".wbt-custom-product", 300, true);
      }, 100);
    });

    $(document).on("cp_additional_products", function() {
      setTimeout(function() {
        MatchMe(".xoo-cp-container .wbt-custom-product", ".variations .label", 300, true);
      }, 100);
    });

    setTimeout(function() {
      /* ========== PROCESO DE CHECKOUT ========== */
      MatchMe(
        ".woocommerce-checkout",
        ".woocommerce-checkout label",
        480,
        true
      );

      // Match => Home: Seccion de articulo
      //MatchMe('.woocommerce-checkout', '.woocommerce-checkout .form-row', 480, true);

      MatchMe(".stores-container.layout-grid ", ".store-content", 600, true);
      /* ========== form mmenu - column 50 ========== */
      MatchMe(".mm-panel .wpcf7-form", ".map-title", 319, true);
      // Match => Home: Seccion nuestra historia - grid post
      MatchMe(".vc_pageable-wrapper", ".vc_grid-item .vc_gitem-col", 480, true);
    }, 100);
  });

  /*
   * function Match Height
   * Autor: Francisco Chanto  - francisco@baum.digital
   * Requiere variable: var ww = window.innerWidth;
   * c = condicion => si el selector existe
   * s = selector  => al que se le aplica el plugin
   * q = media query => media query donde termina
   * _byRow = revisa si por cada linea hace falta aplicar el plugin
   */
  function MatchMe(_c, _s, _q, _byRow) {
    var ww = window.innerWidth;
    var _byRow = true;
    if ($(_c).length > 0) {
      ww > _q
        ? $(_s).matchHeight({ byRow: _byRow })
        : $(_s).matchHeight({ remove: _byRow });
    }
  }

	$( document ).ready(function() {
		// Card Match Height
		$('.wb-baum-card-title').matchHeight();

		// Blog list
		$('.blog article').matchHeight();
		$('.blog article header.entry-header').matchHeight();
		$('.blog article .entry-content-excerpt').matchHeight();
		$('.archive article').matchHeight();
		$('.archive article header.entry-header').matchHeight();
		$('.archive article .entry-content-excerpt').matchHeight();
		$('.single-zona-venta').matchHeight();

		// Fix blog list
		setTimeout(function () {
			$('.blog article header.entry-header').matchHeight();
			$('.blog article .entry-content-excerpt').matchHeight();
			$('.single-zona-venta').matchHeight();
		}, 1000);
	});
});
