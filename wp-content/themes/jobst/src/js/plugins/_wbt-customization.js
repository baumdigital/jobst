jQuery(function($) {
	var $variations_form_wrapper = $('.variations_form-wrapper');

	/**
	** woobt_update_count trigger removed
	** 27-12-2019 - KMA
	**/

	$('.woobt_products').find('.select select').change(function() {
		if($(this).val() == '') {
			$(this).closest('.woobt-product').find('.woobt-checkbox').prop('checked', false);
		} else {
			$(this).closest('.woobt-product').find('.woobt-checkbox').prop('checked', true);
		}
	});

	/**
	** found_variation trigger removed
	** 27-12-2019 - KMA
	**/

	$(document).on('added_to_cart', function(fragments, cart_hash, button) {
		$variations_form_wrapper.addClass('bm-loader');
		var _woobt_ids = ($('.xoo-qv-summary').length) ? $('.xoo-qv-summary').find('.woobt_ids') : $('.woobt_ids');
		// var product_id = ($('.xoo-qv-summary').length) ? $('.xoo-qv-summary').find('input[name="product_id"]').val() : $('input[name="product_id"]').val();
		var product_id_wrapper = ($('.xoo-qv-main').length) ? $('.xoo-qv-main') : $('.shop-wrapper');
		
		if(product_id_wrapper.length) {
			var product_id = product_id_wrapper.find('.product.entry').attr('id').split('-');
			product_id = product_id[1];
			var cp_container = $('.xoo-cp-container');
			cp_container.addClass('bm-loader');


			// $('.xoo-cp-container .variations').each(function() {
			// 	$(this).find('select').val('').prop('disabled', false).trigger('change');
			// });

			var data = {
				action: 'baumchild_wbt_custom_items',
				woobt_ids: _woobt_ids.val(),
				product_id: product_id,
			}
			$.post(baumchild_ajax.ajax_url, data)
			.done(function (response) {
				cp_container.find('.added-to-cart-additionals').html(response.additional_products);

				var _selects = cp_container.find('.added-to-cart-additionals select');
				_selects.children('option[value="-1"]').remove()
				_selects.select2({
					width: 'resolve',
					minimumResultsForSearch: Infinity
				});

				$(document.body).trigger('cp_additional_products', [product_id]);
				
				$.each(response.products, function(key, value) {
					$.each(value, function(_key, _value) {
						var select = $('.xoo-cp-container').find('select[name="' + _key + '"]');
						if(select.length) {
							select.val(_value).prop('disabled', true).trigger('change');
						}

						select.closest('.variations').find('label').append('<span class="xoo-cp-icon-check"></span>');
					});
				});
				$variations_form_wrapper.removeClass('bm-loader');
				cp_container.removeClass('bm-loader');
			})
			.fail(function (error) {
				console.log('fail');
			})
			.always(function() {
				cp_container.removeClass('bm-loader');
			});
		}
	});

	$(document).on('success_modal', function() {
		var qv_container = $('.xoo-qv-main');
		// var product_id = qv_container.find('.variations_form').data('product_id');
		var product_id = qv_container.find('.product.entry').attr('id').split('-');
		product_id = product_id[1];
		
		var data = {
			action: 'baumchild_get_additional_products',
			product_id: product_id,
		}
		$.post(baumchild_ajax.ajax_url, data)
		.done(function (response) {
			// console.log(JSON.stringify(response));
			qv_container.find('.additional-products-wrapper').html(response.additional_products);
			var _selects = qv_container.find('.additional-products-wrapper select');
			_selects.children('option[value="-1"]').remove()
			_selects.select2({
				width: 'resolve',
				minimumResultsForSearch: Infinity
			});

			// Reset woobt_ids value
			$('.xoo-qv-summary').find('.woobt_ids').val('');
			$(document.body).trigger('qv_additional_products', [product_id]);
		})
		.fail(function (error) {
			console.log('fail');
		});
	});

	$(document).on('change', '.added-to-cart-additionals select', function() {
		var _this = $(this);
		var _this_id = $(this).attr('id');
		// var _woobt_ids = ($('.xoo-qv-summary').length) ? $('.xoo-qv-summary').find('.woobt_ids') : $('.woobt_ids');
		var _wrapper = ($('.xoo-qv-main').length) ? $('.xoo-qv-main') : $('.shop-wrapper');
		// var _cp_product = ($('.xoo-qv-main').length) ? $('.xoo-qv-main').find('.wbt-custom-product') : $('.shop-wrapper').find('.woobt-product');
		// var _cp_product = _wrapper.find('.wbt-custom-product');
		_wrapper.find('#' + _this_id).val(_this.val()).trigger('change');
		var variations_form = _wrapper.find('#' + _this_id).closest('.variations_form');

		var is_disabled = _this.prop('disabled');
		var data = {
			attributeFields: {},
			variationData: variations_form.data('product_variations')
		};

		data.attributeFields[_this.attr('name')] = _this.val();
		var variation = _findMatchingVariations(data.variationData, data.attributeFields);
		var t = variation[0];
		

		if(!is_disabled && _this.val() != '') {
			var data = {
				action: 'baumchild_ajax_add_to_cart',
				product_id: variations_form.data('product_id'),
				quantity: 1,
				variation_id: t['variation_id'],
			};
			ajax_add_cart(data, _this.closest('.variations_form-wrapper'));
		}
	});

	$(document).on('change', '.xoo-qv-summary .additional-products-wrapper select', function() {
		var _this = $(this);
		var _qv_product = _this.closest('.wbt-custom-product');
		var variations_form = _this.closest('.variations_form');
		var data = {
			attributeFields: {},
			variationData: variations_form.data('product_variations')
		};

		data.attributeFields[_this.attr('name')] = _this.val();
		var variation = _findMatchingVariations(data.variationData, data.attributeFields);
		var t = variation[0];
		
		if (t['is_purchasable'] && t['is_in_stock']) {
			_qv_product.data('id', t['variation_id']);
			_qv_product.data('price-ori', t['display_price']);
		} else {
			_qv_product.data('id', 0);
		}

		if(_this.val() == '') 
			_qv_product.data('id', 0);

		baumchild_qv_woobt_save_ids();
	});

	function ajax_add_cart(data, loader_wrapper) {
		loader_wrapper.addClass('bm-loader');
		$.post(wc_add_to_cart_params.ajax_url, data)
		.done(function (response) {
			if (response.error & response.product_url) {
				window.location = response.product_url;
				return;
			} else {
				$(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash]);
			}
		})
		.fail(function (error) {
			console.log(JSON.stringify(error));
		})
		.always(function() {
			loader_wrapper.removeClass('bm-loader');
		});
    }

    function baumchild_qv_woobt_save_ids() {
    	var woobt_items = [];
    	var _wrapper = $('.xoo-qv-summary');
    	var _woobt_btn = _wrapper.find('.single_add_to_cart_button');
		var _woobt_products = _wrapper.find('.wbt-custom-product').each(function() {
			var item_id = $(this).data('id');
			if(item_id > 0)
				woobt_items.push(item_id + '/100%/1');
		});

		if (woobt_items.length > 0) {
			$('.xoo-qv-summary').find('.woobt_ids').val(woobt_items.join(','));
		} else {
			$('.xoo-qv-summary').find('.woobt_ids').val('');
		}
    }

	function _findMatchingVariations( variations, attributes ) {
		var matching = [];
		for ( var i = 0; i < variations.length; i++ ) {
			var variation = variations[i];

			if ( _isMatch( variation.attributes, attributes ) ) {
				matching.push( variation );
			}
		}
		return matching;
	}

	function _isMatch( variation_attributes, attributes ) {
		var match = true;
		for ( var attr_name in variation_attributes ) {
			if ( variation_attributes.hasOwnProperty( attr_name ) ) {
				var val1 = variation_attributes[ attr_name ];
				var val2 = attributes[ attr_name ];
				if ( val1 !== undefined && val2 !== undefined && val1.length !== 0 && val2.length !== 0 && val1 !== val2 ) {
					match = false;
				}
			}
		}
		return match;
	}
});