jQuery(function ($) {
	/**
	 ** Punto Venta dropdown change trigger and redirect
	 **/
	$(document).on('change', '.punto-venta-categories', function () {
		var url = $(this).find('option:selected').data('url');
		window.location.href = url;
	});

	/**
	 ** Listing zonas filterables
	 **/
	$(document).on('click', '.listing-zonas-categories .single-zona', function () {
		var _this = $(this);
		var zona = _this.data('zona');
		var tiendas = $('.listing-zonas > li');

		$('.listing-zonas-categories .single-zona').removeClass('active');
		_this.addClass('active');

		if (_this.hasClass('all-zonas')) {
			// tiendas.removeClass('is-animated').fadeOut('slow', function() {
			tiendas.removeClass('is-animated').fadeOut().promise().done(function () {
				tiendas.addClass('is-animated').fadeIn();
				get_the_fisrt_zona();
			});
		} else {
			// tiendas.removeClass('is-animated').fadeOut('slow', function() {
			tiendas.removeClass('is-animated').fadeOut().promise().done(function () {
				tiendas.filter('[class*="' + zona + '"]').addClass('is-animated').fadeIn();
				get_the_fisrt_zona();
			});
		}

		// jQuery Match height
		$('.single-zona-venta').matchHeight();

		return false;
	});

	get_the_fisrt_zona();

	function get_the_fisrt_zona() {
		var columns = $('.listing-zonas').data('columns');
		if ($(document).width() > 768 && $(document).width() < 993) {
			columns = 3;
		}

		if ($(document).width() < 769) {
			columns = 2;
		}

		$('.listing-zonas > li').removeClass('the-first');
		$('.listing-zonas > li.is-animated').filter(function (index) {
			return index % columns === 0;
		}).addClass('the-first');
	}

	$(window).on('resize', function () {
		get_the_fisrt_zona();
	});
});
