jQuery(function ($) {
	/**
	** Clase ready al ul en listado y detalle de producto
	**/
	if ($('body.post-type-archive-product ul.products > li, body.tax-product_cat ul.products > li, body.single-product ul.products  li.product').length > 1) {
		$('ul.products').addClass('ready');
	}
	
	$(window).on('load', function () {
		$('ul.products li.product img').css('opacity', '1');
	});

	if ($('body.single-product').length > 0) {
		$(window).on('load', function () {
			if($('#wpis-gallery .slick-slide').length > 1) {
				$('#wpis-gallery').addClass('ready');
			}
		});

		$('.images section.wpis-slider-for').append('<span class="click-mobile fa"></span>');
		$(document).on('click', '.click-mobile', function() {
			$('.slick-current .wpis-popup').trigger('click');
		});
	}

	/**
	** Aplica clase .quick-view-hover en el hover al li.product
	**/
	$('a.xoo-qv-button').on('mouseenter, mouseover ', function () {
		$(this).closest('li.product').addClass('quick-view-hover');
	});

	/**
	** Remueve clase .quick-view-hover en el hover al li.product
	**/
	$('a.xoo-qv-button').on('mouseleave', function () {
		$(this).closest('li.product').removeClass('quick-view-hover');
	});

	/**
	** Muestra las images del carrusel
	**/
	if ($('.product div.images').length > 0) {
		$(window).on('load', function () {
			$('.woocommerce .product div.images').animate({
				opacity: 1
			}, 800, function () {
				$(this).addClass('slick-loaded');
			});
		});

		$(document).on('change', '.div.xoo-qv-panel', function () {
			$('.woocommerce .product div.images').animate({
				opacity: 1
			}, 800, function () {
				$(this).addClass('slick-loaded');
			});
		});
	}
});