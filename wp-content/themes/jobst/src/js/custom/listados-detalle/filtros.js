jQuery(function ($) {
    /**
     ** Toggle de filtros en responsivo
     ** Agrega clase opened al button
     ** agrega la clase open-filter al section que contiene los filtros
     ** toggle class filter-drop-open al contenedor del listado
     ** Anima el button al top
     **/
    $("#toggle-filtros").on('click', function () {
        $(this).toggleClass('opened');
        $(this).siblings('section.widget').toggleClass('open-filters');
        $('.archive-container').toggleClass('filter-drop-open');
//        $('html, body').animate({scrollTop: $(this).offset().top - ($('header').height() + 30)}, 900, function () {
//            $('body').toggleClass('body-overflow');
//        });
        $('html, body').animate({scrollTop: $(this).offset().top - ($('header').height() + 30)}, 900, function () {
            $('body').toggleClass('body-overflow');
        });

    });

    /**
     ** Si ww < 600 se le aplica evento al H4 de cada filtro
     **/
    $(window).on('load resize', function () {
        var ww = window.innerWidth;

        if (ww > 600) {
            $('.woof_block_html_items').show();
        } else {
            $('.woof_block_html_items').hide();
        }
    });
    $(window).on('load resize', function () {
        var wsize = window.innerWidth;
        wsize >= 600 ? $('.woof_container_inner h4').removeClass('filter-action') : '';
    });

    $('.woof_container_inner h4').on('click', function (e) {
        var size = window.innerWidth;
        e.preventDefault();
        if (size <= 600) {
            $(this).toggleClass('filter-action');
            $(this).siblings('.woof_block_html_items').slideToggle();
        }
    });

	let compression = {
		'preventiva': {
			'term_id': '80',
			'content': ': 8-15 mmHg'
		},
		'baja': {
			'term_id': '39',
			'content': ': 15-20 mmHg'
		},
		'media': {
			'term_id': '38',
			'content': ': 20-30 mmHg'
		},
		'alta': {
			'term_id': '37',
			'content': ': 30-40 mmHg'
		},
	};

	for (let compressionKey in compression) {
		let term_id = compression[compressionKey].term_id;
		let content = compression[compressionKey].content;
		let selector = $('.woof_term_' + term_id + ' label');

		let prev_text = selector.text();
		selector.text( prev_text + content );
	}
});
