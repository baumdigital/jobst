jQuery(function ($) {
	/**
	** Gallery on success modal
	**/
	$(document).on('success_modal', function () {
		var select = $('.xoo-qv-summary .variations select').first();
		var name = select.attr('name');
		if (select.hasClass('select_active'))
			select.removeClass('select_active');

		select.addClass('select_listing_active');

		$('.xoo-qv-images .wpis-slider-for').slick({
			fade: true,
			arrows: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: true,
			asNavFor: '.wpis-slider-nav'
		});

		$('.xoo-qv-images .wpis-slider-nav').slick({
			dots: false,
			arrows: true,
			centerMode: false,
			focusOnSelect: true,
			infinite:false,
			slidesToShow: 4,
			slidesToScroll: 1,
			infinite: true,
			lazyLoad: 'ondemand',
			asNavFor: '.wpis-slider-for'
		});

		$('.xoo-qv-images .wpis-slider-for .slick-track').addClass('woocommerce-product-gallery__image single-product-main-image');
		$('.xoo-qv-images .wpis-slider-nav .slick-track').addClass('flex-control-nav');
		$('.xoo-qv-images .wpis-slider-nav .slick-track li img').removeAttr('srcset');

		$('.variations select').change(function(){
			$('.xoo-qv-images .wpis-slider-nav').slick('slickGoTo', 0);
		});
	});
});