jQuery(function($) {
	if($('.woocommerce-view-order .woocommerce-bacs-bank-details').length) {
		$('.woocommerce-bacs-bank-details').toggle()
	}

	$('.btn-detalle-cuentas').click(function() {
		$('.woocommerce-bacs-bank-details').slideToggle();
		$(this).find('.toggle').toggle();

		return false;
	});

	$(document).on('click', '.btn-bacs_transaction', function() {
		var redirect = $('form#bacs_transaction input[name="redirect"]').val();

		if($('form#bacs_transaction #bacs_transaction_code').val() == "") {
			$('form#bacs_transaction #bacs_transaction_code').addClass('required');
			$('.bacs_transaction_result').addClass('woocommerce-error').show().text('Agregue número de transferencia');
		} else if($('form#bacs_transaction select#bacs_banco').val() == "") {
			$('.bacs_transaction_result').addClass('woocommerce-error').show().text('Seleccione una opción de banco');
		} else {
			var data = {
				action: 'baumchild_bacs_payment_save_custom_data',
				order_id: $('form#bacs_transaction input[name="orderid"]').val(),
				bacs_transaction_code: $('form#bacs_transaction #bacs_transaction_code').val(),
				bacs_banco: $('form#bacs_transaction #bacs_banco').val()
			};
			$(this).find('.fa').removeClass('hide');
			
			$.post(baumchild_ajax.ajax_url, data)
			.done(function (response) {
				$('#bacs_transaction').slideUp();
				$('.bacs_transaction_result').removeClass('woocommerce-error').addClass('woocommerce-message').show().text('¡Número de transferencia guardado, la orden será procesada!');

				setTimeout(function() {
					window.location.href = redirect;
				}, 2000);
			})
			.fail(function (error) {
				$('.bacs_transaction_result').addClass('woocommerce-error').show().text('¡Ocurrió algo, intente luego!');
			})
			.always(function() {
				$('.btn-bacs_transaction').find('.fa').addClass('hide');
			});
		}

		setTimeout(function() {
			$('.bacs_transaction_result.woocommerce-error').fadeOut('slow');
		}, 3500);

		return false;
	});

	$('#bacs_transaction').on('submit', function() {
		$('.btn-bacs_transaction').trigger('click');

		return false;
	});

	$('form#bacs_transaction #bacs_transaction_code').on('blur change keyup keypress', function() {
		if($(this).val() == "") {
			$(this).addClass('required');
		} else {
			$(this).removeClass('required');
		}
	});
});