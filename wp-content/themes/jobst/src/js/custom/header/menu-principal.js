jQuery(function ($) {

    /*
     * Cierra el menu principal
     */
    $(document).on('click', '.nav-overlay, .menu-item-buscador, .contact-button', function (e) {
        e.preventDefault();
        if ($('.site-header-menu-primary').hasClass('nav-open')) {
            if (!$(e.target).closest('#site-navigation').length) {
                closeMobileNav($('.site-header-toggle-btn'));
            }
        }
    });
    $(document).on('click', '.site-header-toggle-btn', function (e) {
        if (typeof $(this).attr('data-state') == 'undefined' || $(this).attr('data-state') == 1) {
            openMobileNav(this);
        } else {
            closeMobileNav(this);
        }
    });

    $(window).on('resize', function () {
        var ww = window.innerWidth;
        if (ww > 767) {
            closeMobileNav('.site-header-toggle-btn');
        }
    });

    function openMobileNav(target) {
        $('body').addClass('body-overflow');
        $('.site-header-menu-primary').addClass('nav-open');
        $('.site-header-toggle-btn').addClass('open');
        $('#site-navigation').animate({
            left: '0%'
        }, 400, function () {
            $(target).attr('data-state', 0);
        });

        // Open products menu by default
        $('#_productos-mobile .mega-menu-link').addClass('_sub-menu-open');
		$('#_productos-mobile .mega-sub-menu').css('display', 'block');
    }

    function closeMobileNav(target) {
        $('.site-header-toggle-btn').removeClass('open');
        $('#site-navigation').animate({
            left: '-100%'
        }, 400, function () {
            $('.site-header-menu-primary').removeClass('nav-open');
            $('body').removeClass('body-overflow');
            $(target).attr('data-state', 1);
        });
    }


    /*
     *  Abre primer nivel Menu
     */
    $(document).on('click', '._productos-mobile > a ', function () {
        if ($(window).innerWidth() < 769) {
            $(this).toggleClass('_sub-menu-open');
            $(this).siblings('.mega-sub-menu').slideToggle(350);
        }
    });
    /*
     *  Abre los hijos de la categoria
     */
    $(document).on('click', '.mega-block-title', function () {
        if ($(window).innerWidth() < 769) {
            $(this).toggleClass('_sub-menu-open');
            $(this).siblings('.wc-product-categories').slideToggle(350);
        }
    });

});


