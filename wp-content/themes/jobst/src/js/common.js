jQuery(function ($) {

    /**
     ** Wishlist & Print button
     **/
    $(document).on('click', 'a.tinvwl_add_to_wishlist_button, .woocommerce .button.print', function () {
        $('body').addClass('bm-loader body-overflow');
    });

    $(document).on('click', '.tinv-close-modal, .tinvwl_button_close, .tinv-overlay', function () {
        $('body').removeClass('bm-loader body-overflow');
    });

    /**
     ** Print click loader
     **/
    $(document).on('printLinkComplete', function () {
        $('body').removeClass('bm-loader body-overflow');
    });

    /**
     ** Custom checked checkboxes
     **/
    $(document).on('click', 'input[type="checkbox"]', function () {
        console.log('click');
        if ($(this).is(':checked')) {
            $(this).parent('label[class*="checkbox"]').addClass('checked');
        } else {
            $(this).parent('label[class*="checkbox"]').removeClass('checked');
        }
    });

    /**
     ** Select per page
     **/
    $('.woocommerce-posts-per-page').on('change', 'select.posts-per-page', function () {
        $(this).closest('form').submit();
    });

    /**
     ** Allow only numbers in input type text
     */

    numbersOnly('.input-container input.input-text');

    function numbersOnly(s) {
        $(s).on('keydown', function (e) {
            -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || (/65|67|86|88/.test(e.keyCode) && (e.ctrlKey === true || e.metaKey === true)) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
        });
    }

    // jQuery Fitvids
	$('iframe[src*="youtube"]').parent().fitVids();
});
