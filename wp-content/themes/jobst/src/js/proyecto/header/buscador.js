jQuery(function ($) {
    // Boton para abrir el buscador
    $('.search-button').on('click', function () {
        if (!$(this).hasClass('_search-is-open')) {
            openSearchBox();
        } else {
            closeSearchBox();
        }
    });


    // Boton para cerrar el buscador
    $('.close-search, .search-overlay').on('click', function () {
        closeSearchBox();
    });

    function openSearchBox() {

        $('.search-button').addClass('_search-is-open');
        $('.search-form').addClass('search-is-open');
        //$('body').addClass('body-overflow search-open');
    }

    function closeSearchBox() {

        $('.search-button').removeClass('_search-is-open');
        $('.search-form').removeClass('search-is-open');
        $('body').removeClass('body-overflow search-open');
        $('#ajaxsearchprores1_1').fadeOut("fast");
        $('.search-form #ajaxsearchpro1_1 input.orig').val("");
        $('.search-form').removeClass('search-is-open');
        //$('body').removeClass('body-overflow search-open');
    }


});