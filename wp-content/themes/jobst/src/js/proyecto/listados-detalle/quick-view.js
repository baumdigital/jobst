jQuery(function ($) {
	/**
	** Add class to add overlay in quick view next/prev
	**/
	$(document).on('click', '.xoo-qv-nxt, .xoo-qv-prev', function() {
		$('.xoo-qv-top-panel').addClass('loading');
	});
});