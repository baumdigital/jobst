jQuery(function ($) {

	$(window).on('load', function () {
		var url = window.location.href;
		if (url.indexOf("#registro-btn") >= 0) {
			$("#login-woocommerce, #register-woocommerce").fadeToggle('slow').toggleClass('open');
		}
	});

	$("#registro-btn, #login-btn").click(function () {
		var site_content_top = $('.site-content').position().top;
		
		$("html, body").animate({scrollTop: site_content_top}, 300);
		$("#login-woocommerce, #register-woocommerce").fadeToggle('slow').toggleClass('open');
		
		window.history.pushState(null, "", "#" + $(this).attr("id"));
	});

	$(".toggle-account").click(function () {
		$(".menu-edit-account").toggle();
	});
});