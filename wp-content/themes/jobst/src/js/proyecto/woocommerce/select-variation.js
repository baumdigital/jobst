jQuery(function($) {
	/**
	** Variations trigger
	**/
	$(document).on("woocommerce_variation_select_change", ".variations_form", function () {
		$(this).find('input.qty').val(1);

		setTimeout(function() {
			if($(this).find('input.qty').attr('max') == "") {
				$(this).find('input.qty').attr('max', 1);
			}
		}, 80);
	});

	$(document).on('change blur keyup', 'input.qty', function () {
		var _this = $(this);
		var _val = parseFloat(_this.val());
		var _max = parseFloat(_this.attr('max'));
		var _min = (_this.attr('min') > 0) ? parseFloat(_this.attr('min')) : 1;

		if(_val > _this.attr('max')) {
			_this.val(_min);
			_this.trigger('change');
		}

		if(_val <= _max) {
			enable_qty_input('down', _this);
			enable_qty_input('up', _this);
		}		

		if(_val == _max) {
			disable_qty_input('up', _this);
		}

		if(_val == 1) {
			disable_qty_input('down', _this);
		}
	});

	function enable_qty_input(pos, input) {
		input.parent().find('.quantity-' + pos).removeClass('quantity-disabled');
	}

	function disable_qty_input(pos, input) {
		input.parent().find('.quantity-' + pos).addClass('quantity-disabled');
	}
});