********* functions.php ***********
	** Declare child theme from twentynineteen
	** Declare env variables
	** Custom functions
	** Default logo
	** Social Links
	** Manage menus
	** Including the url of fonts googleapis 
	** Enqueue and dequeue CSS and JS
	** CSS Inline
	** Customizer Options
	** Admin login logo
	** Tracking codes
	** Settings

********* wc-custom-functions.php ***********
	** WC Support
	** Declare WC env variables
	** Attributes and Terms setup after update
	** Checkout page
	** WC Performance
	** Second image in listings
	** Manage sidebar in WC Pages
	** Posts per page
	** Manage pagination, columns and product limit
	** Customize nav items in Account Menu
	** Manage endpoints data
	** Manage WC Breadcrumbs
	** Add container to WC Pages
	** Sorter Options
	** Remove WC Add to Cart
	** Remove WC Add to Cart
	** Remove Result count in categories
	** Change SKU Position, product page & quick view
	** Adding Return to Shop button when not products found
	** Change column layout to 3 columns
	** Remove sale flash in product listing & Quick view
	** Carousel Related Products
	** Change WC image placeholder
	** Enable/Disable field in shipping calculator
	** Adding "From" to variable products price
	** Share options single product
	** Update product title structure in loops
	** Move single variation div before add to cart button
	** Remove WC Product Tabs, Cross Sell (Cart Page), Upsell & default Related Products
	** Listing product structure
	** CRC Currency
	** Update cart quantity in cart page
	** Set content_block post type as public, visibility for VC
	** Remove cart shipping label
	** Add image placeholder to Ajax Search Pro results
	** Hide shipping rates when free shipping is available.
	** Remove navigation in accounts pages