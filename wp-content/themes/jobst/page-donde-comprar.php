<?php
/**
 * Template Name: Sin encabezado
 * Template Post Type: Page
 */

get_header( 'page' ); ?>

<div id="primary" class="content-area contenido">
	<main id="main" class="site-main">
		<?php
		$child_terms       = array();
		$id_destacado      = array( 157 ); // ID term "Destacado"
		$current_term_id   = get_queried_object()->term_id;
		$current_term_name = get_queried_object()->name;
		$terms             = get_terms( array(
				'taxonomy'   => 'pv_category',
				'hide_empty' => false,
				'exclude'    => $id_destacado
		) );

		$zonas_destacadas = get_posts(
				array(
						'posts_per_page' => 2,
						'post_type'      => 'punto-venta',
						'post_status'    => 'publish',
						'tax_query'      => array(
								'relation' => 'AND',
								array(
										'taxonomy' => 'pv_category',
										'field'    => 'id',
										'terms'    => $current_term_id,
								),
								array(
										'taxonomy' => 'pv_category',
										'field'    => 'id',
										'terms'    => $id_destacado,
								)
						)
				)
		);

		$zonas_array = get_posts(
				array(
						'posts_per_page' => - 1,
						'post_type'      => 'punto-venta',
						'post_status'    => 'publish',
						'tax_query'      => array(
								array(
										'taxonomy' => 'pv_category',
										'field'    => 'id',
										'terms'    => $current_term_id,
								)
						)
				)
		);
		?>
		<?php if ( $terms ) : ?>
			<div class="section-select-zonas">
				<h1><?= __( '¿Dónde comprar?', 'baumchild' ) ?></h1>
				<p><?= __( 'Seleccione un país', 'baumchild' ) ?></p>
				<select class="punto-venta-categories">
					<?php
					foreach ( $terms as $key => $term ) :
						if ( $term->parent != 0 ) {
							if ( $term->parent == $current_term_id ) {
								$child_terms[] = $term;
							}
						} else {
							?>
							<option data-url="<?= esc_url( get_term_link( $term ) ) ?>"
									value="<?= $term->slug ?>" <?php selected( $current_term_id, $term->term_id ) ?>><?= $term->name ?></option>
							<?php
						}
					endforeach;
					?>
				</select>
			</div>

			<?php if ( $zonas_destacadas ) : ?>
				<div class="section-zonas-destacadas">
					<div class="row">
						<?php foreach ( $zonas_destacadas as $key => $zonas_destacada ) : $zona_id = $zonas_destacada->ID; ?>
							<div id="zona-<?= $zona_id ?>" class="col-sm-6 col-xs-12 single-zona-destacada">
								<div class="zona-venta-thumbnail">
									<?= get_the_post_thumbnail( $zona_id ) ?>
								</div>
								<div class="single-zona-venta">
									<h4 class="zona-title"><?= $zonas_destacada->post_title ?></h4>

									<?php do_action( 'baumchild_after_single_zona_venta', $zona_id ) ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>

			<?php
			$gallery_images = get_term_meta( $current_term_id, 'vdw_gallery_id', true );
			if ( $gallery_images ) :
				?>
				<div class="section-carousel-images">
					<div class="container">
						<div class="carousel-images-wrapper">
							<div class="row">
								<ul class="cS-hidden zona-ventas-carousel" data-ci_lg="4" data-ci_md="3" data-ci_sm="2"
									data-ci_xs="2">
									<?php
									foreach ( $gallery_images as $key => $image_id ) :
										$image = wp_get_attachment_image_src( $image_id, 'full' );
										?>
										<li>
											<img class="image-preview" src="<?php echo $image[0]; ?>">
										</li>

									<?php endforeach; ?>
								</ul>
								<div class="slider-arrows">
									<a href="#" class="arrow arrow-prev"><i class="fa fa-angle-left"></i></a>
									<a href="#" class="arrow arrow-next"><i class="fa fa-angle-right"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>

			<div class="section-filterable-zonas">
				<div class="listing-zonas-categories">
					<p><?= __( 'A continuación detallamos una lista de los establecimientos en los que puede comprar nuestros productos.', 'baumchild' ) ?></p>
					<p><?= __( 'Seleccione una zona', 'baumchild' ) ?></p>
					<a href="#" class="single-zona all-zonas" data-zona=""><?= __( 'Todos', 'baumchild' ) ?></a>
					<?php
					if ( $child_terms ) :
						foreach ( $child_terms as $key => $child_term ) :
							?>
							<a href="#" class="single-zona"
							   data-zona="<?= $child_term->slug ?>"><?= $child_term->name ?></a>
						<?php
						endforeach;
					endif;
					?>
				</div>
				<div class="filtering-zonas-venta">
					<?php if ( $zonas_array ) : ?>
						<ul data-columns="3" class="columns-3 listing-zonas list-unstyled">
							<?php
							foreach ( $zonas_array as $key => $zona_array ) :
								$the_terms = $custom_fields = array();
								$zona_id = $zona_array->ID;
								foreach ( wp_get_post_terms( $zona_array->ID, 'pv_category', array( 'parent' => $current_term_id ) ) as $key => $current_terms ) {
									$the_terms[] = $current_terms->slug;
								}
								$the_terms[] = 'is-animated';
								?>
								<li id="zona-<?= $zona_id ?>" class="<?= implode( ' ', $the_terms ) ?>">
									<div class="single-zona-venta">
										<i class="icon-store"></i>
										<h4 class="zona-title"><?= $zona_array->post_title ?></h4>

										<?php do_action( 'baumchild_after_single_zona_venta', $zona_id, array( 'pv_ciudad' ) ) ?>
									</div>
								</li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
		<?php
		else :

			get_template_part( 'template-parts/content/content', 'none' );

		endif; ?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
