27-08-2019 - v1.0.5
	-Update: Update plugins (Network)
	-Update: WooCommerce database
	-Update: WC templates: Menú > WooCommerce > Estado > Plantillas
		* woocommerce/cart/cart-shipping.php
		* woocommerce/cart/cart.php > Agregar action woocommerce_before_cart_collaterals (ver original)
		* woocommerce/checkout/form-billing.php
		* woocommerce/checkout/thankyou.php
		* woocommerce/global/quantity-input.
		* woocommerce/global/form-login.php
		* woocommerce/myaccount/form-login.php
		* woocommerce/order/order-details-item.php
		* woocommerce/order/order-details.php
		- woocommerce/ti-wishlist-item-data.php
		+ woocommerce/ti-wishlist-user.php
		+ woocommerce/ti-wishlist-social.php
		+ woocommerce/content-product.php
		* woocommerce/ti-addedtowishlist-dialogbox.php
		* woocommerce/ti-wishlist.
		* woocommerce/emails/customer-invoice.php
		* woocommerce/emails/email-address.php
		* woocommerce/emails/email-order-details.php
		* woocommerce/emails/email-order-items.php
	-New: Font Awesome 5
		+ sass/dev/flexbox/: copiar directorio
		* sass/dev/includes/_inc.scss: incluir flexbox directory
		* sass/dev/default/_functions.scss: agregar function fa-content
		* sass/proyecto/mixins-and-placeholders/mixins.scss: mixin boton-de-favoritos
		* sass/proyecto/sections/common/_tiendas.scss: fix iconos FA5
		* functions/wp-customizer.php: function baumchild_sociales
		* functions.php: cdn font awesome
		* functions.php: dequeue style wpis-fontawesome-css
		* woocommerce/ti-addedtowishlist-dialogbox.php: fix icono heart
		+ sass/dev/modulos/_font-awesome-5.scss
		* sass/dev/includes/_inc.scss: incluir font-awesome-5
		* sass/proyecto/sections/common/_menu-principal.scss Agregar al Link principal del MegaMenu @include font-awesome('\f107');
	-Update: Compartir producto y tabla favoritos	
		* functions/wc/wc-share-products.php
		+ sass/proyecto/sections/common/_sociales.scss
		* sass/proyecto/_global.scss Quitar los estilos de las listas ul y ol
		* sass/proyecto/woocommerce/detalle-producto/_detalle-info.scss: eliminar regla de .product-sharing
	-Update: css de variaciones
		* sass/proyecto/woocommerce/detalle-producto/_utilities.scss: mixin producto-variaciones eliminado, borrar donde se importaba
		* sass/proyecto/woocommerce/listado-productos/_quick-view.scss: z-index del bloque eliminado
		* sass/proyecto/woocommerce/listado-productos/_quick-view.scss: estilos descripción de producto _quick-view unificados en sass/proyecto/woocommerce/_main-utilities.scss	
		* sass/proyecto/woocommerce/detalle-producto/_detalle-info.scss: estilos descripción de producto _detalle-info unificados en sass/proyecto/woocommerce/_main-utilities.scss
		* sass/proyecto/woocommerce/_main-utilities.scss: product_meta del detalle de producto unificado
		* sass/proyecto/woocommerce/detalle-producto/_detalle-fotos.scss: slick gallery estilos unificados en sass/proyecto/woocommerce/_main-utilities.scss
		- sass/proyecto/woocommerce/detalle-producto/_detalle-info.scss Eliminado
