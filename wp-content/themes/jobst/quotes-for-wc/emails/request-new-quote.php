<?php
if (! defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
/**
 * Request New Quote email
 */
$order_obj = new WC_order($order->order_id);

$opening_paragraph = __('Una cotización de %s espera su respuesta. Los detalles a continuación:', 'baumchild');
?>

<?php do_action('woocommerce_email_header', $email_heading); ?>

<?php
$billing_first_name = (version_compare(WOOCOMMERCE_VERSION, "3.0.0") < 0) ? $order_obj->billing_first_name : $order_obj->get_billing_first_name();
$billing_last_name = (version_compare(WOOCOMMERCE_VERSION, "3.0.0") < 0) ? $order_obj->billing_last_name : $order_obj->get_billing_last_name(); 
if ($order && $billing_first_name && $billing_last_name) : ?>
	<p><?php printf($opening_paragraph, $billing_first_name . ' ' . $billing_last_name); ?></p>
<?php endif; ?>

<table cellspacing="0" cellpadding="6" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
	<tbody>
		<tr>
			<th style="text-align:left; border: 1px solid #eee;"><?php _e('Producto', 'baumchild'); ?></th>
			<th style="text-align:left; border: 1px solid #eee;"><?php _e('Cantidad', 'baumchild'); ?></th>			
		</tr>
		<?php
		foreach($order_obj->get_items() as $items) {
		    ?>
		    <tr>
                <td style="text-align:left; border: 1px solid #eee;">
                	<?php
						$product = $items->get_product();
						$image_size = array('85', '85');

						echo apply_filters( 'quote_wc_order_item_thumbnail', '<div style="margin-bottom: 5px"><img src="' . ( $product->get_image_id() ? current( wp_get_attachment_image_src( $product->get_image_id(), 'thumbnail' ) ) : baumchild_placeholder_img_src('png') ) . '" alt="' . esc_attr__( 'Product image', 'woocommerce' ) . '" height="' . esc_attr( $image_size[1] ) . '" width="' . esc_attr( $image_size[0] ) . '" style="vertical-align:middle; margin-' . ( is_rtl() ? 'left' : 'right' ) . ': 10px; float: left;" /></div>', $items, $image_size );
                	?>
                	<?php echo $items->get_name(); ?>
                </td>
                <td style="text-align:left; border: 1px solid #eee;"><?php echo $items->get_quantity(); ?></td>
            </tr>
            <?php 
		} 
		?>
	</tbody>
</table>

<p><?php _e('Cotización pendiente.', 'baumchild'); ?></p>

<p><?php echo make_clickable(sprintf(__('Ir al link para ver o editar la cotización: %s', 'baumchild'), admin_url('post.php?post=' . $order->order_id . '&action=edit'))); ?></p>

<?php do_action('woocommerce_email_footer'); ?>
