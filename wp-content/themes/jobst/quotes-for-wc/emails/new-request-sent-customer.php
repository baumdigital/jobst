<?php
if (! defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
/**
 * Request New Quote email
 */
$order_obj = new WC_order($order->order_id);
$display_price = false;
$opening_paragraph = __('Has hecho una solicitud de cotización en %s. Los detalles a continuación:', 'baumchild');
?>

<?php do_action('woocommerce_email_header', $email_heading); ?>

<?php
if ($order) : ?>
	<p><?php printf($opening_paragraph, $site_name); ?></p>
<?php endif; ?>

<table cellspacing="0" cellpadding="6" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
	<tbody>
		<tr>
			<th style="text-align:left; border: 1px solid #eee;"><?php _e('Producto', 'baumchild'); ?></th>
			<th style="text-align:left; border: 1px solid #eee;"><?php _e('Cantidad', 'baumchild'); ?></th>			
		</tr>
		<?php
		foreach($order_obj->get_items() as $items) {
		    ?>
		    <tr>
                <td style="text-align:left; border: 1px solid #eee;">
                	<?php
						$product = $items->get_product();
						$image_size = array('85', '85');

						echo apply_filters( 'quote_wc_order_item_thumbnail', '<div style="margin-bottom: 5px"><img src="' . ( $product->get_image_id() ? current( wp_get_attachment_image_src( $product->get_image_id(), 'thumbnail' ) ) : baumchild_placeholder_img_src('png') ) . '" alt="' . esc_attr__( 'Product image', 'woocommerce' ) . '" height="' . esc_attr( $image_size[1] ) . '" width="' . esc_attr( $image_size[0] ) . '" style="vertical-align:middle; margin-' . ( is_rtl() ? 'left' : 'right' ) . ': 10px; float: left;" /></div>', $items, $image_size );
                	?>
                	<?php echo $items->get_name(); ?>
                </td>
                <td style="text-align:left; border: 1px solid #eee;"><?php echo $items->get_quantity(); ?></td>
            </tr>
            <?php 
		} 
		?>
	</tbody>
</table>

<p><?php _e('Esta cotización está pendiente.', 'baumchild'); ?></p>

<p><?php _e('Pronto recibirá un correo con la cotización por parte del administrador del sitio.', 'baumchild'); ?></p>

<?php do_action('woocommerce_email_footer'); ?>
