<?php
/**
 * * Declare child theme from twentynineteen
 * */
function baumchild_child_enqueue_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
}

// add_action('wp_enqueue_scripts', 'baumchild_child_enqueue_styles');

/**
 * * Declare env variables
 * */
if (!defined('THEME_DIR_URI'))
{
    define('THEME_DIR_URI', get_stylesheet_directory_uri());
}

/**
 * * Custom functions
 * */
require get_stylesheet_directory() . '/functions/wc-includes.php';
require get_stylesheet_directory() . '/functions/wp-fonts.php';
require get_stylesheet_directory() . '/functions/wp-sidebars.php';
require get_stylesheet_directory() . '/functions/wp-customizer.php';
require get_stylesheet_directory() . '/functions/wp-helpers.php';

/**
 * * Manage menus
 * * Tweentynineteen menus added
 * */
function baumchild_manage_menu_locations()
{
    add_theme_support('custom-logo', array(
        'height' => '',
        'width' => '',
        'flex-width' => true,
        'flex-height' => true,
    ));

    $menus = array('menu-1', 'primary', 'footer', 'social');

    foreach ($menus as $key => $menu)
    {
        unregister_nav_menu($menu);
    }

	register_nav_menus( array(
		'primary'     => __( 'Menú Principal', 'baumchild' ),
		'secondary'   => __( 'Menú Secundario', 'baumchild' ),
		'footer'      => __( 'Menú en Pie', 'baumchild' ),
		'footer-term' => __( 'Menú términos y condiciones', 'baumchild' )
	) );
}

add_action('after_setup_theme', 'baumchild_manage_menu_locations', 20);

/**
 * * Enqueue and dequeue CSS and JS
 * */
function baumchild_enqueue_styles() {
	$theme_path = get_stylesheet_directory_uri();
	$min        = ! is_baum_localhost() ? '.min' : '';

	$arrays_css = baum_get_array_for_register_css();
	$arrays_js  = baum_get_array_for_register_js();

	wp_register_style( 'datatell_icons', $theme_path . '/assets/fonts/datatell_icons/styles.css' );
	wp_register_style( 'aquawax_font', $theme_path . '/assets/fonts/aquawax/stylesheet.css' );
	wp_register_style( 'bootstrap_css', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
	wp_register_style( 'font_awesome', '//use.fontawesome.com/releases/v5.6.3/css/all.css' );
	wp_enqueue_style( 'mmenu_css', '//cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/7.3.2/jquery.mmenu.all.css' );
	wp_enqueue_style( 'lightslider', '//cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css' );
	wp_enqueue_style( 'interstate_font', $theme_path . '/assets/fonts/interstate/stylesheet.css' );
	wp_enqueue_style( 'baumchild-fonts', baumchild_fonts_url(), $arrays_css, null );

	/* Adding inline styles */
	wp_add_inline_style( 'baumchild-fonts', baumchild_print_inline_css() );

	wp_register_script( 'jquery_mask', '//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.min.js', array( 'jquery' ) );
	wp_register_script( 'jquery_cookie', '//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js' );
	wp_register_script( 'bootstrap_js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js' );
	wp_register_script( 'matchHeight_js', '//cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js' );
	wp_register_script( 'mmenu_js', '//cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/7.3.2/jquery.mmenu.all.js', array( 'jquery' ) );
	wp_register_script( 'lightslider', '//cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'baumchild-vendor', THEME_DIR_URI . '/assets/js/vendor/vendor' . $min . '.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'baumchild-script', THEME_DIR_URI . '/assets/js/custom/custom' . $min . '.js', $arrays_js, null, true );

	// Declare it to use ajax
	wp_localize_script( 'baumchild-script', 'baumchild_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}

add_action( 'wp_enqueue_scripts', 'baumchild_enqueue_styles' );

function baumchild_dequeue_styles()
{
    $styles = array('font-awesome', 'tinvwl-font-awesome', 'woocommerce-smallscreen', 'woof', 'genericons', 'turbotabs', 'twentynineteen-print-style', 'wps_bootstrap', 'wps_fontawesome', 'twentynineteen-style', 'woocommerce-general', 'wpis-fontawesome-css');
    /*
      if(!is_user_logged_in()) {
      $styles[] = 'dashicons';
      }
     */
    foreach ($styles as $style)
    {
        wp_dequeue_style($style);
        wp_deregister_style($style);
    }

    if (is_baum_localhost())
    {
        wp_enqueue_style('baumchild-style', THEME_DIR_URI . '/assets/css/theme.css', array(), null);
    }

	if ( baum_is_plugin_activated( 'woocommerce/woocommerce.php' ) ) {
		if ( is_product_category() || is_shop() || is_front_page() ) {
			wp_enqueue_script( 'wpis-slick-js', plugins_url( 'woo-product-images-slider/assets/js/slick.min.js' ), array( 'jquery' ), '1.6.0', false );
			wp_enqueue_style( 'wpis-front-css', plugins_url( 'woo-product-images-slider/assets/css/wpis-front.css' ), '1.0', true );
		}
	}
}

add_action('wp_enqueue_scripts', 'baumchild_dequeue_styles', 99);

function baumchild_dequeue_script()
{
    $scripts = array('turbotabs');

    foreach ($scripts as $script)
    {
        wp_dequeue_script($script);
    }
}

add_action('wp_print_scripts', 'baumchild_dequeue_script', 100);

add_action('init', function() {
    if (!isset($_GET['import']))
        return;

	// $products = baumchild_get_products();
	// $data = baumchild_manage_term_translations($products);
	// print_r(json_encode($data));

	// $cont = 1;
	// foreach (baumchild_get_products() as $key => $_product) {
	//     $product_id = $_product->ID;
	//     $products[] = baumchild_manage_imported_products_by_id($product_id, $additionals);
	//     $cont++;
	// }
	// $data['cont'] = $cont;
	// $data['products'] = $products;
	// print_r(json_encode($data));

	global $WOOF;
	$taxonomy_info = 'pa_poe';
	if (empty($string)) {
		if (is_object($taxonomy_info)) {
			$string = $WOOF->settings['custom_tax_label'][$taxonomy_info->name];
		}
	}
	$woof_settings = get_option('woof_settings');
	// $woof_settings['wpml_tax_labels']
	// var_dump($woof_settings);
	// print_r(json_encode($WOOF->settings['custom_tax_label']));
	// var_dump(get_term_by('term_taxonomy_id', $taxonomy_info), 'product_cat');
});
