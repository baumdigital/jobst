<?php
/**
 * Template Name: Sin encabezado
 * Template Post Type: Page
 */

get_header( 'page' ); ?>

	<div id="primary" class="content-area contenido">
		<main id="main" class="site-main">

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content/content-page-without-header' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
