<?php
/**
** Save current category before filter
** Fix filter title
**/
function baumchild_replace_archive_title( $title ) {
	if(is_shop()) {
		set_transient('wc_current_cat', '', 3600);
	}

	if(isset($_GET['really_curr_tax'])) {
		$term_id = str_replace('-product_cat', '', $_GET['really_curr_tax']);
		$cat = get_term_by('id', $term_id, 'product_cat');
	}

	if (is_product_category()) {
		global $wp_query;
		$cat = $wp_query->get_queried_object();
	}

	if(!empty($cat)) {
		$cat_data = array(
			'term' => $cat,
			'name' => $cat->name
		);
		set_transient('wc_current_cat', $cat_data, 3600);
	}

	if(isset($_GET['search']) && !empty(get_transient('wc_current_cat'))) {
		$title = get_transient('wc_current_cat')['name'];
	}

	return $title;
}
add_filter('woocommerce_page_title', 'baumchild_replace_archive_title');


/**
** Woof tax relation to AND
** Option in category filter configuration
**/
function baumchild_woof_main_query_tax_relations() {
	return array(
		'product_cat' => 'AND'
	);
}
add_filter('woof_main_query_tax_relations', 'baumchild_woof_main_query_tax_relations');