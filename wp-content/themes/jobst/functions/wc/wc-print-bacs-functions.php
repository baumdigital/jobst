<?php
/**
** Print order custom hook
**/
function baumchild_print_custom_data($order) {
	WC()->shipping->get_shipping_methods();
}
add_action('baumchild_print_custom_data', 'baumchild_print_custom_data');

/**
** BACS Payment order details
** Transaction BACS form
**/
function baumchild_check_bacs_accounts() {
	$bacs = new WC_Gateway_BACS();
	$bacs_accounts = apply_filters('woocommerce_bacs_accounts', $bacs->account_details);

	if(empty($bacs_accounts))
		return false;

	return $bacs_accounts;
}

function baumchild_check_bacs_payment_method($order) {
	if ( $order->get_payment_method() !== 'bacs') return;
	$order_id = $order->get_id();
	$bacs = new WC_Gateway_BACS();

	if(baumchild_check_bacs_accounts()) :
		?>
		<div class="order-bacs-payment">
			<?php
			$bacs->thankyou_page($order_id);
			?>
		</div>
		<p class="text-center toggle-accounts-container" id="bacs-transaction-top"><a href="#" class="btn-detalle-cuentas"><span class="toggle" style="display: none;">Ocultar</span><span class="toggle">Ver</span> detalle de cuentas</a></p>
		<?php
	endif;
	
	wc_get_template('order/bc-bacs-transferencia.php', array('order' => $order));		
}
add_action('baumchild_bacs_payment_method', 'baumchild_check_bacs_payment_method');

function baumchild_thankyou_bacs_payment_method($order_id) {
	$order = wc_get_order($order_id);
	if($order->get_payment_method() == 'bacs') {
		wc_get_template('order/bc-bacs-transferencia.php', array('order' => $order));
	}
}
add_action('woocommerce_thankyou_bacs', 'baumchild_thankyou_bacs_payment_method', 20);
add_action('custom_after_woocommerce_pay', 'baumchild_thankyou_bacs_payment_method', 20);

function baumchild_update_payment_button($actions, $order) {
	if ($order->needs_payment() && $order->get_payment_method() == 'bacs') {
		$actions['pay'] = array(
			'name' => __('N° transferencia', 'baumchild'),
			'url' => $order->get_view_order_url() . '#bacs-transaction-top',
		);
	}

	return $actions;
}
add_filter('woocommerce_my_account_my_orders_actions', 'baumchild_update_payment_button', 10, 2);

function baumchild_wc_status_valid_for_payment($statuses, $order) {
	$statuses[] = 'on-hold';

	return $statuses;
}
add_filter('woocommerce_valid_order_statuses_for_payment', 'baumchild_wc_status_valid_for_payment', 10, 2);

/**
** Save Transaction BACS
**/
function baumchild_bacs_payment_save_custom_data() {
	$bacs_transaction_code = $_POST['bacs_transaction_code'];
	$bacs_banco = $_POST['bacs_banco'];
	$order_id = $_POST['order_id'];
	$order = wc_get_order($order_id);
	$message = array();
	
	if(!empty($bacs_transaction_code)) {
		if(!empty($bacs_banco)) {
			update_post_meta($order_id, 'bacs_banco', $bacs_banco);
			$message[] = 'Banco: ' . $bacs_banco;
		}

		if(!empty($bacs_transaction_code)) {
			update_post_meta($order_id, 'bacs_transaction', $bacs_transaction_code);
			$message[] = 'Código: ' . $bacs_transaction_code;
		}
		

		if($order->has_status(array('on-hold', 'pending'))) {
			$order->add_order_note('Transacción bancaria realizada. ' . implode(', ', $message));
			$order->payment_complete();
		}
	}
	echo "POST: " . $bacs_transaction_code . " - Order: " . $order_id;

	wp_die();
}
add_action('wp_ajax_nopriv_baumchild_bacs_payment_save_custom_data', 'baumchild_bacs_payment_save_custom_data');
add_action('wp_ajax_baumchild_bacs_payment_save_custom_data', 'baumchild_bacs_payment_save_custom_data');

/**
** Extra Order fields data
**/
function baumchild_bacs_order_fields($order) {
	$order_id = $order->get_id();
	$banco = get_post_meta($order_id, 'bacs_banco', true);
	$transaction = get_post_meta($order_id, 'bacs_transaction', true);

	if(!empty($banco)) {
		echo '<p class="clean-p"><strong>' . __('Banco', 'baumchild') . ':</strong><span style="float: right;">' . $banco . '</span></p>';
	}

	if(!empty($transaction)) {
		echo '<p class="clean-p"><strong>' . __('N° Transacción', 'baumchild') . ':</strong><span style="float: right;">' . $transaction . '</span></p>';
	}
}
add_action('woocommerce_admin_order_data_after_billing_address', 'baumchild_bacs_order_fields', 30, 1);

/**
** Shows transaction ID into the order totals array
**/
function baumchild_reordering_bacs_item_totals($total_rows, $order, $tax_display) {
	$order_id = $order->get_id();
	$transaction = get_post_meta($order_id, 'bacs_transaction', true);

	if($order->get_payment_method() == 'bacs') {
		foreach ($total_rows as $key => $row) {
			$new_rows[$key] = $row;
			if($key == 'payment_method') {
				if(!empty($transaction)) {					
					$new_rows['transactionid'] = array(
						'label' => __('N° Transacción', 'baumchild'),
						'value' => $transaction
					);
				}
			}
		}
		$total_rows = $new_rows;
	}

	return $total_rows;
}
add_filter('woocommerce_get_order_item_totals', 'baumchild_reordering_bacs_item_totals', 10, 3);