<?php
/**
** Remove product price in loop
**/
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price');

/**
** Remove stock label
**/
add_filter('woocommerce_get_stock_html', '__return_empty_string');

/**
** Add Checkout form in cart page
**/
function baumchild_checkout_form_to_cart_page() {
	wc_get_template('cart/proceed-to-quote-button.php');
}
add_action('woocommerce_cart_collaterals', 'baumchild_checkout_form_to_cart_page');
remove_action('woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10);

/**
** Checkout review top title
** Function baumchild_checkout_payment_title removed
** 29-11-2019 -KMA
**/

/**
** Changing add to cart & order button text from plugin quotes_for_wc
**/
function baumchild_add_to_cart_button_text($button_text) {
	if(class_exists('quotes_for_wc')) {
		global $post;
		$post_id = $post->ID;
		$enable_quote = product_quote_enabled($post_id);

		if($enable_quote) {
			$button_text = __('Agregar a cotización', 'baumchild');
		}
	}

	return $button_text;
}
add_filter('woocommerce_product_single_add_to_cart_text', 'baumchild_add_to_cart_button_text', 99);
add_filter('woocommerce_product_add_to_cart_text', 'baumchild_add_to_cart_button_text', 99);

/**
** Change Process payment button text
**/
function baumchild_order_button_text() {
	if(class_exists('quotes_for_wc')) {
		return __('Enviar cotización', 'baumchild');
	}
}
add_filter('woocommerce_order_button_text', 'baumchild_order_button_text', 999);
add_filter('quotes_wc_order_button_text', 'baumchild_order_button_text');

/**
** Remove product price from template
**/
function baumdigital_product_price_display($price) {
	return '';
}
add_filter( 'woocommerce_get_price_html', 'baumdigital_product_price_display' );
add_filter( 'woocommerce_cart_item_price', 'baumdigital_product_price_display' );
// add_filter( 'woocommerce_cart_item_subtotal', 'baumdigital_product_price_display' );

/**
** Set default product minimum price and enable quotes for product in admin
**/
function baumchild_set_product_default_price( $post_id, $post ) {
	$product = wc_get_product($post_id);
	$already_set = get_post_meta($post_id, '_set_default_price', true);
	$price = $product->get_price();

	if ('yes' !== $already_set) {
		if(empty($price)) {
			$product->set_regular_price('100');
			$product->save();
		}

		update_post_meta($post_id, '_set_default_price', 'yes');
		update_post_meta($post_id, 'qwc_enable_quotes', 'on');
	}
}
add_action('woocommerce_process_product_meta', 'baumchild_set_product_default_price', 999, 2);

/**
** Empty order items totals
**/
function baumchild_quote_order_item_totals($total_rows, $order, $tax_display) {
	if(class_exists('quotes_for_wc')) {
		$total_rows = array();
	}

	return $total_rows;
}
add_filter('woocommerce_get_order_item_totals', 'baumchild_quote_order_item_totals', 10, 3);

/**
** Quote new order status
**/
function baumchild_quote_order_status() {
	$quoted_status = array(
		'label' => __('Cotizado', 'baumchild'),
		'public' => true,
		'show_in_admin_status_list' => true,
		'show_in_admin_all_list' => true,
		'exclude_from_search' => false,
		'label_count' => _n_noop('Cotizaciones <span class="count">(%s)</span>', 'Cotización <span class="count">(%s)</span>')
	);
	$on_quote_status = array(
		'label' => __('Pendiente de cotizar', 'baumchild'),
		'public' => true,
		'show_in_admin_status_list' => true,
		'show_in_admin_all_list' => true,
		'exclude_from_search' => false,
		'label_count' => _n_noop('Pendiente de cotizar <span class="count">(%s)</span>', 'Pendiente de cotizar <span class="count">(%s)</span>')
	);

	// register_post_status('wc-quote-pending', $on_quote_status);
	register_post_status('wc-quote-complete', $quoted_status);
	register_post_status('quote-sent', $quoted_status);
}
add_action('init', 'baumchild_quote_order_status');

function baumchild_add_quote_to_order_statuses($order_statuses) {
	$new_order_statuses = array();

	foreach($order_statuses as $key => $status) {
		$new_order_statuses[ $key ] = $status;
		if ('wc-on-hold' === $key) {
			// $new_order_statuses['wc-quote-pending'] = __('Pendiente de cotizar', 'baumchild');
			$new_order_statuses['wc-quote-complete'] = __('Cotizado', 'baumchild');
		}
	}

	return $new_order_statuses;
}
add_filter('wc_order_statuses', 'baumchild_add_quote_to_order_statuses');

/**
** Admin quote styles
**/
function baumchild_admin_quote_styles() {
	?>
	<style type="text/css">
		.order-status.status-pending,
		.order-status.status-quote-pending {
			background: #8d827c;
			color: #fff;
		}
		.order-status.status-quote-complete {
			background: #DB7139;
			color: #fff;
		}
	</style>
	<?php
}
add_action('admin_head', 'baumchild_admin_quote_styles');

/**
** Print content option
** woocommerce-delivery-notes
**/
function baumchild_print_options_array($order_info) {
	unset($order_info['payment_method']);

	$order_info['order_number']['label'] = __('Número de cotización', 'baumchild');
	$order_info['order_date']['label'] = __('Fecha de cotización', 'baumchild');

	return $order_info;
}
add_filter('wcdn_order_info_fields', 'baumchild_print_options_array');

/**
** Endpoint titles
**/
function baumchild_change_endpoint_titles($title) {
	if ( ! baum_is_plugin_activated( 'woocommerce/woocommerce.php' ) ) {
		return $title;
	}

	if (is_wc_endpoint_url('order-received') && in_the_loop()) {
		$title = __('Cotización recibida', 'baumchild');
	} elseif (is_wc_endpoint_url('orders') && in_the_loop()) {
		$title = __('Cotizaciones', 'baumchild');
	} elseif (is_wc_endpoint_url('edit-address') && in_the_loop()) {
		$title = __('Dirección principal', 'baumchild');
	}

	return $title;
}
add_filter('the_title', 'baumchild_change_endpoint_titles', 10, 2);

/**
** Thank you page top message
**/
function baumchild_thankyou_order_received_text() {
	return __('Gracias, su cotización ha sido recibida.', 'baumchild');
}
// add_filter('woocommerce_thankyou_order_received_text', 'baumchild_thankyou_order_received_text');

/**
** Remove order details table in Thankyou page
**/
remove_action('woocommerce_thankyou', 'woocommerce_order_details_table', 10);

/**
** My account > Orders table columns
**/
function baumchild_my_account_my_orders_columns($columns) {
	$columns['order-number'] = __('Cotizaciones', 'baumchild');
	$columns['order-total'] = __('Ítems', 'baumchild');

	return $columns;
}
add_filter('woocommerce_my_account_my_orders_columns', 'baumchild_my_account_my_orders_columns');

/**
** WooCommerce edit address slugs
**/
function baumchild_wc_edit_address_slugs($array) {
	$array['billing'] = __('principal', 'baumchild');
	$array['shipping'] = __('secundaria', 'baumchild');

	return $array; 
}
add_filter('woocommerce_edit_address_slugs', 'baumchild_wc_edit_address_slugs', 10); 

/**
** View Order endpoint title
**/
function baumchild_view_order_title($title) {
	global $wp;
	$order = wc_get_order($wp->query_vars['view-order']);
	$title = ($order) ? sprintf(__('Cotización #%s', 'baumchild'), $order->get_order_number()) : '';

	return $title;
}
add_filter('woocommerce_endpoint_view-order_title', 'baumchild_view_order_title');

/**
** Remove filter that hides order item prices for email layout
** Quotes for WooCommerce - 24-12-2019 - KMA
**/
function baumchild_remove_filters_from_wc_quotes() {
	global $quotes_for_wc;

	remove_filter('woocommerce_order_formatted_line_subtotal', array($quotes_for_wc, 'qwc_remove_cart_checkout_prices'));
}
add_action('init', 'baumchild_remove_filters_from_wc_quotes');

/**
** Use Pending payment status instead of Pending quote
** Quotes for WooCommerce - 24-12-2019 - KMA
**/ 
function baumchild_thankyou_change_order_status($order_id) {
	if( ! $order_id ) return;

	$order = wc_get_order($order_id);
	$order->update_status('pending');
}
add_action('woocommerce_thankyou', 'baumchild_thankyou_change_order_status');

/**
** Rename Pending payment order status
** 24-12-2019 - KMA
**/
function baumchild_rename_pending_order_status($order_statuses) {
	foreach ($order_statuses as $key => $status) {
		if ('wc-pending' === $key) {
			$order_statuses['wc-pending'] = _x('Pendiente de cotización', 'Order status', 'baumchild');
		}
	}
	return $order_statuses;
}
add_filter('wc_order_statuses', 'baumchild_rename_pending_order_status');