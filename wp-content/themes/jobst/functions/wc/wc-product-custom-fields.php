<?php
/**
** WC Custom product tab
**/
function wc_misc_baum_product_data_tab( $product_data_tabs ) {
	$product_data_tabs['misc_baum-tab'] = array(
		'label' => __('Misc. Baum', 'baumchild'),
		'target' => 'misc_baum_product_data',
	);
	return $product_data_tabs;
}
add_filter('woocommerce_product_data_tabs', 'wc_misc_baum_product_data_tab' , 99 , 1);

function add_my_custom_product_data_fields() {
	global $woocommerce, $post;
	// $video = get_post_meta($post->ID, 'video', true);
	// $ficha_tecnica = get_post_meta($post->ID, '_ficha_tecnica', true);
	// $marca = get_post_meta($post->ID, '_marca', true);
	?>
	<div id="misc_baum_product_data" class="panel woocommerce_options_panel">
		<?php
		/*
		 * Uncomment for show default fields for the starter WordPress theme
		 * woocommerce_wp_text_input( array(
			'id' => '_ficha_tecnica',
			// 'wrapper_class' => 'show_if_simple',
			'label' => __('Ficha técnica', 'baumchild'),
			'description' => __('URL de la ficha técnica.', 'baumchild'),
			'default' => '0',
			'desc_tip' => false,
			'style' => 'width: 100%'
		));

		woocommerce_wp_text_input( array(
			'id' => '_marca',
			// 'wrapper_class' => 'show_if_simple',
			'label' => __('Logo de Marca', 'baumchild'),
			'description' => __('URL para el logo de marca', 'baumchild'),
			'default' => '0',
			'desc_tip' => false,
			'style' => 'width: 100%'
		));

		woocommerce_wp_text_input( array(
			'id' => 'video',
			// 'wrapper_class' => 'show_if_simple',
			'label' => __('Video ID', 'baumchild'),
			'description' => __('Agregue un video para el producto', 'baumchild'),
			'default' => '0',
			'desc_tip' => false,
			'style' => 'width: 100%'
		));*/

		woocommerce_wp_text_input( array(
				'id'          => '_know_my_size',
				'label'       => __( 'Ficha de talla', 'baumchild' ),
				'description' => __( 'URL de la ficha de talla.', 'baumchild' ),
				'default'     => '0',
				'desc_tip'    => false,
				'style'       => 'width: 100%'
		) );

		woocommerce_wp_text_input( array(
				'id'          => '_know_compression',
				'label'       => __( 'Ficha de compresión', 'baumchild' ),
				'description' => __( 'URL de la ficha de compresión.', 'baumchild' ),
				'default'     => '0',
				'desc_tip'    => false,
				'style'       => 'width: 100%'
		) );

		woocommerce_wp_text_input( array(
				'id'          => '_video_tutorial',
				'label'       => __( 'Video', 'baumchild' ),
				'description' => __( 'URL del video tutorial de Youtube', 'baumchild' ),
				'default'     => '0',
				'desc_tip'    => false,
				'style'       => 'width: 100%'
		) );
		?>
	</div>
	<?php
}
add_action('woocommerce_product_data_panels', 'add_my_custom_product_data_fields');

// Save Fields
function wc_add_custom_general_fields_save( $post_id ) {
	update_post_meta( $post_id, '_ficha_tecnica', esc_attr( stripslashes( $_POST['_ficha_tecnica'] ) ) );
	update_post_meta( $post_id, '_marca', esc_attr( stripslashes( $_POST['_marca'] ) ) );
	update_post_meta( $post_id, 'video', esc_attr( stripslashes( $_POST['video'] ) ) );
	update_post_meta( $post_id, '_know_my_size', esc_attr( stripslashes( $_POST['_know_my_size'] ) ) );
	update_post_meta( $post_id, '_know_compression', esc_attr( stripslashes( $_POST['_know_compression'] ) ) );
	update_post_meta( $post_id, '_video_tutorial', esc_attr( stripslashes( $_POST['_video_tutorial'] ) ) );
}

add_action( 'woocommerce_process_product_meta', 'wc_add_custom_general_fields_save' );

/**
** Add a bit of style.
**/
function wcpp_custom_style() {
	?><style>
		#woocommerce-product-data ul.wc-tabs li.misc_baum-tab_options a { background: #db7139; color: #fff; }
		/*#woocommerce-product-data ul.wc-tabs li.misc_baum-tab_options a:before { content: '\f126'; }*/
	</style><?php
}
add_action('admin_head', 'wcpp_custom_style');

/**
** Product Video Custom Field
**/
function baumchild_template_product_video() {
	global $product;
	$video = get_post_meta($product->get_id(), 'video', true);

	if(!empty($video)) :
		?>
		<div class="product-video">
			<div class="product-video-wrapper">
				<div class="responsive-iframe">
					<iframe width="560" height="315" src="//www.youtube.com/embed/<?= $video ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
			</div>
		</div>

		<?php
	endif;
}
add_action('woocommerce_after_single_product_summary', 'baumchild_template_product_video', 20);

/**
** Custom Fields
** Datatell - KMA
**/
function baumchild_wc_template_pdf_link() {
	global $product;

	$product_id = $product->get_id();
	$ficha_tecnica = get_post_meta($product_id, '_ficha_tecnica', true);
	$ficha_tecnica = (wp_remote_fopen($ficha_tecnica)) ? $ficha_tecnica : '';
	?>
	<hr class="hr">
	<?php if(!empty($ficha_tecnica)) : ?>
		<div class="pdf-wrapper">
			<a href="<?= esc_url($ficha_tecnica) ?>" target="_blank" download class="btn btn-pdf"><i class="fa fa-download"></i> <?= __('Descargar ficha técnica', 'baumchild') ?></a>
		</div>
		<?php
	endif;
}

add_action('woocommerce_single_product_summary', 'baumchild_wc_template_pdf_link', 45);
add_action('xoo-qv-summary', 'baumchild_wc_template_pdf_link', 30);

function baumchild_wc_template_brand_image() {
	global $product;

	$product_id = $product->get_id();
	$marca = get_post_meta($product_id, '_marca', true);
	$marca = (wp_remote_fopen($marca)) ? $marca : '';

	if(!empty($marca)) :
		?>
		<div class="brand-wrapper">
			<span class="__temp">
				<img width="150" src="<?= esc_url($marca) ?>" alt="<?php bloginfo('name') ?>">
			</span>
		</div>
		<?php
	endif;
}

add_action('woocommerce_single_product_summary', 'baumchild_wc_template_brand_image', 4);
add_action('xoo-qv-summary', 'baumchild_wc_template_brand_image', 4);

/**
** Quick view link product details
** Removed - 26-12-2019 - KMA
**/
remove_action('xoo-qv-summary', 'xoo_qv_product_info', 35);

/**
 * Open container for Baum products custom fields
 */
function baumchild_wc_template_container_open() {
	?>
	<hr class="hr">
	<div class="baum-product-container-custom-fields">
	<?php
}
add_action( 'woocommerce_single_product_summary', 'baumchild_wc_template_container_open', 35 );

/**
 * Custom Fields - Know my size
 */
function baumchild_wc_template_know_my_size() {
	global $product;

	$product_id   = $product->get_id();
	$know_my_size = get_post_meta( $product_id, '_know_my_size', true );
	$know_my_size = ( wp_remote_fopen( $know_my_size ) ) ? $know_my_size : '';
	?>
	<?php if ( ! empty( $know_my_size ) ) : ?>
		<div class="know-my-size-wrapper baum-product-custom-fields">
			<button type="button" class="btn btn-info btn-know-my-size" data-toggle="modal" data-target="#know-my-size"><?php echo __('No sé mi talla', 'baumchild' ); ?></button>

			<!-- Modal know-my-size -->
			<div id="know-my-size" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
						</div>
						<div class="modal-body">
							<img src="<?php echo esc_url( $know_my_size ); ?>" alt="Talla adecuada" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
	endif;
}

add_action( 'woocommerce_single_product_summary', 'baumchild_wc_template_know_my_size', 35 );

/**
 * Custom Fields - Know compression
 */
function baumchild_wc_template_know_compression() {
	global $product;

	$product_id   = $product->get_id();
	$know_compression = get_post_meta( $product_id, '_know_compression', true );
	$know_compression = ( wp_remote_fopen( $know_compression ) ) ? $know_compression : '';
	?>
	<?php if ( ! empty( $know_compression ) ) : ?>
		<div class="know-compression-wrapper baum-product-custom-fields">
			<button type="button" class="btn btn-info btn-know-compression" data-toggle="modal" data-target="#know-compression"><?php echo __('No sé el nivel de compresión', 'baumchild' ); ?></button>

			<!-- Modal know-compression -->
			<div id="know-compression" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
						</div>
						<div class="modal-body">
							<img src="<?php echo esc_url( $know_compression ); ?>" alt="Tabla de compresión" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
	endif;
}

add_action( 'woocommerce_single_product_summary', 'baumchild_wc_template_know_compression', 35 );

/**
 * Custom Fields - Know compression
 */
function baumchild_wc_template_video_tutorial() {
	global $product;

	$product_id   = $product->get_id();
	$video_tutorial = get_post_meta( $product_id, '_video_tutorial', true );
	$video_tutorial = ( wp_remote_fopen( $video_tutorial ) ) ? $video_tutorial : '';
	?>
	<?php if ( ! empty( $video_tutorial ) ) : ?>
		<div class="video-tutorial-wrapper baum-product-custom-fields">
			<button type="button" class="btn btn-info btn-video-tutorial" data-toggle="modal" data-target="#video-tutorial"><?php echo __('¿Cómo colocar una media?', 'baumchild' ); ?></button>

			<!-- Modal video-tutorial -->
			<div id="video-tutorial" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
						</div>
						<div class="modal-body">
							<?php
							$embed_code = wp_oembed_get( $video_tutorial );
							echo $embed_code;
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
	endif;
}

add_action( 'woocommerce_single_product_summary', 'baumchild_wc_template_video_tutorial', 35 );

/**
 * Close container for Baum products custom fields
 */
function baumchild_wc_template_container() {
	?>
	</div><!-- baum-product-container-custom-fields -->
	<?php
}
add_action( 'woocommerce_single_product_summary', 'baumchild_wc_template_container', 35 );
