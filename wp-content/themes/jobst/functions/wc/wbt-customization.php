<?php
/**
** Woo Bought Together in Quick view
**/
add_action('xoo-qv-summary', function() {
	?>
	<div class="additional-products-wrapper"></div>
	<?php
}, 29);

/**
** Action after buttons in popup added to cart notification
**/
function baumchild_after_added_to_cart_buttons_notification() {
	global $product;
	$added_to_cart_message = apply_filters('get_option_baumchild_added_to_cart_popup_message', get_theme_mod('baumchild_added_to_cart_popup_message'));

	if(is_null($product)) {
		$current_page = sanitize_post($GLOBALS['wp_the_query']->get_queried_object());
		$page_id = $current_page->ID;
		$product = wc_get_product($page_id);
	}

	ob_start();
	if(!empty($added_to_cart_message)) {
		?>
		<div class="added-to-cart-message">
			<?= wpautop($added_to_cart_message) ?>
		</div>
		<?php
	}

	// echo baumchild_wbt_custom_form($product, true);
	?>
	<div class="added-to-cart-additionals"></div>
	<?php

	echo ob_get_clean();
}
add_action('xoo_cp_after_btns', 'baumchild_after_added_to_cart_buttons_notification');

/**
** Ajax check for selected additional products
**/
function baumchild_wbt_custom_items() {
	$product_id = $_POST['product_id'];
	$products = $variations = array();

	if ( $woobt_ids = get_post_meta( $product_id, 'woobt_ids', true ) ) {
		$woobt_items = explode( ',', $woobt_ids );
	}
	foreach ( $woobt_items as $woobt_item ) {
		$woobt_item_arr = explode( '/', $woobt_item );
		$woobt_item_id  = absint( isset( $woobt_item_arr[0] ) ? $woobt_item_arr[0] : 0 );
		$woobt_product  = wc_get_product( $woobt_item_id );
		$available_variations = $woobt_product->get_available_variations();

		foreach ($available_variations as $key => $variation) {
			foreach ($variation['attributes'] as $key_attribute => $variation_attributes) {
				$variations[$variation['variation_id']] = array(
					$key_attribute => $variation_attributes
				);
			}
		}
	}

	$woobt_ids = explode( ',', $_POST['woobt_ids'] );
	foreach ( $woobt_ids as $woobt_item ) {
		$woobt_item_arr = explode( '/', $woobt_item );
		$woobt_item_id = absint( isset( $woobt_item_arr[0] ) ? $woobt_item_arr[0] : 0 );
		$products[] = $variations[$woobt_item_id];
	}

	wp_send_json(array(
		'products' => $products,
		'additional_products' => baumchild_wbt_custom_form($_POST['product_id'], true)
	));
}
add_action('wp_ajax_nopriv_baumchild_wbt_custom_items', 'baumchild_wbt_custom_items');
add_action('wp_ajax_baumchild_wbt_custom_items', 'baumchild_wbt_custom_items');

/**
** Get additional products with Ajax
**/
function baumchild_get_additional_products() {
	$data = array();

	if(isset($_POST['product_id'])) {
		$data['additional_products'] = baumchild_wbt_custom_form($_POST['product_id']);
	}

	wp_send_json($data);
}
add_action('wp_ajax_nopriv_baumchild_get_additional_products', 'baumchild_get_additional_products');
add_action('wp_ajax_baumchild_get_additional_products', 'baumchild_get_additional_products');

/**
** Custom additional products form template
**/
function baumchild_wbt_custom_form($product, $description = false) {
	if(!$product || is_null($product))
		return;

	// $product_id = !is_int($product) ? $product->get_id() : $product;
	$product = wc_get_product($product);
	$product_id = $product->get_id();

	if ( $woobt_ids = get_post_meta( $product_id, 'woobt_ids', true ) ) {
		$woobt_items = explode( ',', $woobt_ids );
	}

	if(count($woobt_items) > 0) : ob_start(); ?>
		<div class="variations_form-wrapper">
			<?php
			foreach ( $woobt_items as $woobt_item ) {
				$woobt_item_arr = explode( '/', $woobt_item );
				$woobt_item_id  = absint( isset( $woobt_item_arr[0] ) ? $woobt_item_arr[0] : 0 );
				$woobt_product  = wc_get_product( $woobt_item_id );

				$woobt_checked_individual = apply_filters( 'woobt_checked_individual', false, $woobt_item_id, $product_id );
				if ( $woobt_product->is_in_stock() ) {
					$woobt_product_name = $woobt_product->get_name();
				} else {
					$woobt_product_name = '<s>' . $woobt_product->get_name() . '</s>';
				}

				if ( $woobt_product->is_type( 'variable' ) ) {
					$attributes = $woobt_product->get_variation_attributes();
					$available_variations = $woobt_product->get_available_variations();

					if ( is_array( $attributes ) && ( count( $attributes ) > 0 ) ) { ?>
						<div class="wbt-custom-product">
							<div class="variations_form" data-product_id="<?= absint( $woobt_product->get_id() ) ?>" data-product_variations="<?= htmlspecialchars( wp_json_encode( $available_variations ) ) ?>">

								<?php foreach ( $attributes as $attribute_name => $options ) : ?>
									<?php
									$selected = isset( $_REQUEST[ 'attribute_' . sanitize_title( $attribute_name ) ] ) ? wc_clean( stripslashes( urldecode( $_REQUEST[ 'attribute_' . sanitize_title( $attribute_name ) ] ) ) ) : $woobt_product->get_variation_default_attribute( $attribute_name );
									?>
									<table class="variations" data-options="<?= htmlspecialchars( wp_json_encode( $options ) ) ?>">
										<tbody>
											<tr>
												<?php if ($description) : ?>
													<td class="label">
														<label for="<?= $attribute_name ?>"><?php echo wc_attribute_label( $attribute_name ); ?></label>
														<div class="woobt-description"><?= wpautop($woobt_product->get_short_description()) ?></div>
													</td>
												<?php endif; ?>
												<td class="value" data-selected="<?= $attribute_name ?>" data-options="<?= json_encode($options) ?>">
													<?php
													wc_dropdown_variation_attribute_options( array(
														'options' => $options,
														'attribute' => $attribute_name,
														'product' => $woobt_product,
													) );
													?>
												</td>
											</tr>
										</tbody>
									</table>
								<?php endforeach; ?>

							</div>
						</div>
						<?php
					}
				} else {
					echo wc_get_stock_html( $woobt_product );
				}
			}
			?>
		</div>
		<?php
		return ob_get_clean();
	endif;
}

/**
 * Clean option none from Woo Bought Together
 */
function baumchild_dropdown_variation_attribute_options_args( $args ) {
	$option_none = '';

	if ( ! empty( $args['attribute'] ) && function_exists( 'pl__' ) ) {
		$get_name    = get_taxonomy( $args['attribute'] );
		$option_none = ( ! empty( $get_name->labels->singular_name ) ) ? pll__( $get_name->labels->singular_name ) : '';
	}

	$args['show_option_none'] = $option_none;

	return $args;
}

add_filter( 'woocommerce_dropdown_variation_attribute_options_args', 'baumchild_dropdown_variation_attribute_options_args' );

/**
** WBT Associated Post titles
**/
function baumchild_wbt_post_states($html, $woobt_count, $_post_id) {
	$woobt_ids = '';
	$woobt_ids = get_post_meta($_post_id, 'woobt_ids', true);
	$woobt_ids = explode(',', $woobt_ids);

	if (!empty($woobt_ids)) {
		$titles = array();
		foreach ($woobt_ids as $key => $the_id) {
			$the_id = explode('/', $the_id);
			$titles[] = get_the_title($the_id[0]);
		}

		return '<span class="woobt-state">' . sprintf( esc_html__( 'Associate (%s)', 'woobt' ), $woobt_count ) . '<br>' . implode(', ', $titles) . '</span>';
	}

	return $html;
}
add_filter('woobt_post_states', 'baumchild_wbt_post_states', 15, 3);

/**
** Empty cart
**/
add_action('init', function() {

	if(isset($_GET['empty'])) {
		WC()->cart->empty_cart();
	}
});
