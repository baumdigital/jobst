<?php
/**
** Baumchild Admin Import Class
** TODO: Crear el atributo en inglés si no existe
**/
class Baumchild_Admin_Import {
	public function __construct() {
		$this->id = 'baumchild';

		add_action('admin_menu', array($this, 'baumchild_admin_page'));
	}

	public function baumchild_admin_page() {
		add_options_page('Manage Products by Baum', 'Manage Products by Baum', 'manage_options', $this->id, array($this, 'baumchild_settings_page'));
	}

	public function baumchild_settings_page() {
		?>
		<div class="wrap">
			<h1><?= __('Manage Products by Baum', 'baumchild') ?></h1>
			<h4><?= __('Manejo de productos luego de importarlos desde el .csv.', 'baumchild') ?></h4>
			<p><?= __('Seguir los pasos en orden para realizar la conversión de manera óptima.', 'baumchild') ?></p>
			<form action="options.php" id="wpcd-form" method="post">
				<?php settings_fields($this->id . '-plugin-settings'); ?>

				<hr>

				<?php do_action('baumchild_before_table_settings'); ?>

				<table class="form-table">
					<tr>
						<th><?= __('1. Productos adicionales', 'baumchild') ?></th>
						<td>
							<div class="manual-execution">
								<button class="btn-manage-products button button-small" data-function="baumchild_adicionales"><?= __('Ejecutar', 'baumchild') ?></button>
								<i class="fa fa-2x fa-spin fa-spinner loading" style="display: none;"></i>
								<p class="description"><?= __('Asociación de adicionales por producto.', 'baumchild') ?></p>
								<div class="manual-response" style="max-height: 150px; max-width: 800px; overflow-y: scroll;"></div>
							</div>
						</td>
					</tr>
					<tr>
						<th><?= __('2. Idioma y traducciones de los Terms', 'baumchild') ?></th>
						<td>
							<div class="manual-execution">
								<button class="btn-manage-products button button-small button-disabled" data-function="baumchild_manage_terms"><?= __('Ejecutar', 'baumchild') ?></button>
								<i class="fa fa-2x fa-spin fa-spinner loading" style="display: none;"></i>
								<p class="description"><?= __('Asociación de los atributos (terms) por idioma y traducciones.', 'baumchild') ?></p>
								<div class="manual-response" style="max-height: 150px; max-width: 800px; overflow-y: scroll;"></div>
							</div>
						</td>
					</tr>
					<tr>
						<th><?= __('3. Idioma y traducciones de productos', 'baumchild') ?></th>
						<td>
							<div class="manual-execution">
								<button class="btn-manage-products button button-small button-disabled" data-function="baumchild_manage_products"><?= __('Ejecutar', 'baumchild') ?></button>
								<i class="fa fa-2x fa-spin fa-spinner loading" style="display: none;"></i>
								<p class="description"><?= __('Asociación de productos por idioma.', 'baumchild') ?></p>
								<div class="manual-response" style="max-height: 150px; max-width: 800px; overflow-y: scroll;"></div>
							</div>
						</td>
					</tr>
				</table>
				<hr>

				<?php do_action('baumchild_after_table_settings'); ?>
			</form>
		</div>
		<script type="text/javascript">
			jQuery(function($) {
				$(document).on('click', '.btn-manage-products', function() {
					if($(this).hasClass('button-disabled'))
						return false;

					var data = {
						action: 'baumchild_manage_imported_products',
						function: $(this).data('function')
					}

					if($(this).data('order')) {
						data.order_id = $(this).data('order');
					}

					var response_wrapper = $(this).closest('.manual-execution').find('.manual-response');
					var loading = $(this).closest('.manual-execution').find('.loading');
					loading.show();

					$.post(ajaxurl, data)
					.done(function (response) {
						response_wrapper.html('<pre>' + JSON.stringify(response) + '</pre>');
						$('.btn-manage-products.button-disabled').first().removeClass('button-disabled');
					})
					.fail(function (error) {
						console.log(JSON.stringify(error));
						response_wrapper.html('<p class="error">Algo ocurrió con la respuesta.</p>');
					})
					.always(function() {
						loading.hide();
					});

					return false;
				});
			});
		</script>
		<?php
	}
}
new Baumchild_Admin_Import();


function baumchild_manage_imported_products() {
	$additionals = false;
	$data = array();

	switch ($_POST['function']) {
		case 'baumchild_adicionales':
			$additionals = true;
			$cont = 1;
			foreach (baumchild_get_products() as $key => $_product) {
				$product_id = $_product->ID;
				$products[] = baumchild_manage_imported_products_by_id($product_id, $additionals);
				$cont++;
			}
			$data['cont'] = $cont;
			$data['products'] = $products;
		break;
		
		case 'baumchild_manage_terms':
			$products = baumchild_get_products();
			$data = baumchild_manage_term_translations($products);
		break;
		case 'baumchild_manage_products':
			$cont = 1;
			foreach (baumchild_get_products() as $key => $_product) {
				$product_id = $_product->ID;
				$products[] = baumchild_manage_imported_products_by_id($product_id, $additionals);
				$cont++;
			}
			$data['cont'] = $cont;
			$data['products'] = $products;
		break;
		// case 'baumchild_manage_translations':
		// 	$cont = 1;
		// 	foreach (baumchild_get_products() as $key => $_product) {
		// 		$product_id = $_product->ID;
		// 		$products[] = baumchild_set_product_translation($product_id);
		// 		$cont++;
		// 	}
		// 	$data['cont'] = $cont;
		// 	$data['products'] = $products;
		// break;
		default:
			$data = __('No hay respuesta', 'baumchild');
		break;
	}
	
	wp_send_json($data);
}
add_action('wp_ajax_nopriv_baumchild_manage_imported_products', 'baumchild_manage_imported_products');
add_action('wp_ajax_baumchild_manage_imported_products', 'baumchild_manage_imported_products');

/**
** Get products with custom query
**/
function baumchild_get_products($args = array()) {
	$defaults = array(
		'post_type' => array('product_variation', 'product'),
		'post_status' => 'publish',
		'lang' => implode(',', baumchild_current_languages()),
		'showposts' => -1
	);
	$parsed_args = wp_parse_args($args, $defaults);

	return get_posts($parsed_args);
}

/**
** Get product data by ID
**/
function baumchild_manage_imported_products_by_id($_product_id, $additionals = false) {
	$cur_product = array();
	$the_product = wc_get_product($_product_id);
	
	$cur_product = array(
		'ID' => $_product_id,
		'name' => $the_product->get_name(),
		'type' => $the_product->get_type(),
		'attributes' => $the_product->get_attributes()
	);

	if($additionals) {
		$cur_product['additionals'] = baumchild_set_additional_products($_product_id);
	} else {
		$cur_product['translation'] = baumchild_set_product_language_and_translation($_product_id);
		$cur_product['quotes'] = baumchild_set_quote_options($_product_id);
	}
		
	return $cur_product;
}

/**
** Set additional products by ID
**/
function baumchild_set_additional_products($_product_id) {
	$woobt_ids = array();
	$soporte = get_post_meta($_product_id, '_soporte_asociado', true);
	$licencia = get_post_meta($_product_id, '_licencia_asociado', true);
	$woobt_product_id = get_post_meta($_product_id, 'woobt_product_id', true);
	$soporte_id = !empty($soporte) ? wc_get_product_id_by_sku($soporte) : 0;
	$licencia_id = !empty($licencia) ? wc_get_product_id_by_sku($licencia) : 0;

	// woobt_ids
	// 908/100%/1,850/100%/1
	if($soporte_id > 0) {
		$woobt_ids[] = $soporte_id . '/100%/1';
	}

	if($licencia_id > 0) {
		$woobt_ids[] = $licencia_id . '/100%/1';
	}

	if(!empty($woobt_ids) && empty($woobt_product_id)) {
		update_post_meta($_product_id, 'woobt_ids', implode(',', $woobt_ids));
		update_post_meta($_product_id, 'woobt_checked_all', 'off');
		update_post_meta($_product_id, 'woobt_separately', 'on');
		update_post_meta($_product_id, 'woobt_custom_qty', 'off');
		update_post_meta($_product_id, 'woobt_sync_qty', 'off');
		update_post_meta($_product_id, 'woobt_limit_each_min_default', 'off');
		update_post_meta($_product_id, 'woobt_product_id', $_product_id);
	}

	return array(
		'additionals' => $woobt_ids
	);
}

/**
** Set language and translation by ID
**/
function baumchild_set_product_language_and_translation($_product_id) {
	$lang = strtolower(get_post_meta($_product_id, '_lang', true));
	$_translation = get_post_meta($_product_id, '_translation', true);
	$translation_id = !empty($_translation) ? wc_get_product_id_by_sku($_translation) : 0;
	$translation = $taxonomies = array();
	$langs = baumchild_current_languages();
	$the_product = wc_get_product($_product_id);

	if($translation_id > 0) {
		foreach ($langs as $key => $_lang) {
			if($lang == $_lang) {
				$translation[$_lang] = (int) $_product_id;
			} else {
				$translation[$_lang] = (int) $translation_id;
			}
		}
	}

	if(!empty($lang) && in_array($lang, $langs)) {
		$language = pll_set_post_language($_product_id, $lang);

		if(!empty($translation))
			$the_translation = pll_save_post_translations($translation);
	}

	do_action('baumchild_after_set_product_language_and_translation', $_product_id);

	return array(
		'lang' => $lang,
		'translation' => $translation,
		'taxonomies' => $taxonomies
	);
}

/**
** Manage the term language and translations
**/
function baumchild_manage_term_translations($products) {
	$the_products = $term_translations = array();
	foreach ($products as $key => $_product) {
		$product_id = $_product->ID;
		$the_product = wc_get_product($product_id);

		$lang = strtolower(get_post_meta($product_id, '_lang', true));
		$_translation = get_post_meta($product_id, '_translation', true);
		$translation_id = !empty($_translation) ? wc_get_product_id_by_sku($_translation) : 0;
		
		if(!empty($lang)) {
			if(!empty($the_product->get_attributes())) {
				$the_term = array();
				foreach ($the_product->get_attributes() as $attr_name => $attr) {
					if(!is_string($attr)) {
						foreach( $attr->get_terms() as $term ) {
							if(!empty($term->term_id)) {
								$the_term[$term->term_id] = $term->name;
								pll_set_term_language($term->term_id, $lang);
							}
						}
					}
				}
			}
			$the_products[$product_id] = array(
				'lang' => $lang,
				'translation' => $translation_id,
				'terms' => $the_term
			);
		}
	}

	$term_translations['the_products'] = $the_products;
	$term_translations['terms'] = baumchild_pll_save_term_translations($the_products);
	return $term_translations;
}

/**
** Save Polylang term translations
**/
function baumchild_pll_save_term_translations($product_terms) {
	$term_result = array();
	foreach ($product_terms as $product_id => $_product) {
		if(!empty($_product['terms'])) {
			$translations = $product_terms[$_product['translation']]['terms'];
			$translation_lang = $product_terms[$_product['translation']]['lang'];
			$this_term = $_product['terms'];

			if(!empty($translations) && !empty($this_term)) {
				$this_term = array_keys($this_term);
				$translations = array_keys($translations);

				for ($i=0; $i < count($this_term); $i++) { 
					$term_language = array( $_product['lang'] => $this_term[$i], $translation_lang => $translations[$i]);
					$term_result[] = $term_language;
					// var_dump($term_language);
					// wp_die();
					pll_save_term_translations($term_language);
				}
			}
		}
	}

	return $term_result;
}

/**
** Set quote options by ID
**/
function baumchild_set_quote_options($_product_id) {
	update_post_meta($_product_id, '_set_default_price', 'yes');
	update_post_meta($_product_id, 'qwc_enable_quotes', 'on');

	return true;
}

/**
** Get current languages by Polylang
**/
function baumchild_current_languages() {
	global $polylang;

	foreach ($polylang->get_languages_list() as $term) {
		$langs[] = $term->slug;
	}

	return $langs;
}


// add_action('init', function() {
// 	if (is_admin())
// 		return;

// 	var_dump(baumchild_set_product_language_and_translation(2995));
// });