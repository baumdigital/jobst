<?php
/**
** Polylang Theme mod options
** Register some string from the customizer to be translated with Polylang
**/
if ( function_exists('pll_register_string') ) :
	function baumchild_pll_register_string() {
		$instance = wd_asp()->instances->get(1); // ID: 1
		$sd = &$instance['data'];
		
		pll_register_string('baumchild_share_title', get_theme_mod('baumchild_share_title'), 'baumchild', true);
		pll_register_string('baumchild_added_to_cart_popup_message', get_theme_mod('baumchild_added_to_cart_popup_message'), 'baumchild', true);
		pll_register_string('baumchild_checkout_industria', get_theme_mod('baumchild_checkout_industria'), 'baumchild', true);
		pll_register_string('baumchild_checkout_puesto', get_theme_mod('baumchild_checkout_puesto'), 'baumchild', true);

		pll_register_string('xoo-qv-gl-pbutton-text', get_option('xoo-qv-gl-pbutton-text'), 'baumchild', true);
		pll_register_string('woocommerce_checkout_terms_and_conditions_checkbox_text', get_option('woocommerce_checkout_terms_and_conditions_checkbox_text'), 'baumchild', true);
		
		pll_register_string('asp_no_results_text', $sd['noresultstext'], 'baumchild', true);
		pll_register_string('asp_default_search_text', $sd['defaultsearchtext'], 'baumchild', true);
		pll_register_string('asp_did_you_mean_text', $sd['didyoumeantext'], 'baumchild', true);
		pll_register_string('asp_more_results', $sd['showmoreresultstext'], 'baumchild', true);
		pll_register_string('asp_top_box_text', $sd['results_top_box_text'], 'baumchild', true);

		/* Category label in Filters */
		pll_register_string('product_cat', __('Categoría', 'baumchild'), 'baumchild', true);

		/* Revolution slider alias */
		pll_register_string('baumchild_site_slider', get_theme_mod('baumchild_site_slider'), 'baumchild', true);
	}
	add_action('init', 'baumchild_pll_register_string');
endif;

if ( function_exists('pll__') ) :
	/**
	** Filter for Baumchild Share Title
	**/
	function baumchild_get_option_baumchild_share_title($option_value) {
		return pll__($option_value);
	}
	add_filter('get_option_baumchild_share_title', 'baumchild_get_option_baumchild_share_title');
	
	/**
	** Filter for checkout Industria options
	**/
	function baumchild_get_option_added_to_cart_popup_message($message) {
		return pll__($message);
	}
	add_filter('get_option_baumchild_added_to_cart_popup_message', 'baumchild_get_option_added_to_cart_popup_message');
	
	/**
	** Filter for checkout Industria options
	**/
	function baumchild_get_option_checkout_industria($industrias) {
		return pll__($industrias);
	}
	add_filter('get_option_baumchild_checkout_industria', 'baumchild_get_option_checkout_industria');

	/**
	** Filter for checkout Puesto options
	**/
	function baumchild_get_option_checkout_puesto($puestos) {
		return pll__($puestos);
	}
	add_filter('get_option_baumchild_checkout_puesto', 'baumchild_get_option_checkout_puesto');

	/**
	** Filter for checkout terms and conditions checkbox text
	**/
	function baumchild_get_terms_and_conditions_checkbox_text($option) {
		return pll__($option);
	}
	add_filter('woocommerce_get_terms_and_conditions_checkbox_text', 'baumchild_get_terms_and_conditions_checkbox_text');

	/**
	** Filter for checkout Puesto options
	**/
	function baumchild_get_option_site_slider($slider_alias) {
		return pll__($slider_alias);
	}
	add_filter('get_option_baumchild_site_slider', 'baumchild_get_option_site_slider');

	/**
	** Translation for Quick view View details button
	**/
	function baumchild_qv_button_text() {
		global $xoo_qv_gl_pbutton_text_value;

		$xoo_qv_gl_pbutton_text_value = pll__($xoo_qv_gl_pbutton_text_value);
	}
	// add_action('init', 'baumchild_qv_button_text');
endif;

/**
** Manage slugs by language
**/
function baumchild_pll_translated_post_type_rewrite_slugs($post_type_translated_slugs) {
	// Add translation for "product" post type.
	$post_type_translated_slugs = array(
		'product' => array(
			'es' => array(
				'has_archive' => true,
				'rewrite' => array(
					'slug' => 'producto',
				),
			),
			'en' => array(
				'has_archive' => true,
				'rewrite' => array(
					'slug' => 'product',
				),
			),
		)
	);

	return $post_type_translated_slugs;
}
add_filter('pll_translated_post_type_rewrite_slugs', 'baumchild_pll_translated_post_type_rewrite_slugs');

/**
 * Translate slug for woocommerce
 *
 * @param $taxonomy_translated_slugs
 *
 * @return array
 */
function baumchild_pll_translated_taxonomy_rewrite_slugs($taxonomy_translated_slugs) {
	$taxonomy_translated_slugs = array(
		'product_cat' => array(
			'es' => 'productos',
			'en' => 'products',
		),
	);
	return $taxonomy_translated_slugs;
}
add_filter('pll_translated_taxonomy_rewrite_slugs', 'baumchild_pll_translated_taxonomy_rewrite_slugs');

/**
** Custom filter in WOOF plugin
** Function edited and custom filter added in > woof_print_tax:after global $WOOF
** /woocommerce-products-filter/views/woof.php
**/
function baumchild_woof_settings($settings) {
	if (isset($settings['custom_tax_label'])) {
		foreach ($settings['custom_tax_label'] as $key => $custom_tax_label) {
			if (preg_match('/pa_/', $key)) {
				if (taxonomy_exists($key)) {
					$settings['show_title_label'][$key] = '1';
					$settings['custom_tax_label'][$key] = pll__(wc_attribute_label($key));
				}
			}

			if ($key == 'product_cat') {
				$settings['show_title_label'][$key] = '1';
				$settings['custom_tax_label'][$key] = pll__(__('Categoría', 'baumchild'));
			}
		}
	}

	return $settings;
}
add_filter('baumchild_woof_settings', 'baumchild_woof_settings');

/**
** Remove lang prefix from SKU
** Filter woocommerce/includes/abstracts/abstract-wc-data.php:get_prop
**/
function baumchild_wc_get_prop_get_sku($value, $_this) {
	$value = str_replace('EN-', '', $value);
	$value = str_replace('ES-', '', $value);
    return $value;
}
add_filter('woocommerce_product_get_sku', 'baumchild_wc_get_prop_get_sku', 10, 2);
