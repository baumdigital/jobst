<?php

/**
 * * Remove sidebars from parent theme
 * */
function baumchild_remove_parent_sidebars()
{
    unregister_sidebar('sidebar-1');
    unregister_sidebar('sidebar-2');
    unregister_sidebar('sidebar-3');
}

add_action('init', 'baumchild_remove_parent_sidebars', 11);

/**
 * * Add new sidebars
 * */
function baumchild_custom_widgets_init()
{
    // Shop sidebar
    register_sidebar(array(
        'name' => __('Main Sidebar', 'baumchild'),
        'id' => 'sidebar-principal',
        'description' => __('Aparece en el listado principal de productos.', 'baumchild'),
        'before_widget' => '<section id="%1$s" class="widget widget-principal %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    // Single Product Carousels
    register_sidebar(array(
        'name' => __('Single Product Carousels', 'baumchild'),
        'id' => 'sidebar-single-product-carousels',
        'description' => __('Aparece abajo del detalle de producto.', 'baumchild'),
        'before_widget' => '<section id="%1$s" class="widget widget-single-product-carousels %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    // Footer sidebar
    /*
      register_sidebar(array(
      'name' => __('Footer Center', 'baumchild'),
      'id' => 'sidebar-footer-main',
      'description' => __('', 'baumchild'),
      'before_widget' => '<section id="%1$s" class="widget widget-footer-main %2$s">',
      'after_widget' => '</section>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
      ));
     */

    // Footer COL 1
    register_sidebar(array(
        'name' => __('Footer Col 1', 'baumchild'),
        'id' => 'sidebar-footer-1',
        'description' => __('', 'baumchild'),
        'before_widget' => '<section id="%1$s" class="widget widget-1 %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    // Footer COL 2
    register_sidebar(array(
        'name' => __('Footer Col 2', 'baumchild'),
        'id' => 'sidebar-footer-2',
        'description' => __('', 'baumchild'),
        'before_widget' => '<section id="%1$s" class="widget widget-2 %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    // Footer COL 3
    register_sidebar(array(
        'name' => __('Footer Col 3', 'baumchild'),
        'id' => 'sidebar-footer-3',
        'description' => __('', 'baumchild'),
        'before_widget' => '<section id="%1$s" class="widget widget-3 %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    // Footer COL 4
    register_sidebar(array(
        'name' => __('Footer col 4', 'baumchild'),
        'id' => 'sidebar-footer-4',
        'description' => __('', 'baumchild'),
        'before_widget' => '<section id="%1$s" class="widget widget-4 %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    // Footer Copyright text
    register_sidebar(array(
        'name' => __('Footer Copy Text', 'baumchild'),
        'id' => 'sidebar-footer-copy',
        'description' => __('', 'baumchild'),
        'before_widget' => '<div class="copy-text %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    // Footer Menu Terminos
    register_sidebar(array(
        'name' => __('Footer Copy Terms', 'baumchild'),
        'id' => 'sidebar-footer-terms',
        'description' => __('', 'baumchild'),
        'before_widget' => '<div class="copy-terms %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    // Footer sidebar
    register_sidebar(array(
        'name' => __('mmenu Contacto', 'baumchild'),
        'id' => 'menu_contacto',
        'description' => __('', 'baumchild'),
        'before_widget' => '<div class="panel col">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    // mmenu cotizar
    register_sidebar(array(
        'name' => __('mmenu Cotizar', 'baumchild'),
        'id' => 'menu_cotizar',
        'description' => __('', 'baumchild'),
        'before_widget' => '<div class="panel col">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    // Baum video modal
    register_sidebar(array(
        'name' => __('Baum Video Modal', 'baumchild'),
        'id' => 'menu_baum_video_modal',
        'description' => __('Area de videos para mostrar en modal', 'baumchild'),
        'before_widget' => '<!--baum video modal widget--><div class="baum-video-modal">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

	// Baum video modal
	register_sidebar(array(
		'name' => __('Baum Quote Menu', 'baumchild'),
		'id' => 'baum_quote_menu',
		'description' => __('Area de cotizaciones para mostrar formularios', 'baumchild'),
		'before_widget' => '<!--baum quote menu widget--><div class="baum-quote-menu panel col">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

	// Blog Sidebar
	register_sidebar( array(
			'name'          => __( 'Blog Sidebar', 'baumchild' ),
			'id'            => 'baum_blog_sidebar',
			'description'   => __( 'Sidebar en el detalle del blog', 'baumchild' ),
			'before_widget' => '<!-- blog sidebar widgets --><div class="blog-sidebar-container row">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
	) );
}

add_action('widgets_init', 'baumchild_custom_widgets_init');

/**
 * * mmenu wrapper form
 * */
function baumdivi_mmenu_contact_wrapper()
{
    ob_start();
    ?>
    <?php if (is_active_sidebar('menu_contacto')) : ?>
        <?php /* PANEL EMPLEO */ ?>
        <div id="contactenos" class="new-panel panel-color-primary panel-empleo">
            <div>
                <div id="contactenosWrap" class="new-panel">
                    <?php dynamic_sidebar('menu_contacto'); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if (is_active_sidebar('menu_cotizar')) : ?>
        <?php /* PANEL EMPLEO */ ?>
        <div id="cotizar" class="new-panel panel-color-primary">
            <div>
                <div id="cotizarWrap" class="new-panel">
                    <?php dynamic_sidebar('menu_cotizar'); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php
    echo ob_get_clean();
}

add_action('wp_footer', 'baumdivi_mmenu_contact_wrapper');

/**
 * Add container for Baum Video Modal
 */
function add_baum_video_modal_area() {
	if ( is_page() ) {
        ob_start();
		if ( is_active_sidebar( 'menu_baum_video_modal' ) ) :
            ?>
                <?php dynamic_sidebar('menu_baum_video_modal'); ?>
            <?php
        endif;
	}
	echo ob_get_clean();
}

add_action( 'wp_footer', 'add_baum_video_modal_area' );

/**
 * Add container for Baum Video Modal
 */
function add_baum_quote_menu_area() {
	if ( is_page() ) {
		ob_start();
		if ( is_active_sidebar( 'baum_quote_menu' ) ) :
			?>
			<?php dynamic_sidebar('baum_quote_menu'); ?>
		<?php
		endif;
	}
	echo ob_get_clean();
}

add_action( 'wp_footer', 'add_baum_quote_menu_area' );
