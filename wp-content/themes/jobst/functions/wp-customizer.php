<?php

/**
 * * Social Links
 * */
function baumchild_sociales($key = null)
{
    $social_links = array(
        'baumchild_contacto_facebook' => array(
            'label' => __('Facebook', 'baumchild'),
            'input_type' => 'text',
            'section' => 'baumchild_sociales_section',
            // 'description' => '',
            'class' => 'fab fa-facebook-f',
            'target' => '_blank',
            'link_prefix' => '',
            'show_link' => false
        ),
        'baumchild_contacto_linkedin' => array(
            'label' => __('LinkedIn', 'baumchild'),
            'input_type' => 'text',
            'section' => 'baumchild_sociales_section',
            'class' => 'fab fa-linkedin-in',
            'target' => '_blank',
            'link_prefix' => '',
            'show_link' => false
        ),
        'baumchild_contacto_youtube' => array(
            'label' => __('YouTube', 'baumchild'),
            'input_type' => 'text',
            'section' => 'baumchild_sociales_section',
            'class' => 'fab fa-youtube',
            'link_prefix' => '',
            'show_link' => false
        ),
        'baumchild_contacto_twitter' => array(
            'label' => __('Twitter', 'baumchild'),
            'input_type' => 'text',
            'section' => 'baumchild_sociales_section',
            'class' => 'fab fa-twitter-square',
            'target' => '_blank',
            'link_prefix' => '',
            'show_link' => false
        ),
        'baumchild_contacto_instagram' => array(
            'label' => __('Instagram', 'baumchild'),
            'input_type' => 'text',
            'section' => 'baumchild_sociales_section',
            'class' => 'fab fa-instagram',
            'target' => '_blank',
            'link_prefix' => '',
            'show_link' => false
        ),
        'baumchild_contacto_whatsapp' => array(
            'label' => __('Whatsapp', 'baumchild'),
            'input_type' => 'text',
            'section' => 'baumchild_sociales_section',
            'class' => 'fab fa-whatsapp',
            'target' => '_blank',
            'link_prefix' => 'https://wa.me/',
            'show_link' => false
        ),
        'baumchild_contacto_telefono' => array(
            'label' => __('Telefono', 'baumchild'),
            'input_type' => 'text',
            'section' => 'baumchild_sociales_section',
            'class' => 'fas fa-phone',
            'link_prefix' => 'tel:',
            'show_link' => true
        ),
        'baumchild_contacto_email' => array(
            'label' => __('Email', 'baumchild'),
            'input_type' => 'text',
            'section' => 'baumchild_sociales_section',
            'class' => 'fas fa-envelope',
            'link_prefix' => 'mailto:',
            'show_link' => true
        ),
        'baumchild_contacto_waze' => array(
            'label' => __('Waze', 'baumchild'),
            'input_type' => 'text',
            'section' => 'baumchild_sociales_section',
            'class' => '_social-waze',
            'link_prefix' => '',
            'show_link' => false
        ),
        'baumchild_contacto_maps' => array(
            'label' => __('Maps', 'baumchild'),
            'input_type' => 'text',
            'section' => 'baumchild_sociales_section',
            'class' => '_social-maps',
            'link_prefix' => '',
            'show_link' => false
        ),
    );

    if (!empty($key))
    {
        return $social_links[$key];
    }
    else
    {
        return $social_links;
    }
}

function baumchild_social_links($exclude = array())
{
    $social_links = baumchild_sociales();
    foreach ($social_links as $social_link => $data)
    {
        if (!(empty(get_theme_mod($social_link))) && !in_array($social_link, $exclude))
        {
            $link = baumchild_social_link_format($social_link);
            $target = !empty($data['target']) ? ' target="' . $data['target'] . '"' : '';
            ?>
            <a class="<?= str_replace('baumchild_', '', $social_link) ?>" href="<?= $data['link_prefix'] . $link ?>" <?= $target ?>>
                <?php if ($data['show_link']) : ?>
                    <?= $link ?>
                <?php else : ?>
                    <i class="<?= $data['class'] ?>" aria-hidden="true"></i>
                <?php endif; ?>
            </a>
            <?php
        }
    }
}

function baumchild_social_link_format($social_link)
{
    if (preg_match('/email/', $social_link))
    {
        $link = sanitize_email(get_theme_mod($social_link));
    }
    elseif (preg_match('/whatsapp/', $social_link))
    {
        $link = '506' . str_replace(array('+', '(', ')', '-', '506'), '', get_theme_mod($social_link));
    }
    else
    {
        $link = get_theme_mod($social_link);
    }

    return $link;
}

function baumchild_social_shortcode($args = array())
{
    $args = shortcode_atts(array(
        'id' => 'whatsapp',
        'is_link' => 0,
        'title' => '',
        'is_right' => 'left',
        'show_icon' => false
            ), $args);

    $id = 'baumchild_contacto_' . $args['id'];
    $is_link = (bool) ($args['is_link'] == 'true') ? 1 : 0;
    $is_right = (bool) ($args['is_right'] == 'true') ? 1 : 0;
    $show_icon = (bool) ($args['show_icon'] == 'true') ? 1 : 0;
    $data = baumchild_sociales($id);
    $link = baumchild_social_link_format($id);
    $target = ' target="' . $data['target'] . '"';
    ob_start();

    if (!empty($link)) :
        $title = apply_filters('baumchild_social_title', $args);
        ?>
        <span class="baumchild_social" data-id="<?= $id ?>">
            <?php if ($show_icon && !empty($data['class'])) : ?>
                <a href="<?= $data['link_prefix'] . $link ?>" class="contacto_<?= $args['id'] ?>" <?= !empty($target) ? $target : '' ?>><i class="<?= $data['class'] ?>"></i> <?= $title ?></a>
            <?php elseif ($is_link) : ?>
                <a href="<?= $data['link_prefix'] . $link ?>" <?= !empty($target) ? $target : '' ?>><?= $link . $title ?></a>
            <?php else : ?>
                <?= $link ?>
            <?php endif; ?>
        </span>
        <?php
    endif;

    $html = ob_get_clean();

    echo $html;
}

add_shortcode('baumchild_social', 'baumchild_social_shortcode');

function baumchild_social_title_wrapper($args)
{
    $is_right = (bool) ($args['is_right'] == 'true') ? 1 : 0;
    ob_start();

    if (!empty($args['title'])) :
        ?>
        <span class="title label-<?= ($is_right) ? 'right' : 'left' ?>"><?= $args['title'] ?></span>
        <?php
    endif;

    $html = ob_get_clean();

    return $html;
}

add_filter('baumchild_social_title', 'baumchild_social_title_wrapper');

/**
 * * Customizer Options
 * */
function baumchild_customize_options($wp_customize)
{
    // Remove WP unused sections
    $wp_customize->remove_section('colors');
    $wp_customize->remove_section('header_image');
    $wp_customize->remove_section('background_image');
    $baumchild_options = array(
        'baumchild_tracking_before_head_close' => array(
            'label' => __('Before </head>', 'baumchild'),
            'input_type' => 'textarea',
            'description' => 'Inserta antes de ' . htmlspecialchars('</head>'),
            'section' => 'baumchild_tracking_section',
        ),
        'baumchild_tracking_after_body_open' => array(
            'label' => __('After <body>', 'baumchild'),
            'input_type' => 'textarea',
            'description' => 'Inserta después de ' . htmlspecialchars('<body>'),
            'section' => 'baumchild_tracking_section',
        ),
        'baumchild_tracking_before_body_close' => array(
            'label' => __('Before </body>', 'baumchild'),
            'input_type' => 'textarea',
            'description' => 'Inserta antes de ' . htmlspecialchars('</body>'),
            'section' => 'baumchild_tracking_section',
        ),
        'baumchild_products_per_page' => array(
            'label' => __('Productos por página', 'baumchild'),
            'input_type' => 'select',
            'choices' => array(
                '12' => '12',
                '15' => '15',
                '18' => '18',
                '21' => '21',
                '-1' => __('Todos', 'baumchild'),
            ),
            'description' => 'Cantidad de productos a mostrar por página',
            'section' => 'baumchild_miscellaneous_section'
        )
    );

    if (class_exists('RevSlider'))
    {
        $rev_slider = new RevSlider();
        $sliders[] = __('Sin Slider', 'baumchild');

        foreach ($rev_slider->getAllSliderForAdminMenu() as $key => $slider)
        {
            $sliders[$slider['alias']] = $slider['title'];
        }

        $baumchild_options['baumchild_site_slider'] = array(
            'label' => __('Slider', 'baumchild'),
            'input_type' => 'select',
            'choices' => $sliders,
            'description' => 'Seleccione el slider principal',
            'section' => 'baumchild_miscellaneous_section'
        );
    }

    $baumchild_options['baumchild_added_to_cart_popup_message'] = array(
        'label' => __('Mensaje de "Agregado a cotización"', 'baumchild'),
        'input_type' => 'textarea',
        'description' => __('Mensaje a mostrarse en el popup de "Agregado a cotización", sobre las opciones de Adicionales.', 'baumchild'),
        'panel' => 'woocommerce',
        'section' => 'woocommerce_baumchild_section'
    );

    $baumchild_options['baumchild_checkout_industria'] = array(
        'label' => __('Opciones de Industria', 'baumchild'),
        'input_type' => 'textarea',
        'description' => __('Agregue las opciones de industria a mostrar en el Checkout. Una por línea', 'baumchild'),
        'panel' => 'woocommerce',
        'section' => 'woocommerce_baumchild_section'
    );

    $baumchild_options['baumchild_checkout_puesto'] = array(
        'label' => __('Opciones de Puesto', 'baumchild'),
        'input_type' => 'textarea',
        'description' => __('Agregue las opciones de puesto a mostrar en el Checkout. Una por línea', 'baumchild'),
        'panel' => 'woocommerce',
        'section' => 'woocommerce_baumchild_section'
    );

    $baumchild_sections = array(
        'baumchild_sociales_section' => array(
            'title' => __('Sociales Child', 'baumchild'),
            'description' => __('Opciones para redes sociales. Use [baumchild_social id="facebook" is_link=true title="Facebook"] para imprimir el link en un bloque de texto. Ej: Facebook: https://www.facebook.com/', 'baumchild'),
        ),
        'baumchild_tracking_section' => array(
            'title' => __('Códigos de seguimiento', 'baumchild'),
            'description' => __('Analytics, FB Pixel, etc. Recuerde agregar las etiquetas "script"', 'baumchild'),
        ),
        'baumchild_miscellaneous_section' => array(
            'title' => __('Miscellaneous', 'baumchild'),
            'description' => __('Logos extra y configuraciones en el sitio', 'baumchild'),
        ),
        'woocommerce_baumchild_section' => array(
            'title' => __('WC by Baum', 'baumchild'),
            'description' => __('Extra configuraciones para WC por Baum', 'baumchild'),
            'panel' => 'woocommerce'
        )
    );

    $baumchild_options = array_merge($baumchild_options, baumchild_sociales());

    // Create a custom section and added on top of options
    foreach ($baumchild_sections as $baumchild_section => $section_data)
    {
        $wp_customize->add_section($baumchild_section, array(
            'title' => $section_data['title'],
            'capability' => 'edit_theme_options',
            'priority' => (!empty($section_data['priority'])) ? $section_data['priority'] : 1,
            'description' => (!empty($section_data['description'])) ? $section_data['description'] : '',
            'panel' => (!empty($section_data['panel'])) ? $section_data['panel'] : ''
        ));
    }

    // Create a custom options and controls
    foreach ($baumchild_options as $baumchild_option => $social_data)
    {
        $wp_customize->add_setting($baumchild_option, array(
            'default' => '',
            'capability' => 'edit_theme_options',
            'type' => 'theme_mod',
            'transport' => 'refresh',
        ));

        $options = array(
            'label' => $social_data['label'],
            'type' => $social_data['input_type'],
            'settings' => $baumchild_option,
            'description' => (!empty($social_data['description'])) ? $social_data['description'] : '',
            'section' => $social_data['section'],
        );

        if ($social_data['input_type'] == 'select')
        {
            $options['choices'] = $social_data['choices'];
        }

        $wp_customize->add_control(new WP_Customize_Control($wp_customize, $baumchild_option, $options));
    }
}

add_action('customize_register', 'baumchild_customize_options');

/**
 * * Tracking codes
 * */
function baumchild_tracking_code_head_close()
{
    $before_head_close = get_theme_mod('baumchild_tracking_before_head_close');

    if (!empty($before_head_close) && !is_admin())
        echo $before_head_close;
}

add_action('wp_head', 'baumchild_tracking_code_head_close', 9999);

function baumchild_tracking_code_body_open()
{
    $after_body_open = get_theme_mod('baumchild_tracking_after_body_open');

    if (!empty($after_body_open) && !is_admin())
        echo $after_body_open;
}

add_action('baumchild_after_body_open', 'baumchild_tracking_code_body_open');

function baumchild_tracking_code_body_close()
{
    $before_body_close = get_theme_mod('baumchild_tracking_before_body_close');

    if (!empty($before_body_close) && !is_admin())
        echo $before_body_close;
}

add_action('wp_footer', 'baumchild_tracking_code_body_close');
