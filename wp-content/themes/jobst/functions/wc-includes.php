<?php
/**
** Required plugins
** Removed - 26-11-2019 - KMA
**/

/**
** Custom & override functions
**/
require get_stylesheet_directory() . '/functions/wc/wc-custom-functions.php';
require get_stylesheet_directory() . '/functions/wc/wc-after-products-import.php';
require get_stylesheet_directory() . '/functions/wc/wc-polylang-customization.php';

/**
** Checkout page
**/
require get_stylesheet_directory() . '/functions/wc/wc-checkout-options.php';

/**
** WC Performance
**/
require get_stylesheet_directory() . '/functions/wc/wc-performance.php';

/**
** WC WOOF filter
**/
require get_stylesheet_directory() . '/functions/wc/woof-customization.php';

/**
** Share products
**/
require get_stylesheet_directory() . '/functions/wc/wc-share-products.php';

/**
** Product Custom Fields
**/
require get_stylesheet_directory() . '/functions/wc/wc-product-custom-fields.php';

/**
** Notifications
**/
require get_stylesheet_directory() . '/functions/wc/wc-notifications.php';

/**
** Print options in template
** Include functions for print options and bacs payment gateway
**/
require get_stylesheet_directory() . '/functions/wc/wc-print-bacs-functions.php';

/**
** Visual Composer customization
**/
if ( class_exists( 'WPBakeryShortcode' ) ) {
	require_once( get_stylesheet_directory(). '/functions/vc-elements/custom-elements.php' );
}
if( function_exists('vc_set_shortcodes_templates_dir') ){
	vc_set_shortcodes_templates_dir( get_stylesheet_directory() . '/functions/vc-elements/vc-templates' );
}

/**
** Custom Baum widgets
**/
require get_stylesheet_directory() . '/functions/baum-widgets/wp-baum-widgets.php';

/**
** Baum Quotes edition
**/
require get_stylesheet_directory() . '/functions/wc/wc-quote-edition.php';

/**
** Woo Bought Together
**/
require get_stylesheet_directory() . '/functions/wc/wbt-customization.php';

/**
 * WordPress template tags functions helpers
 */
require get_stylesheet_directory() . '/functions/wp-template-tags.php';

/**
 ** Post Types
 **/
require get_stylesheet_directory() . '/functions/post-types/punto-venta.php';
