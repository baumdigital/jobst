<?php
/**
 * Default logo
 *
 * @param null $position
 * @param bool $alt
 * @param string $ext
 */
function baumchild_default_logo( $position = null, $alt = false, $ext = 'svg' ) {
	$logo_id  = get_theme_mod( 'custom_logo' );
	$logo_url = wp_get_attachment_image_url( $logo_id, 'full', false );
	$is_alt   = ( $alt ) ? '-alt' : '';

	if ( ! empty( $position ) ) {
		$logo_url = get_theme_mod( 'baumchild_logo_' . $position );
	}

	if ( ! $logo_url ) {
		$logo_url = THEME_DIR_URI . '/assets/img/logo-default' . $is_alt . '.' . $ext;
	}
	?>
	<a class="logo" data-position="<?= $position ?>" href="<?php echo home_url( '/' ) ?>" rel="home" itemprop="url">
		<img src="<?php echo $logo_url ?>" alt="<?php echo get_bloginfo( 'name', 'display' ) ?>" itemprop="logo"/>
	</a>
	<?php
}

/**
 * Check URL from localhost
 *
 * @return bool
 */
function is_baum_localhost() {
	if ( preg_match( '/localhost/', site_url() ) || ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ) {
		return true;
	}

	return false;
}

/**
 * Detect if plugin is activated. For use on Frontend and backend.
 *
 * @param string $plugin_path Full path to plugin, ex. plugin-directory/plugin-file.php
 *
 * @return bool
 */
function baum_is_plugin_activated( $plugin_path ) {
	// check for plugin using plugin name
	if ( in_array( $plugin_path, apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		return true;
	}

	return false;
}

/**
 * Array of css dependency for theme
 *
 * @return array
 */
function baum_get_array_for_register_css() {
	$css = array(
		'mmenu_css',
		'bootstrap_css',
		'font_awesome',
		'datatell_icons',
		'aquawax_font'
	);

	if ( wp_style_is( 'select2', 'registered' ) ) {
		$css[] = 'select2';
	}

	if ( wp_style_is( 'wpdreams-ajaxsearchpro-instances', 'registered' ) ) {
		$css[] = 'wpdreams-ajaxsearchpro-instances';
	}

	return $css;
}

/**
 * Array of css dependency for theme
 *
 * @return array
 */
function baum_get_array_for_register_js() {
	$js = array(
		'jquery',
		'jquery_cookie',
		'jquery_mask',
		'bootstrap_js',
		'matchHeight_js',
		'lightslider',
		'mmenu_js',
		'baumchild-vendor'
	);

	if ( wp_script_is( 'select2', 'registered' ) ) {
		$js[] = 'select2';
	}

	if ( wp_script_is( 'megamenu', 'registered' ) ) {
		$js[] = 'megamenu';
	}

	return $js;
}

/**
 * Baum Logo
 */
function baum_logo_branding() {
	?>
	<!-- Baum Logo -->
	<div class="logo-baum" style="padding: 0; width: 40px;">
		<a href="https://www.baumdigital.com" target="_blank" title="Un proyecto Baum Digital">
			<figure>
				<?php echo baumchild_get_styles_data( get_stylesheet_directory_uri() . '/assets/img/logo-baum-animado.svg' ); ?>
			</figure>
		</a>
	</div>
	<?php
}

add_action('baum_logo_branding', 'baum_logo_branding');

/**
 * Admin login logo
 */
function baumchild_admin_logo() {
	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$logo_url       = wp_get_attachment_image_url( $custom_logo_id, 'full', false );

	if ( ! $logo_url ) {
		$logo_url = THEME_DIR_URI . '/assets/img/logo-default.svg';
	}
	?>
	<style type="text/css">
		body.login div#login h1 a {
			background-image: url(<?= $logo_url ?>);
			background-position: center center;
			background-size: contain;
			height: 120px;
			height: 80px;
			width: auto;
		}

		body.login div#login #wp-submit {
			background: #E76136;
			border: 2px solid #E76136;
			box-shadow: none;
			height: auto;
			line-height: 1.5;
			padding-bottom: 5px;
			padding-top: 5px;
			text-shadow: none;
		}

		body.login div#login #wp-submit:hover {
			background: transparent;
			border-color: currentColor;
			color: #E76136;
		}

		body.login {
			background: #011f5d;
		}

		#loginform {
			border-radius: 6px;
		}

		.login #backtoblog a, .login #nav a {
			color: #fff !important;
		}
	</style>
	<?php
}

add_action( 'login_enqueue_scripts', 'baumchild_admin_logo' );

/**
 * Add custom class and param on youtube embed
 *
 * @param $data
 * @param $url
 * @param $args
 *
 * @return string|string[]
 */
function test_oembed($data, $url, $args) {
	if ( isset( $args['class'] ) ) {
		$data = str_replace( '<iframe ', '<iframe class="' . $args['class'] . '" ', $data );
	}

	// Modify youtube params
	if ( strstr( $data, 'youtube.com/embed/' ) ) {
		$data = str_replace( '?feature=oembed', '?version=3&enablejsapi=1', $data );
	}

	// Return oEmbed html
	return $data;
}
add_filter( 'oembed_result', 'test_oembed', 99, 4 );

/**
 * CSS inline
 *
 * @return false|string
 */
function baumchild_print_inline_css() {
	$theme_uri = get_stylesheet_directory_uri();
	$css_path  = $theme_uri . '/assets/css/theme.min.css';

	$css = baumchild_get_inline_styles();

	if ( ! is_baum_localhost() ) {
		$css .= baumchild_get_styles_data( $css_path );
	}

	return $css;
}

function baumchild_get_styles_data( $file ) {
	$data = wp_remote_fopen( $file );

	return $data;
}

/**
 * Enabled custom mime types upload
 *
 * @param $mime_types
 *
 * @return mixed
 */
function baum_extra_mime_type( $mime_types ) {
	$mime_types['svg'] = 'image/svg+xml';
	$mime_types['jpg'] = 'image/jpeg';

	return $mime_types;
}

add_filter( 'upload_mimes', 'baum_extra_mime_type', 1, 1 );

/**
 * Determines if post thumbnail can be displayed.
 */
function baumchild_can_show_post_thumbnail() {
	return apply_filters( 'baumchild_can_show_post_thumbnail', ! post_password_required() && ! is_attachment() && has_post_thumbnail() );
}

// Exclude Historia category from list blog
function baum_exclude_category_blog( $query ) {
	if ( $query->is_home ) {
		$query->set( 'cat', '-50' );
	}

	return $query;
}

add_filter( 'pre_get_posts', 'baum_exclude_category_blog' );
