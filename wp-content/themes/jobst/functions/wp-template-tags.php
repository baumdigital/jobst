<?php
// Custom image size
add_image_size( 'card', 250, 160, true );
add_image_size( 'blog', 350, 263, true );

if ( ! function_exists( 'baumchild_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function baumchild_post_thumbnail() {
		if ( ! baumchild_can_show_post_thumbnail() ) {
			?>
			<figure class="post-thumbnail">
				<a class="post-thumbnail-inner" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/theme/default-thumb-post.png"
					     alt="Jobst" class="img-responsive wp-post-image">
				</a>
			</figure>
			<?php
		}

		if ( is_singular() ) :
			?>
			<figure class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</figure><!-- .post-thumbnail -->
		<?php else : ?>
			<?php if ( has_post_thumbnail() ):?>
				<figure class="post-thumbnail">
					<a class="post-thumbnail-inner" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
						<?php the_post_thumbnail( 'blog', ['class' => 'img-responsive'] ); ?>
					</a>
				</figure>
			<?php endif; ?>
		<?php endif; // End is_singular().
	}
endif;

if ( ! function_exists( 'baum_the_posts_navigation' ) ) :
	/**
	 * Bootstrap 3 pagination for blog
	 */
	function baum_the_posts_navigation($pages = '', $range = 4) {
		$showitems = ( $range * 2 ) + 1;

		global $paged;

		if ( empty( $paged ) ) {
			$paged = 1;
		}

		if ( $pages == '' ) {
			global $wp_query;

			$pages = $wp_query->max_num_pages;

			if ( ! $pages ) {
				$pages = 1;
			}
		}

		if ( 1 != $pages ) {
			echo '<div class="pagination-container text-center col-md-12">';
			echo '<nav><ul class="pagination">';

			/* if ( $paged > 2 && $paged > $range + 1 && $showitems < $pages ) {
				echo "<li><a href='" . get_pagenum_link( 1 ) . "' aria-label='First'>&laquo;<span class='hidden-xs'> First</span></a></li>";
			} */

			if ( $paged > 1 && $showitems < $pages ) {
				echo "<li><a href='" . get_pagenum_link( $paged - 1 ) . "' aria-label='Previous'><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></a></li>";
			}

			for ( $i = 1; $i <= $pages; $i ++ ) {
				if ( 1 != $pages && ( ! ( $i >= $paged + $range + 1 || $i <= $paged - $range - 1 ) || $pages <= $showitems ) ) {
					echo ( $paged == $i ) ? "<li class=\"active\"><span>" . $i . " <span class=\"sr-only\">(current)</span></span></li>" : "<li><a href='" . get_pagenum_link( $i ) . "'>" . $i . "</a></li>";
				}
			}

			if ( $paged < $pages && $showitems < $pages ) {
				echo "<li><a href=\"" . get_pagenum_link( $paged + 1 ) . "\"  aria-label='Next'><i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></a></li>";
			}

			/* if ( $paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages ) {
				echo "<li><a href='" . get_pagenum_link( $pages ) . "' aria-label='Last'><span class='hidden-xs'>Last </span>&raquo;</a></li>";
			} */

			echo "</ul></nav>";
			echo "</div>";
		}
	}
endif;

/**
 ** Share options single
 **/
function baumchild_single_sharing() {
	?>
	<div class="single-sharing">
		<?php if ( ! empty( $share_title = apply_filters( 'get_option_baumchild_share_title', get_theme_mod( 'baumchild_share_title' ) ) ) ) : ?>
			<span class="single-sharing-title"><?php echo esc_html( $share_title );  ?><span class="colon">:</span></span>
		<?php endif; ?>

		<?php
		$prod_share_options = baumchild_prod_share_options();
		unset( $prod_share_options['baumchild_share_title'] );

		if ( ! empty( $prod_share_options ) ) {
			echo '<ul class="single-share-options list-unstyled">';
		}

		foreach ( $prod_share_options as $prod_share_option => $prod_share_data ) {
			$mobile_only = ! empty( $prod_share_data['mobile_only'] ) ? $prod_share_data['mobile_only'] : false;
			if ( ( ! empty( get_theme_mod( $prod_share_option ) ) && ! $mobile_only ) || ( ! empty( get_theme_mod( $prod_share_option ) ) && $mobile_only && wp_is_mobile() ) ) :
				?>
				<li class="single-share-option share-<?php echo esc_attr( strtolower( $prod_share_data['label'] ) );  ?>">
					<a title="<?php echo esc_attr( $prod_share_data['label'] ); ?>"
					   target="_blank"
					   href="<?php echo baumchild_share_links( get_the_ID(), strtolower( $prod_share_data['label'] ) ) ?>" <?php echo ( isset( $prod_share_data['target'] ) ) ? ' target="' . $prod_share_data['target'] . '"' : '' ?>><i
								class="<?php echo esc_attr( $prod_share_data['icon'] ); ?>"></i></a>
				</li>
			<?php
			endif;
		}

		if ( ! empty( $prod_share_options ) ) {
			echo '</ul>';
		}
		?>
	</div>
	<?php
}
