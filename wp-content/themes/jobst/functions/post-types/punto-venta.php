<?php
if ( ! function_exists( 'baumchild_cpt_punto_venta' ) ) {
	// Register Custom Post Type
	function baumchild_cpt_punto_venta() {
		$labels = array(
				'name'                  => _x( 'Puntos de Venta', 'Post Type General Name', 'baumchild' ),
				'singular_name'         => _x( 'Punto de Venta', 'Post Type Singular Name', 'baumchild' ),
				'menu_name'             => __( 'Punto de Venta', 'baumchild' ),
				'name_admin_bar'        => __( 'Post Type', 'baumchild' ),
				'archives'              => __( 'Item Archives', 'baumchild' ),
				'attributes'            => __( 'Item Attributes', 'baumchild' ),
				'parent_item_colon'     => __( 'Parent Item:', 'baumchild' ),
				'all_items'             => __( 'Puntos de Venta', 'baumchild' ),
				'add_new_item'          => __( 'Agregar Punto de Venta', 'baumchild' ),
				'add_new'               => __( 'Agregar', 'baumchild' ),
				'new_item'              => __( 'Nuevo Punto de Venta', 'baumchild' ),
				'edit_item'             => __( 'Edit Item', 'baumchild' ),
				'update_item'           => __( 'Update Item', 'baumchild' ),
				'view_item'             => __( 'Ver Punto de Venta', 'baumchild' ),
				'view_items'            => __( 'View Items', 'baumchild' ),
				'search_items'          => __( 'Search Item', 'baumchild' ),
				'not_found'             => __( 'Not found', 'baumchild' ),
				'not_found_in_trash'    => __( 'Not found in Trash', 'baumchild' ),
				'featured_image'        => __( 'Featured Image', 'baumchild' ),
				'set_featured_image'    => __( 'Set featured image', 'baumchild' ),
				'remove_featured_image' => __( 'Remove featured image', 'baumchild' ),
				'use_featured_image'    => __( 'Use as featured image', 'baumchild' ),
				'insert_into_item'      => __( 'Insert into item', 'baumchild' ),
				'uploaded_to_this_item' => __( 'Uploaded to this item', 'baumchild' ),
				'items_list'            => __( 'Items list', 'baumchild' ),
				'items_list_navigation' => __( 'Items list navigation', 'baumchild' ),
				'filter_items_list'     => __( 'Filter items list', 'baumchild' ),
		);
		$args   = array(
				'label'               => __( 'Punto de Venta', 'baumchild' ),
				'description'         => __( 'Post Type para Punto de Venta', 'baumchild' ),
				'labels'              => $labels,
				'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
				'taxonomies'          => array( 'pv_category' ),
				'hierarchical'        => false,
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'menu_position'       => 10,
				'menu_icon'           => 'dashicons-store',
				'show_in_admin_bar'   => true,
				'show_in_nav_menus'   => true,
				'can_export'          => true,
				'has_archive'         => true,
				'exclude_from_search' => true,
				'publicly_queryable'  => true,
				'capability_type'     => 'post',
		);
		register_post_type( 'punto-venta', $args );

		flush_rewrite_rules();
	}

	add_action( 'init', 'baumchild_cpt_punto_venta', 0 );
}

if ( ! function_exists( 'baumchild_category_punto_venta' ) ) {
	// Register Custom Taxonomy
	function baumchild_category_punto_venta() {
		$labels  = array(
				'name'                       => _x( 'Zonas de venta', 'Taxonomy General Name', 'baumchild' ),
				'singular_name'              => _x( 'Zona de venta', 'Taxonomy Singular Name', 'baumchild' ),
				'menu_name'                  => __( 'Zona de venta', 'baumchild' ),
				'all_items'                  => __( 'All Items', 'baumchild' ),
				'parent_item'                => __( 'Parent Item', 'baumchild' ),
				'parent_item_colon'          => __( 'Parent Item:', 'baumchild' ),
				'new_item_name'              => __( 'New Item Name', 'baumchild' ),
				'add_new_item'               => __( 'Add New Item', 'baumchild' ),
				'edit_item'                  => __( 'Edit Item', 'baumchild' ),
				'update_item'                => __( 'Update Item', 'baumchild' ),
				'view_item'                  => __( 'View Item', 'baumchild' ),
				'separate_items_with_commas' => __( 'Separate items with commas', 'baumchild' ),
				'add_or_remove_items'        => __( 'Add or remove items', 'baumchild' ),
				'choose_from_most_used'      => __( 'Choose from the most used', 'baumchild' ),
				'popular_items'              => __( 'Popular Items', 'baumchild' ),
				'search_items'               => __( 'Search Items', 'baumchild' ),
				'not_found'                  => __( 'Not Found', 'baumchild' ),
				'no_terms'                   => __( 'No items', 'baumchild' ),
				'items_list'                 => __( 'Items list', 'baumchild' ),
				'items_list_navigation'      => __( 'Items list navigation', 'baumchild' ),
		);
		$rewrite = array(
				'slug'         => 'zona-ventas',
				'with_front'   => true,
				'hierarchical' => false,
		);
		$args    = array(
				'labels'            => $labels,
				'hierarchical'      => true,
				'public'            => true,
				'show_ui'           => true,
				'show_admin_column' => true,
				'show_in_nav_menus' => true,
				'show_tagcloud'     => true,
				'rewrite'           => $rewrite,
		);
		register_taxonomy( 'pv_category', array( 'punto-venta' ), $args );
	}

	add_action( 'init', 'baumchild_category_punto_venta', 0 );
}

function baumchild_punto_venta_custom_fields() {
	return array(
			'pv_ciudad'    => array(),
			'pv_direccion' => array(
					'icon_class' => 'fas fa-home'
			),
			'pv_telefono'  => array(
					'icon_class' => 'fas fa-phone'
			),
			'pv_correo'    => array(
					'icon_class' => 'far fa-envelope'
			),
			'pv_web_url'   => array(
					'icon_class' => 'fas fa-globe'
			),
			'pv_fb_link'   => array(),
			'fb_name'      => array(
					'icon_class' => 'fab fa-facebook-square'
			)
	);
}

function baumchild_after_single_zona_venta_wrapper( $zona_id = 0, $exclude = array() ) {
	$custom_fields_arr = array_diff_key( baumchild_punto_venta_custom_fields(), array_flip( $exclude ) );
	foreach ( $custom_fields_arr as $key => $pv_custom_field ) {
		if ( ! empty( $value = get_post_meta( $zona_id, $key, true ) ) ) {
			$custom_fields[ $key ] = array(
					'icon'  => $pv_custom_field['icon_class'],
					'value' => $value
			);
		}
	}
	?>
	<div class="zona-venta-details">
		<?php
		foreach ( $custom_fields as $key => $the_field ) :
			$formatted_value = '';
			switch ( $key ) {
				case 'pv_direccion':
					$formatted_value = esc_html( $the_field['value'] );
					break;
				case 'pv_correo':
					$formatted_value = '<a href="' . esc_url( 'mailto:' . $the_field['value'] ) . '">' . esc_attr( $the_field['value'] ) . '</a>';
					break;
				case 'pv_web_url':
					$formatted_value = '<a href="' . esc_url( $the_field['value'] ) . '" target="_blank">' . esc_url( $the_field['value'] ) . '</a>';
					break;
				case 'pv_fb_link':
					$fb_link = esc_url( $the_field['value'] );
					break;
				case 'fb_name':
					$formatted_value = ( ! empty( $fb_link ) ) ? '<a href="' . $fb_link . '" target="_blank">' . esc_attr( $the_field['value'] ) . '</a>' : '';
					$fb_link         = '';
					break;
				default:
					$formatted_value = esc_attr( $the_field['value'] );
					break;
			}

			if ( ! empty( $formatted_value ) ) :
				?>
				<p class="key-<?= $key ?>"><i class="<?= $the_field['icon'] ?>"></i> <span
							class="value"><?= $formatted_value ?></span></p>
			<?php
			endif;
		endforeach;
		?>
	</div>
	<?php
}

add_action( 'baumchild_after_single_zona_venta', 'baumchild_after_single_zona_venta_wrapper', 10, 2 );
