<?php
// Register and load the widget
function baumchild_load_custom_widgets() {
	// Widget depend of WC_Widget
	if ( class_exists( 'WC_Widget' ) ) {
		register_widget('WC_Baum_Product_Categories');
		register_widget( 'WC_Baum_Product_Categories_With_Image');
	}

	register_widget('Baum_Sociales_Widget');
	register_widget('Woo_Account_Links_Widget');
    register_widget('Baum_Video_Modal');
    register_widget( 'Baum_Quote_Menu');
	register_widget( 'Baum_List_Posts_Blog' );
}
add_action('widgets_init', 'baumchild_load_custom_widgets');

/**
** Baum Custom widgets
**/
require get_stylesheet_directory() . '/functions/baum-widgets/class-product_cat-widget.php';
require get_stylesheet_directory(). '/functions/baum-widgets/class-product_cat-widget-with-image.php';
require get_stylesheet_directory() . '/functions/baum-widgets/class-baum_sociales-widget.php';
require get_stylesheet_directory() . '/functions/baum-widgets/class-woo_account-widget.php';
require get_stylesheet_directory() . '/functions/baum-widgets/class-baum-video-modal-widget.php';
require get_stylesheet_directory() . '/functions/baum-widgets/class-baum-quote-menu-widget.php';
require get_stylesheet_directory() . '/functions/baum-widgets/class-baum-list-posts-blog-widget.php';
