<?php
/**
 ** Baum Product Categories Widget
 **/
if ( ! class_exists( 'WC_Widget' ) ) {
	return;
}

class WC_Baum_Product_Categories_With_Image extends WC_Widget {
	/**
	 ** Constructor.
	 **/
	public function __construct() {
		$this->widget_cssclass    = 'wc_baum_widget widget_product_categories_with_image';
		$this->widget_description = __( 'Muestra categorías de productos con su imagen.', 'baumchild' );
		$this->widget_id          = 'wc_baum_product_cat_with_image';
		$this->widget_name        = __( 'Baum Categoria de Productos con imagen.', 'baumchild' );

		parent::__construct();
	}

	/**
	 * Output widget.
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Widget instance.
	 *
	 * @see WP_Widget
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$category_id = (int) $instance['category'];
		$term = get_term_by( 'id', $category_id, 'product_cat' );
		$title = (empty($title)) ? $term->name : $title;
		$category_thumbnail_id = get_term_meta( $category_id, 'thumbnail_id', true );
		$image = wp_get_attachment_image_url( $category_thumbnail_id, 'medium' ) ? wp_get_attachment_image_url( $category_thumbnail_id, 'medium' ) : get_stylesheet_directory_uri() . '/assets/images/wc-placeholder.svg';


		// before and after widget arguments are defined by themes
		echo $args['before_widget'];

		ob_start();
		?>
		<div class="product_categories_with_image">
			<figure>
				<a href="<?php echo esc_url( get_term_link( $category_id ) ); ?>" title="<?php echo esc_attr( $title ); ?>">
					<img class="aligncenter img-responsive" src="<?php echo esc_url( $image ); ?>" alt="<?php echo esc_attr( $title ); ?>" width="300" height="200" />
				</a>
			</figure>
			<p class="text-center">
				<a href="<?php echo esc_url( get_term_link( $category_id ) );?>" title="<?php echo esc_attr( $title );?>">
					<?php echo esc_attr( $title ); ?>
				</a>
			</p>
		</div>
		<?php
		echo ob_get_clean();

		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance          = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['category'] = ( ! empty( $new_instance['category'] ) ) ? strip_tags( $new_instance['category'] ) : '';

		return $instance;
	}

	public function form( $instance ) {
		$categories = get_terms( [ 'taxonomy' => 'product_cat', 'hide_empty' => false ] );

		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = '';
		}

		// Widget admin form
		?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Título alternativo:', 'baumchild' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
                   name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
                   value="<?php echo esc_attr( $title ); ?>"
					placeholder="<?php __( 'Nuevo Titúlo para la categoría', 'baumchild' ); ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php echo __( 'Categoría', 'baumchild' ) ?>
                <select class="widefat" name="<?php echo $this->get_field_name( 'category' ); ?>"
                        id="<?php echo $this->get_field_id( 'category' ); ?>">
					<?php if ( ! isset( $instance['category'] ) ) : ?>
                        <option value="-1" disabled selected><?php echo __( 'Seleccione una categoría', 'baumchild' ) ?></option>
					<?php endif; ?>

					<?php foreach ( $categories as $category ) : ?>
                        <option value="<?= $category->term_id ?>" <?php selected( $category->term_id, $instance['category'] ); ?>><?= $category->name ?></option>
					<?php endforeach; ?>
                </select>
            </label>
        </p>
		<?php
	}
}
