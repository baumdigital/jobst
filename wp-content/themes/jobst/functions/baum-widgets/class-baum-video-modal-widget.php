<?php

class Baum_Video_Modal extends WP_Widget {
	function __construct() {
		parent::__construct(
		// Base ID of your widget
			'bvm_widget',
			// Widget name will appear in UI
			__( 'Baum Video Modal Widget', 'baumchild' ),
			// Widget description
			array( 'description' => __( 'Add video for modal', 'baumchild' ), )
		);
	}

	// Creating widget front-end
	public function widget( $args, $instance ) {
		$video = apply_filters( 'widget_video', $instance['video'] );
		$id_video_container = apply_filters( 'widget_id_video_container', $instance['id_video_container'] );

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];

		if ( ! empty( $video ) ) :
            // This is where you run the code and display the output
            ob_start();
            ?>
                <div id="<?php echo $id_video_container; ?>" class="modal fade">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                            <div class="modal-body">
                                <?php
                                echo $args['before_video'];
                                echo wp_oembed_get( $video, [ 'class' => 'ytb-player'] );
                                echo $args['after_video']
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
				<script>
                    jQuery(function ($) {
	                    $("#<?php echo $id_video_container; ?>").on('hide.bs.modal', function(){
                            $('.ytb-player').each(function(){
                                this.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
                            });
	                    });
                    });
				</script>
            <?php
            echo ob_get_clean();
		endif;
		echo $args['after_widget'];
	}

	// Widget Backend
	public function form( $instance ) {
		$video = '';
		$id_video_container = '';

		if ( isset( $instance['video'] ) ) {
			$video = $instance['video'];
		}

		if ( isset( $instance['id_video_container'])) {
			$id_video_container = $instance['id_video_container'];
        }

		// Widget admin form
		?>
        <p>
            <label for="<?php echo $this->get_field_id( 'id_video_container' ); ?>"><?php _e( 'Id del contenedor de video:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'id_video_container' ); ?>"
                   name="<?php echo $this->get_field_name( 'id_video_container' ); ?>" type="text"
                   value="<?php echo esc_attr( $id_video_container ); ?>"
                   placeholder="<?php echo __( 'Insert the #ID of video container', 'baumchild' ); ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'video' ); ?>"><?php _e( 'Video:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'video' ); ?>"
                   name="<?php echo $this->get_field_name( 'video' ); ?>" type="text"
                   value="<?php echo esc_attr( $video ); ?>"
                   placeholder="<?php echo __( 'Insert the url of video', 'baumchild' ); ?>"/>
        </p>
		<?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance          = array();
		$instance['video'] = ( ! empty( $new_instance['video'] ) ) ? strip_tags( $new_instance['video'] ) : '';
		$instance['id_video_container'] = ( ! empty( $new_instance['id_video_container'] ) ) ? strip_tags( $new_instance['id_video_container'] ) : '';

		return $instance;
	}
}
