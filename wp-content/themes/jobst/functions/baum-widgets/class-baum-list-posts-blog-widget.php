<?php
/**
 ** Baum List Posts Blog Widget
 **/

class Baum_List_Posts_Blog extends WC_Widget {
	/**
	 ** Constructor.
	 **/
	public function __construct() {
		$this->widget_cssclass    = 'baum_widget widget_list_posts_blog';
		$this->widget_description = __( 'Muestra lista de posts del blog.', 'baumchild' );
		$this->widget_id          = 'baum_list_posts_blog';
		$this->widget_name        = __( 'Baum lista de posts del blog', 'baumchild' );

		parent::__construct();
	}

	/**
	 * Output widget.
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Widget instance.
	 *
	 * @see WP_Widget
	 */
	public function widget( $args, $instance ) {
		$title     = apply_filters( 'widget_title', $instance['title'] );
		$category  = apply_filters( 'widget_category', $instance['category'] );
		$posts_qty = apply_filters( 'widget_posts_qty', $instance['posts_qty'] );
		$posts_qty = ( $posts_qty > 1) ? $posts_qty - 1 : 0;
		$current_post_id = get_the_ID();
		$args_query      = [
			'posts_per_page' => $posts_qty,
			'no_found_rows'  => true,
			'post__not_in'   => [ $current_post_id, ]
		];

		if ( ! empty( $category ) ) {
			$args_query['cat'] = $category;
		}

		$posts = new WP_Query( $args_query );

		if ( $posts->have_posts() ) {
			// before and after widget arguments are defined by themes
			echo $args['before_widget'];

			while ( $posts->have_posts() ) {
				$posts->the_post();
				?>
				<article class="col-md-12 hidden-xs">
					<div class="article-container">
						<figure class="post-thumbnail">
							<a class="post-thumbnail-inner" href="<?php the_permalink(); ?>" aria-hidden="true"
							   tabindex="-1">
								<?php the_post_thumbnail( 'post-thumbnail', [ 'class' => 'img-responsive' ] ); ?>
							</a>
						</figure>

						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

						<div class="entry-content">
							<p><?php the_excerpt(); ?></p>
							<div class="btn-read-more-container">
								<a href="<?php the_permalink(); ?>" class="btn-read-more">Leer más <i class="fas fa-arrow-right"></i></a>
							</div>
						</div>
					</div>
				</article>
                <article class="col-md-12 visible-xs">
                    <div class="row article-container-mobile">
                        <div class="col-xs-5">
	                        <figure class="post-thumbnail">
		                        <a class="post-thumbnail-inner" href="<?php the_permalink(); ?>" aria-hidden="true"
		                           tabindex="-1">
			                        <?php the_post_thumbnail( 'post-thumbnail', [ 'class' => 'img-responsive' ] ); ?>
		                        </a>
	                        </figure>
                        </div>
	                    <div class="col-xs-7">
		                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		                    <a href="<?php the_permalink();?>">Leer más <i class="fas fa-arrow-right"></i></a>
	                    </div>
                    </div>
                </article>
				<?php
			}

			echo $args['after_widget'];
		}
		wp_reset_postdata();
	}

	public function update( $new_instance, $old_instance ) {
		$instance          = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['category'] = ( ! empty( $new_instance['category'] ) ) ? strip_tags( $new_instance['category'] ) : '';
		$instance['posts_qty'] = ( ! empty( $new_instance['posts_qty'] ) ) ? strip_tags( $new_instance['posts_qty'] ) : 0;

		return $instance;
	}

	public function form( $instance ) {
		$categories = get_terms( [ 'taxonomy' => 'category', 'hide_empty' => false ] );

		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = '';
		}

		$posts_qty = (isset($instance['posts_qty'])) ? $instance['posts_qty'] : 0;

		// Widget admin form
		?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Título alternativo:', 'baumchild' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
                   name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
                   value="<?php echo esc_attr( $title ); ?>"
					placeholder="<?php __( 'Nuevo Titúlo para la categoría', 'baumchild' ); ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php echo __( 'Categoría', 'baumchild' ) ?>
                <select class="widefat" name="<?php echo $this->get_field_name( 'category' ); ?>"
                        id="<?php echo $this->get_field_id( 'category' ); ?>">
					<?php if ( ! isset( $instance['category'] ) ) : ?>
                        <option value="-1" disabled selected><?php echo __( 'Seleccione una categoría', 'baumchild' ) ?></option>
					<?php endif; ?>

                    <option value="0" <?php selected( 0, $instance['category'] ); ?>><?php echo __( 'Últimos posts', 'baumchild' ) ?></option>

					<?php foreach ( $categories as $category ) : ?>
                        <option value="<?= $category->term_id ?>" <?php selected( $category->term_id, $instance['category'] ); ?>><?= $category->name ?></option>
					<?php endforeach; ?>
                </select>
            </label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'posts_qty' ); ?>"><?php _e( 'Cantidad de Posts:', 'baumchild' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'posts_qty' ); ?>"
                   name="<?php echo $this->get_field_name( 'posts_qty' ); ?>" type="number"
                   value="<?php echo esc_attr( $posts_qty ); ?>"/>
        </p>
		<?php
	}
}
