<?php

class Baum_Quote_Menu extends WP_Widget {
	function __construct() {
		parent::__construct(
		    // Base ID of your widget
			'bqm_widget',
			// Widget name will appear in UI
			__( 'Baum Quote Menu Widget', 'baumchild' ),
			// Widget description
			array( 'description' => __( 'Add sidebar menu for quote', 'baumchild' ), )
		);
	}

	// Creating widget front-end
	public function widget( $args, $instance ) {
		$quote = apply_filters( 'widget_quote', $instance['quote'] );
		$id_quote_container = apply_filters( 'quote_id_quote_container', $instance['id_quote_container'] );

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];

		if ( ! empty( $quote ) ) :
            // This is where you run the code and display the output
            ob_start();
            ?>
				<div id="<?php echo $id_quote_container; ?>" class="new-panel panel-color-primary quote-menu">
					<div>
						<div id="cotizarWrap" class="new-panel col-md-12">
							<?php
							$shortcode_string = '[contact-form-7 id="' . $quote . '"]';

							echo do_shortcode( $shortcode_string ); ?>
						</div>
					</div>
				</div>
				<script>
                    jQuery(function ($) {
                        var mmenu_args = {
                            extensions: [
                                "fx-panels-slide-100",
                                "pagedim-black",
                                "position-right",
                                "position-front"
                            ],
                            close: false,
                            navbar: false,
                            onClick: {
                                setSelected: true
                            },
                            keyboardNavigation: {
                                enable: true,
                                enhance: true
                            }
                        };

                        var side_menu = $("#<?php echo $id_quote_container; ?>")
                            .mmenu(mmenu_args)
                            .data("mmenu");

                        $('#<?php echo $id_quote_container ?>-btn').click(function () {
                            side_menu.open();
                        });

                        $(document).on("click", "#<?php echo $id_quote_container; ?> .mmenu-close-button", function () {
                            side_menu.close();
                            return false;
                        });
                    });
				</script>
            <?php
            echo ob_get_clean();
		endif;
		echo $args['after_widget'];
	}

	// Widget Backend
	public function form( $instance ) {
		$quote = '';
		$id_quote_container = '';

		if ( isset( $instance['quote'] ) ) {
			$quote = $instance['quote'];
		}

		if ( isset( $instance['id_quote_container'])) {
			$id_quote_container = $instance['id_quote_container'];
        }

		// Widget admin form
		?>
        <p>
            <label for="<?php echo $this->get_field_id( 'id_quote_container' ); ?>"><?php _e( 'Id del contenedor de la cotización:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'id_quote_container' ); ?>"
                   name="<?php echo $this->get_field_name( 'id_quote_container' ); ?>" type="text"
                   value="<?php echo esc_attr( $id_quote_container ); ?>"
                   placeholder="<?php echo __( 'Insert the #ID of quote container', 'baumchild' ); ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'quote' ); ?>"><?php _e( 'Cotización:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'quote' ); ?>"
                   name="<?php echo $this->get_field_name( 'quote' ); ?>" type="text"
                   value="<?php echo esc_attr( $quote ); ?>"
                   placeholder="<?php echo __( 'Insert the contact form ID', 'baumchild' ); ?>"/>
        </p>
		<?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance          = array();
		$instance['quote'] = ( ! empty( $new_instance['quote'] ) ) ? strip_tags( $new_instance['quote'] ) : '';
		$instance['id_quote_container'] = ( ! empty( $new_instance['id_quote_container'] ) ) ? strip_tags( $new_instance['id_quote_container'] ) : '';

		return $instance;
	}
}