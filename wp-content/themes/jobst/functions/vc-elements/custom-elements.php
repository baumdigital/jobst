<?php
/*
	Element Description: VC Customization
*/

if (!defined('ABSPATH')) { exit; }

class VCCustomizations extends WPBakeryShortcode {
	function __construct() {
		add_action('vc_after_init', array($this, 'vc_icon_remapping'));
		add_action('vc_after_init', array($this, 'vc_row_remapping'));

		// New Elements Mapping
		add_action('init', array($this, 'vc_baum_sociales_mapping'));

		// New Elements HTML
		add_shortcode('vc_baum_sociales', array($this, 'vc_baum_sociales_html'));
	}

	// Override single image
	function vc_icon_remapping() {
		// Remove Params
		if (function_exists('vc_remove_param')) {
			vc_remove_param('vc_single_image', 'el_id');
			// vc_remove_param('vc_single_image', 'border_color');
		}

		// Add new Params
		$vc_icon_new_params = array(
			array(
				'type' => 'textarea',
				'class' => 'icon_text',
				'heading' => __('Texto del icono', 'baumchild'),
				'param_name' => 'icon_text',
				// 'value' => __('', 'baumchild'),
				'description' => __('Texto del icono', 'baumchild'),
				'group' => esc_html__( 'Texto', 'baumchild' ),
			),
			array(
				'type' => 'colorpicker',
				'class' => 'icon_color',
				'heading' => __('Colores', 'baumchild'),
				'param_name' => 'icon_color',
				'description' => __('Color del texto', 'baumchild'),
				'group' => esc_html__( 'Texto', 'baumchild' ),
			)
		);

		vc_add_params('vc_icon', $vc_icon_new_params);
	}

	// Override rows
	function vc_row_remapping() {
		// Remove Params
		if (function_exists('vc_remove_param')) {
			vc_remove_param('vc_column', 'el_id');
			vc_remove_param('vc_column_inner', 'el_id');
		}

		// Add new Params
		$vc_row_new_params = array(
			array(
				'type' => 'checkbox',
				'class' => 'attr_feature_boxes',
				'heading' => __('Feature box', 'kiddu'),
				'param_name' => 'attr_feature_boxes',
				'description' => __('Is feature box row?', 'kiddu'),
				'group' => esc_html__('Feature Boxes', 'kiddu'),
			),
		);

		vc_add_params('vc_row', $vc_row_new_params);
	}

	// Elements Mapping
	public function vc_baum_sociales_mapping() {
		// Stop all if VC is not enabled
		if (!defined('WPB_VC_VERSION')) { return; }

		$sociales = array();

		if(function_exists('baumchild_sociales')) :
			foreach (baumchild_sociales() as $key => $social) {
				$item = str_replace('baumchild_contacto_', '', $key);
				$message = __('Empty. Edit options in Appearance > Customize > Sociales Child', 'baumchild');
				$sociales[] = array(
					'type' => 'checkbox',
					'heading' => $social['label'],
					'param_name' => $key,
					'group' => ucfirst($item),
					'description' => !empty(get_theme_mod($key)) ? 'Current: ' . get_theme_mod($key) : $message,
					'value' => ''
				);
				$sociales[] = array(
					'type' => 'textfield',
					'heading' => ucfirst($item) . ' Label',
					'param_name' => $item . '_label',
					'group' => ucfirst($item),
					'value' => ''
				);
				$sociales[] = array(
					'type' => 'checkbox',
					'heading' => 'Label position right',
					'param_name' => $item . '_right',
					'group' => ucfirst($item),
					'description' => 'Set label to the right'
				);
				$sociales[] = array(
					'type' => 'checkbox',
					'heading' => 'Show as link',
					'param_name' => $item . '_link',
					'group' => ucfirst($item),
					'value' => ''
				);
				$sociales[] = array(
					'type' => 'checkbox',
					'heading' => 'Only font awesome icon',
					'param_name' => $item . '_icon',
					'group' => ucfirst($item),
					'value' => ''
				);
			}
		endif;

		// Map the block with vc_map()
		vc_map(
			array(
				'name' => __('Baum Theme Sociales', 'baumchild'),
				'base' => 'vc_baum_sociales',
				'description' => __('Active the site social links', 'kiddu'),
				'category' => __('Baum Themes', 'baumchild'),
				'icon' => 'icon-heart',
				'icon' => get_stylesheet_directory_uri() . '/functions/vc-elements/images/placeholder.jpg',
				'params' => $sociales
			)
		);
	}

	// Elements HTML
	public function vc_baum_sociales_html($atts) {
		// Params extraction
		extract( $atts );

		ob_start();
		foreach ($atts as $key => $attr) {
			if(preg_match('/baumchild_/', $key)) {
				$item = str_replace('baumchild_contacto_', '', $key);
				$item_link = (!empty($atts[$item . '_link'])) ? $atts[$item . '_link'] : '';
				$item_label = (!empty($atts[$item . '_label'])) ? $atts[$item . '_label'] : '';
				$item_right = (!empty($atts[$item . '_right'])) ? $atts[$item . '_right'] : '';
				$item_icon = (!empty($atts[$item . '_icon'])) ? $atts[$item . '_icon'] : '';

				echo do_shortcode('[baumchild_social id="' . $item . '" is_link="' . $item_link . '" title="' . $item_label . '" is_right="' . $item_right . '" show_icon="' . $item_icon . '"]');
			}
		}
		// // Fill $html var with data
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
}

new VCCustomizations();
