# Baum WordPress Starter

## Baum Child Theme for Twentynineteen

### How to use?

1. Local development:

Open the file gulp.config.js and change the value of `projectURL` for the local virtualhost,
for example: `example.test`

2. Install dependency

This project use node 12.16.1, please use [nvm](https://github.com/nvm-sh/nvm) for set up on your local machine.

Setup node version

`$ nvm use`

Install node package

`$ npm install`

3. Run gulp task for workflow development

`$ npm start`

Other task

`$ gulp styles`

`$ gulp vendorsJS`

`$ gulp customJS`

`$ gulp images`

`$ gulp clearCache`

`$ gulp translate`
