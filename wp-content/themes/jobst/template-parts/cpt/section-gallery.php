<?php
/**
 * Template part for taxonomy-pv_category.php
 */

$gallery_images = get_term_meta( $current_term_id, 'vdw_gallery_id', true );

if ( $gallery_images ) :
	?>
	<div class="section-carousel-images">
		<div class="container">
			<div class="carousel-images-wrapper">
				<div class="row">
					<ul class="cS-hidden zona-ventas-carousel" data-ci_lg="4" data-ci_md="3"
					    data-ci_sm="2" data-ci_xs="2">
						<?php
						foreach ( $gallery_images as $key => $image_id ) :
							$image = wp_get_attachment_image_src( $image_id, 'full' );
							?>
							<li>
								<img class="image-preview" src="<?php echo $image[0]; ?>">
							</li>

						<?php endforeach; ?>
					</ul>
					<div class="slider-arrows">
						<a href="#" class="arrow arrow-prev"><i class="fa fa-angle-left"></i></a>
						<a href="#" class="arrow arrow-next"><i class="fa fa-angle-right"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
