<?php
/**
 * Template part for taxonomy-pv_category.php
 */

$zonas_destacadas = get_posts(
	array(
		'posts_per_page' => 2,
		'post_type'      => 'punto-venta',
		'post_status'    => 'publish',
		'tax_query'      => array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'pv_category',
				'field'    => 'id',
				'terms'    => $current_term_id,
			),
			array(
				'taxonomy' => 'pv_category',
				'field'    => 'id',
				'terms'    => $id_destacado,
			)
		)
	)
);

if ( $zonas_destacadas ) : ?>
	<div class="section-zonas-destacadas">
		<div class="row">
			<?php foreach ( $zonas_destacadas as $key => $zonas_destacada ) : $zona_id = $zonas_destacada->ID; ?>
				<div id="zona-<?= $zona_id ?>" class="col-sm-6 col-xs-12 single-zona-destacada">
					<div class="zona-venta-thumbnail">
						<?= get_the_post_thumbnail( $zona_id ) ?>
					</div>
					<div class="single-zona-venta">
						<h4 class="zona-title"><?= $zonas_destacada->post_title ?></h4>

						<?php do_action( 'baumchild_after_single_zona_venta', $zona_id ) ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>
