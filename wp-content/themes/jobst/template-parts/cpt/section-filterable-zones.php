<?php
/**
 * Template part for taxonomy-pv_category.php
 */

?>
<div class="section-filterable-zonas">
	<div class="listing-zonas-categories">
		<a href="#" class="single-zona all-zonas" data-zona=""><?php echo __( 'Todos', 'baumchild' ) ?></a>
		<?php
		if ( $child_terms ) :
			foreach ( $child_terms as $key => $child_term ) :
				?>
				<a href="#" class="single-zona"
				   data-zona="<?php echo esc_attr( $child_term->slug ); ?>"><?php echo esc_html( $child_term->name ); ?></a>
			<?php
			endforeach;
		endif;
		?>
	</div>
	<div class="filtering-zonas-venta">
		<?php if ( $zonas_array ) : ?>
			<ul data-columns="4" class="columns-4 listing-zonas list-unstyled">
				<?php
				foreach ( $zonas_array as $key => $zona_array ) :
					$the_terms = $custom_fields = array();
					$zona_id = $zona_array->ID;
					foreach ( wp_get_post_terms( $zona_array->ID, 'pv_category', array( 'parent' => $current_term_id ) ) as $key => $current_terms ) {
						$the_terms[] = $current_terms->slug;
					}
					$the_terms[] = 'is-animated';
					?>
					<li id="zona-<?= $zona_id ?>" class="<?= implode( ' ', $the_terms ) ?>">
						<div class="single-zona-venta">
							<h4 class="zona-title"><?= $zona_array->post_title ?></h4>

							<?php do_action( 'baumchild_after_single_zona_venta', $zona_id, array( 'pv_ciudad' ) ) ?>
						</div>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>
