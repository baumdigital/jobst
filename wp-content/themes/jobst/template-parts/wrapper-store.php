<?php
/**
** $pid = $post->ID
** Sobreescriba este template desde /[theme]/template-parts/
**/
$waze_icon = apply_filters('wps_store_get_waze_icon', wps_store_get_waze_icon());
$store_full_image = get_the_post_thumbnail($pid, $attr['store_image_size']);
?>
<div class="<?= $class ?> widget_store-content" data-id="<?= $pid ?>">
    <p class="store-title">
    	<?php if(!empty(get_page_by_title('contactenos'))) : ?>
    		<a href="<?= get_permalink(get_page_by_title('contactenos')) ?>"><?php the_title() ?></a>
    	<?php else: ?>
    		<?php the_title() ?>
    	<?php endif; ?>
    </p>
    <?php echo !empty($city) ? '<p class="city">' . $city . '</p>' : ''; ?>
    <?php echo !empty($address) ? '<div class="address">' . apply_filters('the_content', $address) . '</div>' : ''; ?>
    <?php echo !empty($phone) ? '<p class="phone">' . $phone . '</p>' : ''; ?>
    <?php echo !empty($description) ? '<div class="description">' . apply_filters('the_content', $description) . '</div>' : ''; ?>
    <?php echo !empty($waze) ? '<p class="waze"><a href="' . $waze . '" target="_blank">' . $waze_icon . '</a></p>' : ''; ?>
</div>