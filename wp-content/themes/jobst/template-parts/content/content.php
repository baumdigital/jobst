<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'col-md-3' ); ?>>
	<div class="article-container">
		<?php baumchild_post_thumbnail(); ?>

		<header class="entry-header">
			<?php
			if ( is_sticky() && is_home() && ! is_paged() ) {
				printf( '<span class="sticky-post">%s</span>', _x( 'Featured', 'post', 'twentynineteen' ) );
			}
			if ( is_singular() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
			endif;
			?>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<div class="entry-content-excerpt">
				<?php the_excerpt(); ?>
			</div>

			<div class="btn-read-more-container">
				<a href="<?php the_permalink(); ?>" title="Leer más de: <?php the_title(); ?>" class="btn-read-more">
					Leer más <i class="fas fa-arrow-right"></i>
				</a>
			</div>
		</div><!-- .entry-content -->
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
