<div class="container-fluid">
	<div class="contact-section row">
		<div class="contact-section-call-to col-md-12">
			<h3>Consulta y conoce más de nuestros productos</h3>
			<div class="button-contact">
				<a href="" title="Contáctanos">Contáctanos</a>
			</div>
		</div>
		<div class="contact-section-newsletter col-md-12">
			<div class="col-md-6">
				<h2>Inscríbete a nuestro boletín</h2>
				<h3>y recibe nuestras últimas noticias</h3>
			</div>
			<div class="col-md-6">
				<?php echo do_shortcode( '[contact-form-7 id="93" title="Home Newsletter"]' ); ?>
			</div>
		</div>
	</div>
</div>
