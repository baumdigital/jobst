<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'col-md-9' ); ?>>
	<header class="entry-header">
		<?php get_template_part( 'template-parts/header/entry', 'header' ); ?>
	</header>

	<div class="entry-content">
		<?php
		the_post_thumbnail( 'large', [ 'class' => 'post-main-image img-responsive' ] );

		the_content(
				sprintf(
						wp_kses(
						/* translators: %s: Post title. Only visible to screen readers. */
								__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentynineteen' ),
								array(
										'span' => array(
												'class' => array(),
										),
								)
						),
						get_the_title()
				)
		);
		?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->

<aside class="col-md-3">
	<?php if ( is_active_sidebar( 'baum_blog_sidebar' ) ) : ?>
		<?php dynamic_sidebar( 'baum_blog_sidebar' ); ?>
	<?php endif; ?>
</aside>
