<?php
/**
 * * The template for displaying the header
 * */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php do_action('baumchild_after_body_open'); ?>

<div id="page" class="site">
	<header class="site-header">
		<div id="site-header-menu" class="site-header-menu">
			<div class="site-header-wrap container">
				<div class="site-header-nav_toggle">
					<a href="javascript:void(0);" class="site-header-toggle-btn">
						<span class="toggle toggle_open icon-dt_menu"></span>
						<span class="toggle toggle_close icon-dt_cerrar"></span>
					</a>
				</div>
				<div class="site-header-logo">
					<?php
					if (function_exists('baumchild_default_logo'))
					{
						baumchild_default_logo();
					}
					?>
				</div>
				<div class="site-header-menu-primary">
					<?php if (has_nav_menu('primary')) : ?>
						<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e('Primary Menu', 'baumchild'); ?>">
							<?php
							wp_nav_menu(array(
									'theme_location' => 'primary',
									'menu_class' => 'primary-menu',
							));
							?>
						</nav>
						<div class="nav-overlay"></div>
					<?php
					endif;
					?>
				</div>
				<div class="site-header-menu-secondary">
					<nav id="site-navigation-sec" class="menu-secundario" role="navigation" aria-label="<?php esc_attr_e('Menu Secundario', 'baumchild'); ?>">
						<ul class="secondary-menu nav navbar-nav">
							<li class="menu-item menu-item-buscador">
								<a href="javascript:void(0);" title="<?= __('Buscar', 'baumchild'); ?>" class="search-button" data-toggle=".body">
									<i class="icon-dt_search" aria-hidden="true"></i>
									<!--<span><?/*= __('Buscar', 'baumchild'); */?></span>-->
								</a>
							</li>
							<?php if ( class_exists( 'woocommerce' ) && function_exists( 'baumchild_mini_cart_html' ) ) : ?>
								<!-- <li class="menu-item <?/*= ( is_cart() ) ? 'current-menu-item current_page_item' : '' */?> cart-wrapper">
			                                <?php /*echo baumchild_mini_cart_html(); */?>
		                                </li>-->
							<?php
							endif;

							if ( has_nav_menu( 'secondary' ) ) :
								wp_nav_menu( array(
										'theme_location' => 'secondary',
										'menu_class'     => 'secondary-menu nav navbar-nav',
										'items_wrap'     => '%3$s',
										'container'      => '',
								) );
							endif;
							?>
							<li class="menu-item menu-item-contacto">
								<a href="javascript:void(0);" title="<?= __('Contáctanos', 'baumchild'); ?>" class="contact-button btn btn-info" data-toggle=".body">
									<i class="fas fa-envelope"></i>
									<span><?= __('Contáctanos', 'baumchild'); ?></span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>

	<div class="search-form">
		<?php get_search_form(); ?>
		<button type="button" class="navbar-toggle close-search">
			<span class="icon-dt_cerrar"></span>
		</button>
		<div class="search-overlay"></div>
	</div>

	<?php do_action('baumchild_slider_header'); ?>

	<div id="content" class="site-content">
		<div class="container">
