<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */
if (!defined('ABSPATH'))
{
    exit; // Exit if accessed directly
}
?>
<div class="user-dashboard">
    <h3>
        <?php printf(__('Hola %1$s', 'baumchild'), esc_html($current_user->display_name)); ?>
    </h3>

    <p>
        <?php
        printf(__('¿no eres <strong>%1$s</strong>? <a href="%2$s">Cambiar de usuario</a>', 'baumchild'), '<strong>' . esc_html($current_user->display_name) . '</strong>', esc_url(wc_logout_url(wc_get_page_permalink('myaccount')))
        );
        ?>

    </p>

    <p> 
        Desde el panel de <?php printf(__('<a href="%1$s">Mi cuenta</a>', 'baumchild'), esc_url(wc_get_endpoint_url('edit-account'))); ?>, podrás ver y editar:
    </p>
    <p>
        <ul class="account-buttons list-inline">
            <li>
                <?php printf(__('<a class="button" href="%1$s"><i class="fa fa-list-ul" aria-hidden="true"></i> %2$s</a>', 'baumchild'), esc_url(wc_get_endpoint_url('orders')), __('Cotizaciones', 'baumchild')); ?>
            </li>
            <li>
                <?php printf(__('<a class="button" href="%1$s"><i class="glyphicon glyphicon-user"></i> %2$s</a>', 'baumchild'), esc_url(wc_get_endpoint_url('edit-account')), __('Datos personales', 'baumchild')); ?>
            </li>
            <li>
                <?php printf(__('<a class="button" href="%1$s"><i class="fa fa-map-marker" aria-hidden="true"></i> %2$s</a>', 'baumchild'), esc_url(wc_get_endpoint_url('edit-address')), __('Mi Dirección', 'baumchild')); ?>
            </li>
            <li>
                <?php printf(__('<a class="button" href="%1$s"><i class="fa fa-sign-out" aria-hidden="true"></i> %2$s</a>', 'baumchild'), esc_url(wc_logout_url(wc_get_page_permalink('myaccount'))), __('Cerrar sesión', 'baumchild')); ?>
            </li>
        </ul>
    </p>
</div>
<?php
/**
 * My Account dashboard.
 *
 * @since 2.6.0
 */
do_action('woocommerce_account_dashboard');

/**
 * Deprecated woocommerce_before_my_account action.
 *
 * @deprecated 2.6.0
 */
do_action('woocommerce_before_my_account');

/**
 * Deprecated woocommerce_after_my_account action.
 *
 * @deprecated 2.6.0
 */
do_action('woocommerce_after_my_account');

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
