<?php
/**
 * Shipping Methods Display
 *
 * In 2.1 we show methods per package. This allows for multiple methods per order if so desired.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

$formatted_destination    = isset( $formatted_destination ) ? $formatted_destination : WC()->countries->get_formatted_address( $package['destination'], ', ' );
$has_calculated_shipping  = ! empty( $has_calculated_shipping );
$show_shipping_calculator = ! empty( $show_shipping_calculator );
$calculator_text          = '';
?>
<tr class="woocommerce-shipping-totals shipping">
	<th><?php echo __('Envío', 'baumchild'); ?></th>
	<td data-title="<?php echo esc_attr( $package_name ); ?>">
		<?php if ( $available_methods ) : ?>
			<?php if(is_cart()): ?>
				<select class="shipping_methods">
					<option value="calculate_shipping"><?php _e( 'Calculate shipping', 'woocommerce' ); ?></option>
					<?php foreach ($available_methods as $method): ?>
					<option value="<?php echo esc_attr($method->id); ?>" <?php selected($method->id, $chosen_method); ?>><?php echo wp_kses_post(wc_cart_totals_shipping_method_label($method)); ?></option>
					<?php endforeach; ?>
				</select>
			<?php endif; ?>

			<select class="shipping_method <?= (is_cart()) ? 'hidden' : ''; ?>" name="shipping_method[<?php echo $index; ?>]" data-index="<?php echo $index; ?>" id="shipping_method_<?php echo $index; ?>">
				<?php foreach ($available_methods as $method): ?>
					<option value="<?php echo esc_attr($method->id); ?>" <?php selected($method->id, $chosen_method); ?>><?php echo wp_kses_post(wc_cart_totals_shipping_method_label($method)); ?></option>
					<?php do_action( 'woocommerce_after_shipping_rate', $method, $index ); ?>
				<?php endforeach; ?>
			</select>

			<?php if ( is_cart() ) : ?>
				<p class="woocommerce-shipping-destination hide">
					<?php
					if ( $formatted_destination ) {
						// Translators: $s shipping destination.
						printf( esc_html__( 'Estimate for %s.', 'woocommerce' ) . ' ', '<strong>' . esc_html( $formatted_destination ) . '</strong>' );
						$calculator_text = __( 'Change address', 'woocommerce' );
					} else {
						echo esc_html__( 'This is only an estimate. Prices will be updated during checkout.', 'woocommerce' );
					}
					?>
				</p>
			<?php endif; ?>
		<?php
		elseif ( ! $has_calculated_shipping || ! $formatted_destination ) :
			esc_html_e( 'Enter your address to view shipping options.', 'woocommerce' );
		elseif ( ! is_cart() ) :
			echo wp_kses_post( apply_filters( 'woocommerce_no_shipping_available_html', __( 'There are no shipping methods available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'woocommerce' ) ) );
		else :
			// Translators: $s shipping destination.
			echo wp_kses_post( apply_filters( 'woocommerce_cart_no_shipping_available_html', sprintf( esc_html__( 'No shipping options were found for %s.', 'woocommerce' ) . ' ', '<strong>' . esc_html( $formatted_destination ) . '</strong>' ) ) );
			$calculator_text = __( 'Enter a different address', 'woocommerce' );


		endif;
		?>

		<?php if ( $show_package_details ) : ?>
			<?php echo '<p class="woocommerce-shipping-contents"><small>' . esc_html( $package_details ) . '</small></p>'; ?>
		<?php endif; ?>

		<?php if ( $show_shipping_calculator ) : ?>
			<div class="shipping-calculator-wrapper <?php if(!$available_methods) echo 'no-address' ?>">
				<?php woocommerce_shipping_calculator( $calculator_text ); ?>
			</div>
		<?php endif; ?>
	</td>
</tr>
