<?php
/**
 * Proceed to quote button instead of Proceed to checkout button
 * Baum Digital
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<div class="cart_totals">
	<a href="<?php echo esc_url( wc_get_checkout_url() );?>" class="checkout-button button alt wc-forward text-center">
		<?php esc_html_e('Continuar para cotizar', 'baumchild' ); ?> <i class="fa fa-angle-right" aria-hidden="true"></i>
	</a>
</div>
