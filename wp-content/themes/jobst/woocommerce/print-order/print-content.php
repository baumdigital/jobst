<?php
/**
 * Print order content. Copy this file to your themes
 * directory /woocommerce/print-order to customize it.
 *
 * @package WooCommerce Print Invoice & Delivery Note/Templates
 */

if ( !defined( 'ABSPATH' ) ) exit;
?>

	<div class="order-branding">
		<div class="company-logo">
			<?php if( wcdn_get_company_logo_id() ) : ?><?php wcdn_company_logo(); ?><?php endif; ?>
		</div>
		
		<div class="company-info">
			<?php if( !wcdn_get_company_logo_id() ) : ?><h1 class="company-name"><?php wcdn_company_name(); ?></h1><?php endif; ?>
			<div class="company-address"><?php wcdn_company_info(); ?></div>
		</div>
		
		<?php do_action( 'wcdn_after_branding', $order ); ?>
	</div><!-- .order-branding -->

	<div class="order-addresses<?php if( !wcdn_has_shipping_address( $order ) ) : ?> no-shipping-address<?php endif; ?>">
		<div class="billing-address">
			<h3><?php _e( 'Billing Address', 'woocommerce-delivery-notes' ); ?></h3>
			<address>
				
				<?php if( !$order->get_formatted_billing_address() ) _e( 'N/A', 'woocommerce-delivery-notes' ); else echo apply_filters( 'wcdn_address_billing', $order->get_formatted_billing_address(), $order ); ?>
				
			</address>
		</div>
		
		<div class="shipping-address">
			<h3><?php _e( 'Shipping Address', 'woocommerce-delivery-notes' ); ?></h3>
			<address>

				<?php if( !$order->get_formatted_shipping_address() ) _e( 'N/A', 'woocommerce-delivery-notes' ); else echo apply_filters( 'wcdn_address_shipping', $order->get_formatted_shipping_address(), $order ); ?>
			
			</address>
		</div>
								
		<?php do_action( 'wcdn_after_addresses', $order ); ?>
	</div><!-- .order-addresses -->

	<div class="order-info">
		<h2><?= __('Cotización', 'baumchild') ?></h2>

		<ul class="info-list">
			<?php $fields = apply_filters( 'wcdn_order_info_fields', wcdn_get_order_info( $order ), $order ); 
			?>
			<?php foreach( $fields as $key => $field ) : ?>
				<li id="<?php $key ?>">
					<strong><?php echo apply_filters( 'wcdn_order_info_name', $field['label'], $field ); ?></strong>
					<span><?php echo apply_filters( 'wcdn_order_info_content', $field['value'], $field ); ?></span>
				</li>
			<?php endforeach; ?>
		</ul>
		
		<?php do_action( 'wcdn_after_info', $order ); ?>
	</div><!-- .order-info -->
	
	
	<div class="order-items">
		<table>
			<thead>
				<tr>
					<th class="head-name"><span><?php _e('Product', 'woocommerce-delivery-notes'); ?></span></th>
					<th class="head-quantity"><span><?php _e('Quantity', 'woocommerce-delivery-notes'); ?></span></th>
				</tr>
			</thead>
			
			<tbody>
				<?php 

				if( sizeof( $order->get_items() ) > 0 ) : ?>
					<?php foreach( $order->get_items() as $item ) : ?>
						
						<?php
							$product = apply_filters( 'wcdn_order_item_product', $order->get_product_from_item( $item ), $item );
							
							if ( version_compare( get_option( 'woocommerce_version' ), '3.0.0', ">="  ) ) {
							    $item_meta = new WC_Order_Item_Product( $item['item_meta'], $product );
							}else{
							    $item_meta = new WC_Order_Item_Meta( $item['item_meta'], $product );    
							} 
							
						?>
						
						<tr>
							<td class="product-name">
								<?php do_action( 'wcdn_order_item_before', $product, $order ); ?>

								<!-- <span class="sku-wrapper"><?php echo __('SKU', 'baumchild'); ?>: <span><?= $product->get_sku(); ?></span></span> -->
								<dl class="extras">
									<?php 

										$fields = apply_filters( 'wcdn_order_item_fields', array(), $product, $order ); 

										foreach ( $fields as $field ) : 
									?>
									
										<dt><?php echo $field['label']; ?></dt>
										<dd><?php echo $field['value']; ?></dd>
											
									<?php endforeach; ?>
								</dl>

								<span class="name"><?php 
								$product_id   =  $item['product_id'];
                                $prod_name    = get_post( $product_id );
                                $product_name = $prod_name->post_title;
                                

								echo apply_filters( 'wcdn_order_item_name', $product_name, $item ); ?></span>

								<?php
									$product_subtitle = get_post_meta($product_id, 'product_subtitle', true);

									if(!empty($product_subtitle)) :
								?>
										<br><em class="product-subtitle"><?= $product_subtitle ?></em>

								<?php
									endif;
								?>
							</td>
							<td class="product-quantity">
								<span><?php echo apply_filters( 'wcdn_order_item_quantity', $item['qty'], $item ); ?></span>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
			
			<tfoot>
				<?php do_action('baumchild_print_custom_data', $order->get_id()); ?>

				<?php if( $totals = $order->get_order_item_totals() ) : ?>
					<?php 
					foreach( $totals as $total ) : ?>
						<tr>
							<td class="total-name" colspan="3"><strong><?php echo $total['label']; ?></strong></td>
							<td class="total-price"><span><?php echo $total['value']; ?></span></td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</tfoot>
		</table>
							
		<?php do_action( 'wcdn_after_items', $order ); ?>
	</div><!-- .order-items -->
	
	<?php do_action('baumchild_bacs_payment_method', $order) ?>	
	
	<div class="order-notes">
		<?php if( wcdn_has_customer_notes( $order ) ) : ?>
			<h4><?php _e( 'Customer Note', 'woocommerce-delivery-notes' ); ?></h4>
			<?php wcdn_customer_notes( $order ); ?>
		<?php endif; ?>
		
		<?php do_action( 'wcdn_after_notes', $order ); ?>
	</div><!-- .order-notes -->
		
	
	<div class="order-thanks">
		<?php wcdn_personal_notes(); ?>
		
		<?php do_action( 'wcdn_after_thanks', $order ); ?>
	</div><!-- .order-thanks -->
		
		
	<div class="order-colophon">
		<div class="colophon-policies">
			<?php wcdn_policies_conditions(); ?>
		</div>
		
		<div class="colophon-imprint">
			<?php wcdn_imprint(); ?>
		</div>	
		
		<?php do_action( 'wcdn_after_colophon', $order ); ?>
	</div><!-- .order-colophon -->
				