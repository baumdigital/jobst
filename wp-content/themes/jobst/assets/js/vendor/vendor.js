"use strict";

/*jshint browser:true */

/*!
* FitVids 1.1
*
* Copyright 2013, Chris Coyier - http://css-tricks.com + Dave Rupert - http://daverupert.com
* Credit to Thierry Koblentz - http://www.alistapart.com/articles/creating-intrinsic-ratios-for-video/
* Released under the WTFPL license - http://sam.zoy.org/wtfpl/
*
*/
;

(function ($) {
  'use strict';

  $.fn.fitVids = function (options) {
    var settings = {
      customSelector: null,
      ignore: null
    };

    if (!document.getElementById('fit-vids-style')) {
      // appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
      var head = document.head || document.getElementsByTagName('head')[0];
      var css = '.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}';
      var div = document.createElement("div");
      div.innerHTML = '<p>x</p><style id="fit-vids-style">' + css + '</style>';
      head.appendChild(div.childNodes[1]);
    }

    if (options) {
      $.extend(settings, options);
    }

    return this.each(function () {
      var selectors = ['iframe[src*="player.vimeo.com"]', 'iframe[src*="youtube.com"]', 'iframe[src*="youtube-nocookie.com"]', 'iframe[src*="kickstarter.com"][src*="video.html"]', 'object', 'embed'];

      if (settings.customSelector) {
        selectors.push(settings.customSelector);
      }

      var ignoreList = '.fitvidsignore';

      if (settings.ignore) {
        ignoreList = ignoreList + ', ' + settings.ignore;
      }

      var $allVideos = $(this).find(selectors.join(','));
      $allVideos = $allVideos.not('object object'); // SwfObj conflict patch

      $allVideos = $allVideos.not(ignoreList); // Disable FitVids on this video.

      $allVideos.each(function () {
        var $this = $(this);

        if ($this.parents(ignoreList).length > 0) {
          return; // Disable FitVids on this video.
        }

        if (this.tagName.toLowerCase() === 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) {
          return;
        }

        if (!$this.css('height') && !$this.css('width') && (isNaN($this.attr('height')) || isNaN($this.attr('width')))) {
          $this.attr('height', 9);
          $this.attr('width', 16);
        }

        var height = this.tagName.toLowerCase() === 'object' || $this.attr('height') && !isNaN(parseInt($this.attr('height'), 10)) ? parseInt($this.attr('height'), 10) : $this.height(),
            width = !isNaN(parseInt($this.attr('width'), 10)) ? parseInt($this.attr('width'), 10) : $this.width(),
            aspectRatio = height / width;

        if (!$this.attr('name')) {
          var videoName = 'fitvid' + $.fn.fitVids._count;
          $this.attr('name', videoName);
          $.fn.fitVids._count++;
        }

        $this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', aspectRatio * 100 + '%');
        $this.removeAttr('height').removeAttr('width');
      });
    });
  }; // Internal counter for unique video names.


  $.fn.fitVids._count = 0; // Works with either jQuery or Zepto
})(window.jQuery || window.Zepto);
"use strict";

/*!
 *	dotdotdot JS 4.0.11
 *
 *	dotdotdot.frebsite.nl
 *
 *	Copyright (c) Fred Heusschen
 *	www.frebsite.nl
 *
 *	License: CC-BY-NC-4.0
 *	http://creativecommons.org/licenses/by-nc/4.0/
 */
var Dotdotdot = function () {
  function t(e, i) {
    void 0 === i && (i = t.options);
    var n = this;

    for (var o in this.container = e, this.options = i || {}, this.watchTimeout = null, this.watchInterval = null, this.resizeEvent = null, t.options) {
      t.options.hasOwnProperty(o) && void 0 === this.options[o] && (this.options[o] = t.options[o]);
    }

    var r = this.container.dotdotdot;
    r && r.destroy(), this.API = {}, ["truncate", "restore", "destroy", "watch", "unwatch"].forEach(function (t) {
      n.API[t] = function () {
        return n[t].call(n);
      };
    }), this.container.dotdotdot = this.API, this.originalStyle = this.container.getAttribute("style") || "", this.originalContent = this._getOriginalContent(), this.ellipsis = document.createTextNode(this.options.ellipsis);
    var s = window.getComputedStyle(this.container);
    "break-word" !== s["word-wrap"] && (this.container.style["word-wrap"] = "break-word"), "pre" === s["white-space"] ? this.container.style["white-space"] = "pre-wrap" : "nowrap" === s["white-space"] && (this.container.style["white-space"] = "normal"), null === this.options.height && (this.options.height = this._getMaxHeight()), this.truncate(), this.options.watch && this.watch();
  }

  return t.prototype.restore = function () {
    var t = this;
    this.unwatch(), this.container.setAttribute("style", this.originalStyle), this.container.classList.remove("ddd-truncated"), this.container.innerHTML = "", this.originalContent.forEach(function (e) {
      t.container.append(e);
    });
  }, t.prototype.destroy = function () {
    this.restore(), this.container.dotdotdot = null;
  }, t.prototype.watch = function () {
    var t = this;
    this.unwatch();

    var e = {
      width: null,
      height: null
    },
        i = function i(_i, n, o) {
      if (t.container.offsetWidth || t.container.offsetHeight || t.container.getClientRects().length) {
        var r = {
          width: _i[n],
          height: _i[o]
        };
        return e.width == r.width && e.height == r.height || t.truncate(), r;
      }

      return e;
    };

    "window" == this.options.watch ? (this.resizeEvent = function (n) {
      t.watchTimeout && clearTimeout(t.watchTimeout), t.watchTimeout = setTimeout(function () {
        e = i(window, "innerWidth", "innerHeight");
      }, 100);
    }, window.addEventListener("resize", this.resizeEvent)) : this.watchInterval = setInterval(function () {
      e = i(t.container, "clientWidth", "clientHeight");
    }, 1e3);
  }, t.prototype.unwatch = function () {
    this.resizeEvent && (window.removeEventListener("resize", this.resizeEvent), this.resizeEvent = null), this.watchInterval && clearInterval(this.watchInterval), this.watchTimeout && clearTimeout(this.watchTimeout);
  }, t.prototype.truncate = function () {
    var t = this,
        e = !1;
    return this.container.innerHTML = "", this.originalContent.forEach(function (e) {
      t.container.append(e.cloneNode(!0));
    }), this.maxHeight = this._getMaxHeight(), this._fits() || (e = !0, this._truncateToNode(this.container)), this.container.classList[e ? "add" : "remove"]("ddd-truncated"), this.options.callback.call(this.container, e), e;
  }, t.prototype._truncateToNode = function (e) {
    var i = [],
        n = [];

    if (t.$.contents(e).forEach(function (t) {
      if (1 != t.nodeType || !t.matches(".ddd-keep")) {
        var e = document.createComment("");
        t.replaceWith(e), n.push(t), i.push(e);
      }
    }), n.length) {
      for (var o = 0; o < n.length; o++) {
        i[o].replaceWith(n[o]);
        var r = this.ellipsis.cloneNode(!0);

        switch (n[o].nodeType) {
          case 1:
            n[o].append(r);
            break;

          case 3:
            n[o].after(r);
        }

        var s = this._fits();

        if (r.parentElement.removeChild(r), !s) {
          if ("node" == this.options.truncate && o > 1) return void n[o - 2].remove();
          break;
        }
      }

      for (var a = o; a < i.length; a++) {
        i[a].remove();
      }

      var h = n[Math.max(0, Math.min(o, n.length - 1))];

      if (1 == h.nodeType) {
        var c = document.createElement(h.nodeName);
        c.append(this.ellipsis), h.replaceWith(c), this._fits() ? c.replaceWith(h) : (c.remove(), h = n[Math.max(0, o - 1)]);
      }

      1 == h.nodeType ? this._truncateToNode(h) : this._truncateToWord(h);
    }
  }, t.prototype._truncateToWord = function (t) {
    for (var e = t.textContent, i = -1 !== e.indexOf(" ") ? " " : "　", n = e.split(i), o = n.length; o >= 0; o--) {
      if (t.textContent = this._addEllipsis(n.slice(0, o).join(i)), this._fits()) {
        "letter" == this.options.truncate && (t.textContent = n.slice(0, o + 1).join(i), this._truncateToLetter(t));
        break;
      }
    }
  }, t.prototype._truncateToLetter = function (t) {
    for (var e = t.textContent.split(""), i = "", n = e.length; n >= 0 && (!(i = e.slice(0, n).join("")).length || (t.textContent = this._addEllipsis(i), !this._fits())); n--) {
      ;
    }
  }, t.prototype._fits = function () {
    return this.container.scrollHeight <= this.maxHeight + this.options.tolerance;
  }, t.prototype._addEllipsis = function (t) {
    for (var e = [" ", "　", ",", ";", ".", "!", "?"]; e.indexOf(t.slice(-1)) > -1;) {
      t = t.slice(0, -1);
    }

    return t += this.ellipsis.textContent;
  }, t.prototype._getOriginalContent = function () {
    var e = "script, style";
    this.options.keep && (e += ", " + this.options.keep), t.$.find(e, this.container).forEach(function (t) {
      t.classList.add("ddd-keep");
    });
    var i = "div, section, article, header, footer, p, h1, h2, h3, h4, h5, h6, table, td, td, dt, dd, li";
    [this.container].concat(t.$.find("*", this.container)).forEach(function (e) {
      e.normalize(), t.$.contents(e).forEach(function (t) {
        8 == t.nodeType && e.removeChild(t);
      }), t.$.contents(e).forEach(function (t) {
        if (3 == t.nodeType && "" == t.textContent.trim()) {
          var n = t.previousSibling,
              o = t.nextSibling;
          (t.parentElement.matches("table, thead, tbody, tfoot, tr, dl, ul, ol, video") || !n || 1 == n.nodeType && n.matches(i) || !o || 1 == o.nodeType && o.matches(i)) && e.removeChild(t);
        }
      });
    });
    var n = [];
    return t.$.contents(this.container).forEach(function (t) {
      n.push(t.cloneNode(!0));
    }), n;
  }, t.prototype._getMaxHeight = function () {
    if ("number" == typeof this.options.height) return this.options.height;

    for (var t = window.getComputedStyle(this.container), e = ["maxHeight", "height"], i = 0, n = 0; n < e.length; n++) {
      if ("px" == (o = t[e[n]]).slice(-2)) {
        i = parseFloat(o);
        break;
      }
    }

    if ("border-box" == t.boxSizing) {
      e = ["borderTopWidth", "borderBottomWidth", "paddingTop", "paddingBottom"];

      for (n = 0; n < e.length; n++) {
        var o;
        "px" == (o = t[e[n]]).slice(-2) && (i -= parseFloat(o));
      }
    }

    return Math.max(i, 0);
  }, t.version = "4.0.11", t.options = {
    ellipsis: "… ",
    callback: function callback(t) {},
    truncate: "word",
    tolerance: 0,
    keep: null,
    watch: "window",
    height: null
  }, t.$ = {
    find: function find(t, e) {
      return e = e || document, Array.prototype.slice.call(e.querySelectorAll(t));
    },
    contents: function contents(t) {
      return t = t || document, Array.prototype.slice.call(t.childNodes);
    }
  }, t;
}();

!function (t) {
  void 0 !== t && (t.fn.dotdotdot = function (t) {
    return this.each(function (e, i) {
      var n = new Dotdotdot(i, t);
      i.dotdotdot = n.API;
    });
  });
}(window.Zepto || window.jQuery);
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*
* jquery-match-height 0.7.0 by @liabru
* http://brm.io/jquery-match-height/
* License MIT
*/
!function (t) {
  "use strict";

  "function" == typeof define && define.amd ? define(["jquery"], t) : "undefined" != typeof module && module.exports ? module.exports = t(require("jquery")) : t(jQuery);
}(function (t) {
  var e = -1,
      o = -1,
      i = function i(t) {
    return parseFloat(t) || 0;
  },
      a = function a(e) {
    var o = 1,
        a = t(e),
        n = null,
        r = [];
    return a.each(function () {
      var e = t(this),
          a = e.offset().top - i(e.css("margin-top")),
          s = r.length > 0 ? r[r.length - 1] : null;
      null === s ? r.push(e) : Math.floor(Math.abs(n - a)) <= o ? r[r.length - 1] = s.add(e) : r.push(e), n = a;
    }), r;
  },
      n = function n(e) {
    var o = {
      byRow: !0,
      property: "height",
      target: null,
      remove: !1
    };
    return "object" == _typeof(e) ? t.extend(o, e) : ("boolean" == typeof e ? o.byRow = e : "remove" === e && (o.remove = !0), o);
  },
      r = t.fn.matchHeight = function (e) {
    var o = n(e);

    if (o.remove) {
      var i = this;
      return this.css(o.property, ""), t.each(r._groups, function (t, e) {
        e.elements = e.elements.not(i);
      }), this;
    }

    return this.length <= 1 && !o.target ? this : (r._groups.push({
      elements: this,
      options: o
    }), r._apply(this, o), this);
  };

  r.version = "0.7.0", r._groups = [], r._throttle = 80, r._maintainScroll = !1, r._beforeUpdate = null, r._afterUpdate = null, r._rows = a, r._parse = i, r._parseOptions = n, r._apply = function (e, o) {
    var s = n(o),
        h = t(e),
        l = [h],
        c = t(window).scrollTop(),
        p = t("html").outerHeight(!0),
        d = h.parents().filter(":hidden");
    return d.each(function () {
      var e = t(this);
      e.data("style-cache", e.attr("style"));
    }), d.css("display", "block"), s.byRow && !s.target && (h.each(function () {
      var e = t(this),
          o = e.css("display");
      "inline-block" !== o && "flex" !== o && "inline-flex" !== o && (o = "block"), e.data("style-cache", e.attr("style")), e.css({
        display: o,
        "padding-top": "0",
        "padding-bottom": "0",
        "margin-top": "0",
        "margin-bottom": "0",
        "border-top-width": "0",
        "border-bottom-width": "0",
        height: "100px",
        overflow: "hidden"
      });
    }), l = a(h), h.each(function () {
      var e = t(this);
      e.attr("style", e.data("style-cache") || "");
    })), t.each(l, function (e, o) {
      var a = t(o),
          n = 0;
      if (s.target) n = s.target.outerHeight(!1);else {
        if (s.byRow && a.length <= 1) return void a.css(s.property, "");
        a.each(function () {
          var e = t(this),
              o = e.attr("style"),
              i = e.css("display");
          "inline-block" !== i && "flex" !== i && "inline-flex" !== i && (i = "block");
          var a = {
            display: i
          };
          a[s.property] = "", e.css(a), e.outerHeight(!1) > n && (n = e.outerHeight(!1)), o ? e.attr("style", o) : e.css("display", "");
        });
      }
      a.each(function () {
        var e = t(this),
            o = 0;
        s.target && e.is(s.target) || ("border-box" !== e.css("box-sizing") && (o += i(e.css("border-top-width")) + i(e.css("border-bottom-width")), o += i(e.css("padding-top")) + i(e.css("padding-bottom"))), e.css(s.property, n - o + "px"));
      });
    }), d.each(function () {
      var e = t(this);
      e.attr("style", e.data("style-cache") || null);
    }), r._maintainScroll && t(window).scrollTop(c / p * t("html").outerHeight(!0)), this;
  }, r._applyDataApi = function () {
    var e = {};
    t("[data-match-height], [data-mh]").each(function () {
      var o = t(this),
          i = o.attr("data-mh") || o.attr("data-match-height");
      i in e ? e[i] = e[i].add(o) : e[i] = o;
    }), t.each(e, function () {
      this.matchHeight(!0);
    });
  };

  var s = function s(e) {
    r._beforeUpdate && r._beforeUpdate(e, r._groups), t.each(r._groups, function () {
      r._apply(this.elements, this.options);
    }), r._afterUpdate && r._afterUpdate(e, r._groups);
  };

  r._update = function (i, a) {
    if (a && "resize" === a.type) {
      var n = t(window).width();
      if (n === e) return;
      e = n;
    }

    i ? -1 === o && (o = setTimeout(function () {
      s(a), o = -1;
    }, r._throttle)) : s(a);
  }, t(r._applyDataApi), t(window).bind("load", function (t) {
    r._update(!1, t);
  }), t(window).bind("resize orientationchange", function (t) {
    r._update(!0, t);
  });
});