"use strict";

jQuery(function ($) {
  /**
   ** Wishlist & Print button
   **/
  $(document).on('click', 'a.tinvwl_add_to_wishlist_button, .woocommerce .button.print', function () {
    $('body').addClass('bm-loader body-overflow');
  });
  $(document).on('click', '.tinv-close-modal, .tinvwl_button_close, .tinv-overlay', function () {
    $('body').removeClass('bm-loader body-overflow');
  });
  /**
   ** Print click loader
   **/

  $(document).on('printLinkComplete', function () {
    $('body').removeClass('bm-loader body-overflow');
  });
  /**
   ** Custom checked checkboxes
   **/

  $(document).on('click', 'input[type="checkbox"]', function () {
    console.log('click');

    if ($(this).is(':checked')) {
      $(this).parent('label[class*="checkbox"]').addClass('checked');
    } else {
      $(this).parent('label[class*="checkbox"]').removeClass('checked');
    }
  });
  /**
   ** Select per page
   **/

  $('.woocommerce-posts-per-page').on('change', 'select.posts-per-page', function () {
    $(this).closest('form').submit();
  });
  /**
   ** Allow only numbers in input type text
   */

  numbersOnly('.input-container input.input-text');

  function numbersOnly(s) {
    $(s).on('keydown', function (e) {
      -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/.test(e.keyCode) && (e.ctrlKey === true || e.metaKey === true) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault();
    });
  } // jQuery Fitvids


  $('iframe[src*="youtube"]').parent().fitVids();
});
"use strict";

jQuery(function ($) {
  var config = {
    /* Carrusel de Categorias del Menu Principal */
    slider_column_carrusel: {
      object_class: ".column-carousel > ul.mega-sub-menu",
      instancia: null,
      targetPrev: ".btn-prev",
      targetNext: ".btn-next",
      args: {
        item: 5,
        auto: false,
        enableDrag: true,
        enableTouch: true,
        controls: false,
        slideMargin: 0,
        freeMove: true,
        loop: false,
        pager: false,
        autoWidth: false,
        pauseOnHover: true,
        prevHtml: '<i class="fas fa-chevron-left"></i>',
        nextHtml: '<i class="fas fa-chevron-right"></i>',
        slideMove: 1,
        adaptiveHeight: true,
        responsive: [{
          breakpoint: 992,
          settings: {
            item: 4
          }
        }, {
          breakpoint: 860,
          settings: {
            item: 3
          }
        }],
        onSliderLoad: function onSliderLoad(el) {
          carouselWrapper(".column-carousel .lSSlideOuter");
          $(".column-carousel").removeClass("cS-hidden");
          console.log("%c onSliderLoad ", "background: yellow; color: black"); // ----- Console log

          $(".column-carousel").closest(".mega-menu-item").on("open_panel", function () {
            $(window).trigger("resize");
            console.log("%c trigger resize del Carrusel ", "background: green; color: white"); // ----- Console log
          });
        },
        onBeforeStart: function onBeforeStart(el) {
          clonarCategorias();
          console.log("%c onBeforeStart ", "background: #000; color: yellow"); // ----- Console log
        }
      }
    },

    /* Carrusel de Marcas del Home */
    carrusel_marcas: {
      object_class: ".wpb_image_grid_ul",
      instancia: null,
      args: {
        item: 5,
        prevHtml: '<i class="fa fa-angle-left"></i>',
        nextHtml: '<i class="fa fa-angle-right"></i>',
        enableDrag: true,
        enableTouch: true,
        pager: false,
        loop: true,
        responsive: [{
          breakpoint: 992,
          settings: {
            item: 3
          }
        }, {
          breakpoint: 768,
          settings: {
            item: 2
          }
        }, {
          breakpoint: 481,
          settings: {
            item: 1
          }
        }],
        onSliderLoad: function onSliderLoad(el) {
          if ($(config.carrusel_marcas.object_class).closest(".cS-hidden").length > 0) {
            $(config.carrusel_marcas.object_class).closest(".cS-hidden").removeClass("cS-hidden");
          }

          console.log("%c onSliderLoad carrusel_marcas", "background: cyan; color: black"); // ----- Console log
        }
      }
    },

    /* Carrusel deHistorias del Home */
    carrusel_historias: {
      object_class: "#post-grid-about .vc_pageable-slide-wrapper",
      instancia: null,
      args: {
        item: 2.7,
        //auto: true,
        //speed: 3500,
        //slideMove: 1,
        enableDrag: true,
        enableTouch: true,
        prevHtml: '<i class="fa fa-angle-left"></i>',
        nextHtml: '<i class="fa fa-angle-right"></i>',
        pager: false,
        loop: false,
        responsive: [{
          breakpoint: 992,
          settings: {
            item: 2.7
          }
        }, {
          breakpoint: 768,
          settings: {
            item: 2
          }
        }, {
          breakpoint: 481,
          settings: {
            item: 1.2
          }
        }],
        onSliderLoad: function onSliderLoad(el) {
          /*
                     if ($(config.carrusel_historias.object_class).closest('.cS-hidden').length > 0) {
                     $(config.carrusel_historias.object_class).closest('.cS-hidden').removeClass('cS-hidden');
                     }
                     */
          //console.log('%c onSliderLoad de Mabel => cS-hidden', 'background: cyan; color: black'); // ----- Console log
        }
      }
    }
  };
  $.each(config, function (index, element) {
    if (element.object_class != null && $(element.object_class).length > 0) {
      element.instancia = $(element.object_class).lightSlider(element.args);

      if (typeof element.targetPrev != "undefined" && element.targetPrev != null && $(element.targetPrev).length > 0) {
        $(document).on("click", element.targetPrev, function () {
          element.instancia.goToPrevSlide();
        });
      }

      if (typeof element.targetNext != "undefined" && element.targetNext != null && $(element.targetNext).length > 0) {
        $(document).on("click", element.targetNext, function () {
          element.instancia.goToNextSlide();
        });
      }
    }
  });

  function carouselWrapper(el) {
    if ($(el).length > 0) {
      $(el).wrap('<div class="main-carousel-wrapper"></div>');
    }
  }

  function clonarCategorias() {
    var aux = $("#mega-menu-primary .mega-item-productos");
    var menuProductosTop = $(aux).clone();
    $(menuProductosTop).attr("id", "_productos-mobile").removeClass("mega-item-productos item-productos").addClass("_productos-mobile");
    $(".mega-navs", $(menuProductosTop)).remove();
    $("li", $(menuProductosTop)).removeAttr("id");
    $(menuProductosTop).insertAfter(aux);
  }

  $(document).ready(function () {
    $(".listing-zonas-categories").lightSlider({
      item: 7,
      enableDrag: false,
      enableTouch: false,
      pager: false,
      loop: false,
      controls: false,
      //slideMargin: 60,
      responsive: [{
        breakpoint: 769,
        settings: {
          item: 4.4,
          enableDrag: true,
          enableTouch: true
        }
      }, {
        breakpoint: 481,
        settings: {
          item: 2.4,
          enableDrag: true,
          enableTouch: true
        }
      }]
    });
    $("#post-grid-tips .vc_pageable-slide-wrapper").lightSlider({
      item: 2,
      enableDrag: true,
      enableTouch: true,
      prevHtml: '<i class="fa fa-angle-left"></i>',
      nextHtml: '<i class="fa fa-angle-right"></i>',
      pager: false,
      loop: false,
      slideMargin: 0,
      responsive: [{
        breakpoint: 1400,
        settings: {
          item: 1.10
        }
      }, {
        breakpoint: 769,
        settings: {
          item: 1.1,
          pager: false
        }
      }, {
        breakpoint: 420,
        settings: {
          item: 1.05,
          pager: true
        }
      }],
      onAfterSlide: function onAfterSlide(el) {
        var current = el.getCurrentSlideCount();
        var total = el.getTotalSlideCount();
        var next = $('#post-grid-tips .lSNext');
        var prev = $('#post-grid-tips .lSPrev');

        if (current === total) {
          next.fadeOut();
        } else {
          next.fadeIn();
        }

        if (current === 1) {
          prev.fadeOut();
        } else {
          prev.fadeIn();
        }
      },
      onSliderLoad: function onSliderLoad(el) {
        $('#post-grid-tips .lSPrev').fadeOut();
      }
    });
  });
});
"use strict";

jQuery(function ($) {
  $(window).on("load resize", function () {
    var wwindow = window.innerWidth;
    /* ========== LISTADO de productos ========== */
    // Image container

    setTimeout(function () {
      if ($(".products").length > 0) {
        $("ul.products").each(function () {
          if (wwindow > 349) {
            if ($(this).find("li").length > 1) {
              $(this).find("li.product .product-image-container").matchHeight({
                byRow: true
              });
            } else {
              var height = $(this).find("li.product .product-image-container").height();
              $(this).find("li.product .product-image-container").height(height);
            }
          } else {
            $(this).find("li.product .product-image-container").matchHeight({
              remove: false
            });
          }
        });
      }

      MatchMe("li.product", ".product-title-container", 349, true);
    }, 800);
    $(document).on("success_modal", function () {
      setTimeout(function () {
        /* ========== DETALLE de producto ========== */
        MatchMe("#wpis-gallery", "#wpis-gallery .slick-slide", 100, false);
        MatchMe(".wpis-slider-for", ".wpis-slider-for .slick-slide", 300, true);
      }, 100);
    });
    $(document).on("added_to_cart", function () {
      setTimeout(function () {
        MatchMe(".xoo-cp-container", ".wbt-custom-product", 300, true);
      }, 100);
    });
    $(document).on("cp_additional_products", function () {
      setTimeout(function () {
        MatchMe(".xoo-cp-container .wbt-custom-product", ".variations .label", 300, true);
      }, 100);
    });
    setTimeout(function () {
      /* ========== PROCESO DE CHECKOUT ========== */
      MatchMe(".woocommerce-checkout", ".woocommerce-checkout label", 480, true); // Match => Home: Seccion de articulo
      //MatchMe('.woocommerce-checkout', '.woocommerce-checkout .form-row', 480, true);

      MatchMe(".stores-container.layout-grid ", ".store-content", 600, true);
      /* ========== form mmenu - column 50 ========== */

      MatchMe(".mm-panel .wpcf7-form", ".map-title", 319, true); // Match => Home: Seccion nuestra historia - grid post

      MatchMe(".vc_pageable-wrapper", ".vc_grid-item .vc_gitem-col", 480, true);
    }, 100);
  });
  /*
   * function Match Height
   * Autor: Francisco Chanto  - francisco@baum.digital
   * Requiere variable: var ww = window.innerWidth;
   * c = condicion => si el selector existe
   * s = selector  => al que se le aplica el plugin
   * q = media query => media query donde termina
   * _byRow = revisa si por cada linea hace falta aplicar el plugin
   */

  function MatchMe(_c, _s, _q, _byRow) {
    var ww = window.innerWidth;
    var _byRow = true;

    if ($(_c).length > 0) {
      ww > _q ? $(_s).matchHeight({
        byRow: _byRow
      }) : $(_s).matchHeight({
        remove: _byRow
      });
    }
  }

  $(document).ready(function () {
    // Card Match Height
    $('.wb-baum-card-title').matchHeight(); // Blog list

    $('.blog article').matchHeight();
    $('.blog article header.entry-header').matchHeight();
    $('.blog article .entry-content-excerpt').matchHeight();
    $('.archive article').matchHeight();
    $('.archive article header.entry-header').matchHeight();
    $('.archive article .entry-content-excerpt').matchHeight();
    $('.single-zona-venta').matchHeight(); // Fix blog list

    setTimeout(function () {
      $('.blog article header.entry-header').matchHeight();
      $('.blog article .entry-content-excerpt').matchHeight();
      $('.single-zona-venta').matchHeight();
    }, 1000);
  });
});
"use strict";

jQuery(function ($) {
  var mmenu_args = {
    extensions: ["fx-panels-slide-100", "pagedim-black", "position-right", "position-front"],
    close: false,
    navbar: false,
    onClick: {
      setSelected: true
    },
    keyboardNavigation: {
      enable: true,
      enhance: true
    }
  };
  var contactenos = $("#contactenos").mmenu(mmenu_args).data("mmenu"); // Mmenu -> Empl

  $(".mm-menu").each(function () {
    $(this).find(".mm-panel").append('<span class="et_close_search_field mmenu-close-button"><i class="fas fa-times"></i></span>');
  }); // Contactenos

  $(document).on("click", "#site-navigation-sec .menu-item-contacto a", function (e) {
    e.preventDefault();
    contactenos.open();
  });
  $(document).on("click", ".contact-button button", function (e) {
    e.preventDefault();
    contactenos.open();
  });
  $(document).on("click", ".button-contact a", function (e) {
    e.preventDefault();
    contactenos.open();
  });
  $(document).on("click", ".contact-button a", function (e) {
    e.preventDefault();
    contactenos.open();
  });
  $(document).on("click", "#contactenos .mmenu-close-button", function () {
    contactenos.close();
    return false;
  }); // Cotizar

  var cotizar = $("#cotizar").mmenu(mmenu_args).data("mmenu"); // Mmenu
  // console.log('cotizar:', $(".cotizar button"));

  $(document).on("click", ".cotizar button", function (e) {
    e.preventDefault();
    cotizar.open();
  });
  $(document).on("click", "#cotizar .mmenu-close-button", function () {
    cotizar.close();
    return false;
  }); //fix scroll transition

  $(".mm-tabstart").addClass("et_smooth_scroll_disabled");
});
"use strict";

jQuery(function ($) {
  /**
  ** Internal selects to select2
  **/
  function runSelect2(selector) {
    $(selector).select2({
      width: 'resolve',
      minimumResultsForSearch: Infinity
    });
  }

  runSelect2('select:not(.hide, .hidden, .country_select)');
  /**
  ** Select2 methods select
  **/

  function change_select_shipping_to_select2() {
    $('.select-select2, select.shipping_methods, .woocommerce-checkout select.shipping_method, select#shipping-pickup-store-select').select2({
      width: 'resolve',
      minimumResultsForSearch: Infinity
    });
  }

  change_select_shipping_to_select2();
  /**
  ** After update checkout
  **/

  $(document.body).on('update_checkout, updated_checkout', function () {
    change_select_shipping_to_select2();
  });
  /**
  ** Update cart amount
  **/

  $(document.body).on('updated_cart_totals', function () {
    change_select_shipping_to_select2();
  });
  /**
  ** Placeholder to select2 input search
  **/

  function placeholder_input_search(selector) {
    setTimeout(function () {
      $(selector).on("select2:open", function () {
        $(".select2-search__field").attr("placeholder", $(this).find('option:first').text());
      });
      $(selector).on("select2:close", function () {
        $(".select2-search__field").attr("placeholder", null);
      });
    }, 20);
  }

  placeholder_input_search('select.country_to_state, select.state_select, select.city_select');
  /**
  ** Ejecuta el Select2 en el Quickview
  **/

  $(document).on('success_modal', function () {
    if (!$('.xoo-qv-panel').hasClass('current-modal')) {
      runSelect2('.xoo-qv-summary select:not(.hide)');
    }
  });
});
"use strict";

jQuery(function ($) {
  var $variations_form_wrapper = $('.variations_form-wrapper');
  /**
  ** woobt_update_count trigger removed
  ** 27-12-2019 - KMA
  **/

  $('.woobt_products').find('.select select').change(function () {
    if ($(this).val() == '') {
      $(this).closest('.woobt-product').find('.woobt-checkbox').prop('checked', false);
    } else {
      $(this).closest('.woobt-product').find('.woobt-checkbox').prop('checked', true);
    }
  });
  /**
  ** found_variation trigger removed
  ** 27-12-2019 - KMA
  **/

  $(document).on('added_to_cart', function (fragments, cart_hash, button) {
    $variations_form_wrapper.addClass('bm-loader');

    var _woobt_ids = $('.xoo-qv-summary').length ? $('.xoo-qv-summary').find('.woobt_ids') : $('.woobt_ids'); // var product_id = ($('.xoo-qv-summary').length) ? $('.xoo-qv-summary').find('input[name="product_id"]').val() : $('input[name="product_id"]').val();


    var product_id_wrapper = $('.xoo-qv-main').length ? $('.xoo-qv-main') : $('.shop-wrapper');

    if (product_id_wrapper.length) {
      var product_id = product_id_wrapper.find('.product.entry').attr('id').split('-');
      product_id = product_id[1];
      var cp_container = $('.xoo-cp-container');
      cp_container.addClass('bm-loader'); // $('.xoo-cp-container .variations').each(function() {
      // 	$(this).find('select').val('').prop('disabled', false).trigger('change');
      // });

      var data = {
        action: 'baumchild_wbt_custom_items',
        woobt_ids: _woobt_ids.val(),
        product_id: product_id
      };
      $.post(baumchild_ajax.ajax_url, data).done(function (response) {
        cp_container.find('.added-to-cart-additionals').html(response.additional_products);

        var _selects = cp_container.find('.added-to-cart-additionals select');

        _selects.children('option[value="-1"]').remove();

        _selects.select2({
          width: 'resolve',
          minimumResultsForSearch: Infinity
        });

        $(document.body).trigger('cp_additional_products', [product_id]);
        $.each(response.products, function (key, value) {
          $.each(value, function (_key, _value) {
            var select = $('.xoo-cp-container').find('select[name="' + _key + '"]');

            if (select.length) {
              select.val(_value).prop('disabled', true).trigger('change');
            }

            select.closest('.variations').find('label').append('<span class="xoo-cp-icon-check"></span>');
          });
        });
        $variations_form_wrapper.removeClass('bm-loader');
        cp_container.removeClass('bm-loader');
      }).fail(function (error) {
        console.log('fail');
      }).always(function () {
        cp_container.removeClass('bm-loader');
      });
    }
  });
  $(document).on('success_modal', function () {
    var qv_container = $('.xoo-qv-main'); // var product_id = qv_container.find('.variations_form').data('product_id');

    var product_id = qv_container.find('.product.entry').attr('id').split('-');
    product_id = product_id[1];
    var data = {
      action: 'baumchild_get_additional_products',
      product_id: product_id
    };
    $.post(baumchild_ajax.ajax_url, data).done(function (response) {
      // console.log(JSON.stringify(response));
      qv_container.find('.additional-products-wrapper').html(response.additional_products);

      var _selects = qv_container.find('.additional-products-wrapper select');

      _selects.children('option[value="-1"]').remove();

      _selects.select2({
        width: 'resolve',
        minimumResultsForSearch: Infinity
      }); // Reset woobt_ids value


      $('.xoo-qv-summary').find('.woobt_ids').val('');
      $(document.body).trigger('qv_additional_products', [product_id]);
    }).fail(function (error) {
      console.log('fail');
    });
  });
  $(document).on('change', '.added-to-cart-additionals select', function () {
    var _this = $(this);

    var _this_id = $(this).attr('id'); // var _woobt_ids = ($('.xoo-qv-summary').length) ? $('.xoo-qv-summary').find('.woobt_ids') : $('.woobt_ids');


    var _wrapper = $('.xoo-qv-main').length ? $('.xoo-qv-main') : $('.shop-wrapper'); // var _cp_product = ($('.xoo-qv-main').length) ? $('.xoo-qv-main').find('.wbt-custom-product') : $('.shop-wrapper').find('.woobt-product');
    // var _cp_product = _wrapper.find('.wbt-custom-product');


    _wrapper.find('#' + _this_id).val(_this.val()).trigger('change');

    var variations_form = _wrapper.find('#' + _this_id).closest('.variations_form');

    var is_disabled = _this.prop('disabled');

    var data = {
      attributeFields: {},
      variationData: variations_form.data('product_variations')
    };
    data.attributeFields[_this.attr('name')] = _this.val();

    var variation = _findMatchingVariations(data.variationData, data.attributeFields);

    var t = variation[0];

    if (!is_disabled && _this.val() != '') {
      var data = {
        action: 'baumchild_ajax_add_to_cart',
        product_id: variations_form.data('product_id'),
        quantity: 1,
        variation_id: t['variation_id']
      };
      ajax_add_cart(data, _this.closest('.variations_form-wrapper'));
    }
  });
  $(document).on('change', '.xoo-qv-summary .additional-products-wrapper select', function () {
    var _this = $(this);

    var _qv_product = _this.closest('.wbt-custom-product');

    var variations_form = _this.closest('.variations_form');

    var data = {
      attributeFields: {},
      variationData: variations_form.data('product_variations')
    };
    data.attributeFields[_this.attr('name')] = _this.val();

    var variation = _findMatchingVariations(data.variationData, data.attributeFields);

    var t = variation[0];

    if (t['is_purchasable'] && t['is_in_stock']) {
      _qv_product.data('id', t['variation_id']);

      _qv_product.data('price-ori', t['display_price']);
    } else {
      _qv_product.data('id', 0);
    }

    if (_this.val() == '') _qv_product.data('id', 0);
    baumchild_qv_woobt_save_ids();
  });

  function ajax_add_cart(data, loader_wrapper) {
    loader_wrapper.addClass('bm-loader');
    $.post(wc_add_to_cart_params.ajax_url, data).done(function (response) {
      if (response.error & response.product_url) {
        window.location = response.product_url;
        return;
      } else {
        $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash]);
      }
    }).fail(function (error) {
      console.log(JSON.stringify(error));
    }).always(function () {
      loader_wrapper.removeClass('bm-loader');
    });
  }

  function baumchild_qv_woobt_save_ids() {
    var woobt_items = [];

    var _wrapper = $('.xoo-qv-summary');

    var _woobt_btn = _wrapper.find('.single_add_to_cart_button');

    var _woobt_products = _wrapper.find('.wbt-custom-product').each(function () {
      var item_id = $(this).data('id');
      if (item_id > 0) woobt_items.push(item_id + '/100%/1');
    });

    if (woobt_items.length > 0) {
      $('.xoo-qv-summary').find('.woobt_ids').val(woobt_items.join(','));
    } else {
      $('.xoo-qv-summary').find('.woobt_ids').val('');
    }
  }

  function _findMatchingVariations(variations, attributes) {
    var matching = [];

    for (var i = 0; i < variations.length; i++) {
      var variation = variations[i];

      if (_isMatch(variation.attributes, attributes)) {
        matching.push(variation);
      }
    }

    return matching;
  }

  function _isMatch(variation_attributes, attributes) {
    var match = true;

    for (var attr_name in variation_attributes) {
      if (variation_attributes.hasOwnProperty(attr_name)) {
        var val1 = variation_attributes[attr_name];
        var val2 = attributes[attr_name];

        if (val1 !== undefined && val2 !== undefined && val1.length !== 0 && val2.length !== 0 && val1 !== val2) {
          match = false;
        }
      }
    }

    return match;
  }
});
"use strict";

jQuery(function ($) {
  // Boton para abrir el buscador
  $('.search-button').on('click', function () {
    if (!$(this).hasClass('_search-is-open')) {
      openSearchBox();
    } else {
      closeSearchBox();
    }
  }); // Boton para cerrar el buscador

  $('.close-search, .search-overlay').on('click', function () {
    closeSearchBox();
  });

  function openSearchBox() {
    $('.search-button').addClass('_search-is-open');
    $('.search-form').addClass('search-is-open'); //$('body').addClass('body-overflow search-open');
  }

  function closeSearchBox() {
    $('.search-button').removeClass('_search-is-open');
    $('.search-form').removeClass('search-is-open');
    $('body').removeClass('body-overflow search-open');
    $('#ajaxsearchprores1_1').fadeOut("fast");
    $('.search-form #ajaxsearchpro1_1 input.orig').val("");
    $('.search-form').removeClass('search-is-open'); //$('body').removeClass('body-overflow search-open');
  }
});
"use strict";

jQuery(function ($) {
  /*
   * Cierra el menu principal
   */
  $(document).on('click', '.nav-overlay, .menu-item-buscador, .contact-button', function (e) {
    e.preventDefault();

    if ($('.site-header-menu-primary').hasClass('nav-open')) {
      if (!$(e.target).closest('#site-navigation').length) {
        closeMobileNav($('.site-header-toggle-btn'));
      }
    }
  });
  $(document).on('click', '.site-header-toggle-btn', function (e) {
    if (typeof $(this).attr('data-state') == 'undefined' || $(this).attr('data-state') == 1) {
      openMobileNav(this);
    } else {
      closeMobileNav(this);
    }
  });
  $(window).on('resize', function () {
    var ww = window.innerWidth;

    if (ww > 767) {
      closeMobileNav('.site-header-toggle-btn');
    }
  });

  function openMobileNav(target) {
    $('body').addClass('body-overflow');
    $('.site-header-menu-primary').addClass('nav-open');
    $('.site-header-toggle-btn').addClass('open');
    $('#site-navigation').animate({
      left: '0%'
    }, 400, function () {
      $(target).attr('data-state', 0);
    }); // Open products menu by default

    $('#_productos-mobile .mega-menu-link').addClass('_sub-menu-open');
    $('#_productos-mobile .mega-sub-menu').css('display', 'block');
  }

  function closeMobileNav(target) {
    $('.site-header-toggle-btn').removeClass('open');
    $('#site-navigation').animate({
      left: '-100%'
    }, 400, function () {
      $('.site-header-menu-primary').removeClass('nav-open');
      $('body').removeClass('body-overflow');
      $(target).attr('data-state', 1);
    });
  }
  /*
   *  Abre primer nivel Menu
   */


  $(document).on('click', '._productos-mobile > a ', function () {
    if ($(window).innerWidth() < 769) {
      $(this).toggleClass('_sub-menu-open');
      $(this).siblings('.mega-sub-menu').slideToggle(350);
    }
  });
  /*
   *  Abre los hijos de la categoria
   */

  $(document).on('click', '.mega-block-title', function () {
    if ($(window).innerWidth() < 769) {
      $(this).toggleClass('_sub-menu-open');
      $(this).siblings('.wc-product-categories').slideToggle(350);
    }
  });
});
"use strict";

jQuery(function ($) {
  /**
   ** Toggle de filtros en responsivo
   ** Agrega clase opened al button
   ** agrega la clase open-filter al section que contiene los filtros
   ** toggle class filter-drop-open al contenedor del listado
   ** Anima el button al top
   **/
  $("#toggle-filtros").on('click', function () {
    $(this).toggleClass('opened');
    $(this).siblings('section.widget').toggleClass('open-filters');
    $('.archive-container').toggleClass('filter-drop-open'); //        $('html, body').animate({scrollTop: $(this).offset().top - ($('header').height() + 30)}, 900, function () {
    //            $('body').toggleClass('body-overflow');
    //        });

    $('html, body').animate({
      scrollTop: $(this).offset().top - ($('header').height() + 30)
    }, 900, function () {
      $('body').toggleClass('body-overflow');
    });
  });
  /**
   ** Si ww < 600 se le aplica evento al H4 de cada filtro
   **/

  $(window).on('load resize', function () {
    var ww = window.innerWidth;

    if (ww > 600) {
      $('.woof_block_html_items').show();
    } else {
      $('.woof_block_html_items').hide();
    }
  });
  $(window).on('load resize', function () {
    var wsize = window.innerWidth;
    wsize >= 600 ? $('.woof_container_inner h4').removeClass('filter-action') : '';
  });
  $('.woof_container_inner h4').on('click', function (e) {
    var size = window.innerWidth;
    e.preventDefault();

    if (size <= 600) {
      $(this).toggleClass('filter-action');
      $(this).siblings('.woof_block_html_items').slideToggle();
    }
  });
  var compression = {
    'preventiva': {
      'term_id': '80',
      'content': ': 8-15 mmHg'
    },
    'baja': {
      'term_id': '39',
      'content': ': 15-20 mmHg'
    },
    'media': {
      'term_id': '38',
      'content': ': 20-30 mmHg'
    },
    'alta': {
      'term_id': '37',
      'content': ': 30-40 mmHg'
    }
  };

  for (var compressionKey in compression) {
    var term_id = compression[compressionKey].term_id;
    var content = compression[compressionKey].content;
    var selector = $('.woof_term_' + term_id + ' label');
    var prev_text = selector.text();
    selector.text(prev_text + content);
  }
});
"use strict";

jQuery(function ($) {
  /**
  ** Clase ready al ul en listado y detalle de producto
  **/
  if ($('body.post-type-archive-product ul.products > li, body.tax-product_cat ul.products > li, body.single-product ul.products  li.product').length > 1) {
    $('ul.products').addClass('ready');
  }

  $(window).on('load', function () {
    $('ul.products li.product img').css('opacity', '1');
  });

  if ($('body.single-product').length > 0) {
    $(window).on('load', function () {
      if ($('#wpis-gallery .slick-slide').length > 1) {
        $('#wpis-gallery').addClass('ready');
      }
    });
    $('.images section.wpis-slider-for').append('<span class="click-mobile fa"></span>');
    $(document).on('click', '.click-mobile', function () {
      $('.slick-current .wpis-popup').trigger('click');
    });
  }
  /**
  ** Aplica clase .quick-view-hover en el hover al li.product
  **/


  $('a.xoo-qv-button').on('mouseenter, mouseover ', function () {
    $(this).closest('li.product').addClass('quick-view-hover');
  });
  /**
  ** Remueve clase .quick-view-hover en el hover al li.product
  **/

  $('a.xoo-qv-button').on('mouseleave', function () {
    $(this).closest('li.product').removeClass('quick-view-hover');
  });
  /**
  ** Muestra las images del carrusel
  **/

  if ($('.product div.images').length > 0) {
    $(window).on('load', function () {
      $('.woocommerce .product div.images').animate({
        opacity: 1
      }, 800, function () {
        $(this).addClass('slick-loaded');
      });
    });
    $(document).on('change', '.div.xoo-qv-panel', function () {
      $('.woocommerce .product div.images').animate({
        opacity: 1
      }, 800, function () {
        $(this).addClass('slick-loaded');
      });
    });
  }
});
"use strict";

jQuery(function ($) {
  /**
  ** Add class to add overlay in quick view next/prev
  **/
  $(document).on('click', '.xoo-qv-nxt, .xoo-qv-prev', function () {
    $('.xoo-qv-top-panel').addClass('loading');
  });
});
"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

jQuery(function ($) {
  /**
  ** Gallery on success modal
  **/
  $(document).on('success_modal', function () {
    var _$$slick;

    var select = $('.xoo-qv-summary .variations select').first();
    var name = select.attr('name');
    if (select.hasClass('select_active')) select.removeClass('select_active');
    select.addClass('select_listing_active');
    $('.xoo-qv-images .wpis-slider-for').slick({
      fade: true,
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      asNavFor: '.wpis-slider-nav'
    });
    $('.xoo-qv-images .wpis-slider-nav').slick((_$$slick = {
      dots: false,
      arrows: true,
      centerMode: false,
      focusOnSelect: true,
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 1
    }, _defineProperty(_$$slick, "infinite", true), _defineProperty(_$$slick, "lazyLoad", 'ondemand'), _defineProperty(_$$slick, "asNavFor", '.wpis-slider-for'), _$$slick));
    $('.xoo-qv-images .wpis-slider-for .slick-track').addClass('woocommerce-product-gallery__image single-product-main-image');
    $('.xoo-qv-images .wpis-slider-nav .slick-track').addClass('flex-control-nav');
    $('.xoo-qv-images .wpis-slider-nav .slick-track li img').removeAttr('srcset');
    $('.variations select').change(function () {
      $('.xoo-qv-images .wpis-slider-nav').slick('slickGoTo', 0);
    });
  });
});
"use strict";

jQuery(function ($) {
  /**
   ** Punto Venta dropdown change trigger and redirect
   **/
  $(document).on('change', '.punto-venta-categories', function () {
    var url = $(this).find('option:selected').data('url');
    window.location.href = url;
  });
  /**
   ** Listing zonas filterables
   **/

  $(document).on('click', '.listing-zonas-categories .single-zona', function () {
    var _this = $(this);

    var zona = _this.data('zona');

    var tiendas = $('.listing-zonas > li');
    $('.listing-zonas-categories .single-zona').removeClass('active');

    _this.addClass('active');

    if (_this.hasClass('all-zonas')) {
      // tiendas.removeClass('is-animated').fadeOut('slow', function() {
      tiendas.removeClass('is-animated').fadeOut().promise().done(function () {
        tiendas.addClass('is-animated').fadeIn();
        get_the_fisrt_zona();
      });
    } else {
      // tiendas.removeClass('is-animated').fadeOut('slow', function() {
      tiendas.removeClass('is-animated').fadeOut().promise().done(function () {
        tiendas.filter('[class*="' + zona + '"]').addClass('is-animated').fadeIn();
        get_the_fisrt_zona();
      });
    } // jQuery Match height


    $('.single-zona-venta').matchHeight();
    return false;
  });
  get_the_fisrt_zona();

  function get_the_fisrt_zona() {
    var columns = $('.listing-zonas').data('columns');

    if ($(document).width() > 768 && $(document).width() < 993) {
      columns = 3;
    }

    if ($(document).width() < 769) {
      columns = 2;
    }

    $('.listing-zonas > li').removeClass('the-first');
    $('.listing-zonas > li.is-animated').filter(function (index) {
      return index % columns === 0;
    }).addClass('the-first');
  }

  $(window).on('resize', function () {
    get_the_fisrt_zona();
  });
});
"use strict";

jQuery(function ($) {
  /**
  ** Update cart in review order table in Checkout
  **/
  $('.woocommerce-checkout').on('change', 'input.qty', function () {
    var _this = $(this);

    var data = {
      action: 'baumchild_ajax_update_to_cart',
      cart_key: _this.closest('tr').data('cart_item_key'),
      product_sku: _this.closest('tr').find('.product-sku').text(),
      quantity: _this.val()
    };
    ajax_update_cart(data, false);
  });
  $(document).on('click', '.remove-product', function () {
    var _this = $(this);

    var data = {
      action: 'baumchild_ajax_update_to_cart',
      cart_key: _this.closest('tr').data('cart_item_key'),
      quantity: 0
    };
    ajax_update_cart(data, true);
    return false;
  });

  function ajax_update_cart(data, update_checkout) {
    $('#order_review').find('.shop_table').addClass('bm-loader');
    $.post(wc_add_to_cart_params.ajax_url, data).done(function (response) {
      if (response.error & response.product_url) {
        window.location = response.product_url;
        return;
      } else {
        $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash]);
      }

      $(document.body).trigger('update_cart_from_checkout');

      if (update_checkout) {
        setTimeout(function () {
          $('body').trigger('update_checkout');
        }, 250);
      }
    }).fail(function (error) {
      console.log(JSON.stringify(error));
    }).always(function () {
      $('#order_review').find('.shop_table').removeClass('bm-loader');
    });
  }
});
"use strict";

jQuery(function ($) {
  if ($('.woocommerce-view-order .woocommerce-bacs-bank-details').length) {
    $('.woocommerce-bacs-bank-details').toggle();
  }

  $('.btn-detalle-cuentas').click(function () {
    $('.woocommerce-bacs-bank-details').slideToggle();
    $(this).find('.toggle').toggle();
    return false;
  });
  $(document).on('click', '.btn-bacs_transaction', function () {
    var redirect = $('form#bacs_transaction input[name="redirect"]').val();

    if ($('form#bacs_transaction #bacs_transaction_code').val() == "") {
      $('form#bacs_transaction #bacs_transaction_code').addClass('required');
      $('.bacs_transaction_result').addClass('woocommerce-error').show().text('Agregue número de transferencia');
    } else if ($('form#bacs_transaction select#bacs_banco').val() == "") {
      $('.bacs_transaction_result').addClass('woocommerce-error').show().text('Seleccione una opción de banco');
    } else {
      var data = {
        action: 'baumchild_bacs_payment_save_custom_data',
        order_id: $('form#bacs_transaction input[name="orderid"]').val(),
        bacs_transaction_code: $('form#bacs_transaction #bacs_transaction_code').val(),
        bacs_banco: $('form#bacs_transaction #bacs_banco').val()
      };
      $(this).find('.fa').removeClass('hide');
      $.post(baumchild_ajax.ajax_url, data).done(function (response) {
        $('#bacs_transaction').slideUp();
        $('.bacs_transaction_result').removeClass('woocommerce-error').addClass('woocommerce-message').show().text('¡Número de transferencia guardado, la orden será procesada!');
        setTimeout(function () {
          window.location.href = redirect;
        }, 2000);
      }).fail(function (error) {
        $('.bacs_transaction_result').addClass('woocommerce-error').show().text('¡Ocurrió algo, intente luego!');
      }).always(function () {
        $('.btn-bacs_transaction').find('.fa').addClass('hide');
      });
    }

    setTimeout(function () {
      $('.bacs_transaction_result.woocommerce-error').fadeOut('slow');
    }, 3500);
    return false;
  });
  $('#bacs_transaction').on('submit', function () {
    $('.btn-bacs_transaction').trigger('click');
    return false;
  });
  $('form#bacs_transaction #bacs_transaction_code').on('blur change keyup keypress', function () {
    if ($(this).val() == "") {
      $(this).addClass('required');
    } else {
      $(this).removeClass('required');
    }
  });
});
"use strict";

jQuery(function ($) {
  /**
  ** Add to Cart loader
  ** Removed, now it is using the loader from Added to Cart popup WC plugin
  ** 28-11-2019 - KMA
  **/

  /**
  ** Toggle checkout actions
  **/
  $('.woocommerce-checkout-notifications .woocommerce-info a').click(function () {
    var parent = $(this).parent().parent();
    remove_coupon_message(0);
    $('[class*="woocommerce-form-"]').not(parent).removeClass('open');

    if (parent.hasClass('open')) {
      parent.removeClass('open');
    } else {
      parent.addClass('open');
    }

    if (parent.hasClass('open')) {
      $('body').addClass('overlay');
    } else {
      $('body').removeClass('overlay');
    }
  });
  /**
  ** Toggle overlay
  **/

  $(document).on('click', 'body.overlay', function (e) {
    if (!$(e.target).closest('.open + form').length) {
      remove_checkout_info_overlay();
    }
  });

  function remove_checkout_info_overlay() {
    $('body').removeClass('overlay');
    $('[class*="woocommerce-form-"]').removeClass('open');
  }

  function remove_coupon_message(time) {
    // var elements = '[class*="woocommerce-form"] .woocommerce-error, .woocommerce-checkout .woocommerce-message';
    var elements = '.woocommerce-checkout-notifications .woocommerce-error, .woocommerce-checkout .woocommerce-message';
    setTimeout(function () {
      $(elements).fadeOut('slow', function () {
        setTimeout(function () {
          $(elements).remove();
        }, 60);
      });
    }, time);
  }
  /**
  ** Habilitar el boton en formulario cupones
  **/


  $(document).on('change keyup keypress blur', '#coupon_code', function () {
    if ($(this).val() != "") {
      $(this).parent().find('.button').removeAttr('disabled');
    } else {
      $(this).parent().find('.button').attr('disabled', 'disabled');
    }
  });
  /**
  ** Activar check para crear cuenta en checkout
  **/

  $("#account_password, #account_confirm_password").on("blur", function () {
    var _account = $("#account_password");

    var _account_conf = $("#account_confirm_password");

    if (_account.val() != "" || _account_conf.val() != "") {
      $("#createaccount").prop("checked", true);
    } else {
      $("#createaccount").prop("checked", false);
    }
  });
  /**
  ** jQuery checkout fields validation
  **/

  $(document).on('change', '#payment_methods', function () {
    $('.payment_box').not('.payment_method_' + $(this).val()).slideUp();
    $('.payment_box.payment_method_' + $(this).val()).slideDown();
  }); // placeholder: '8888-8888'

  $("#billing_phone, #billing_celular").mask("0000-0000");
  $('#account_email, #billing_email').blur(function () {
    $(this).val($(this).val().split(' ').join(''));
  });
  /**
  ** Shipping calculator as dropdown
  **/

  $(document).on("change", ".shipping_methods", function () {
    var _this = $(this).val();

    if (_this == "calculate_shipping") {
      $(".shipping-calculator-button").trigger("click");
    } else {
      if ($(".shipping-calculator-form").is(':visible')) {
        $(".shipping-calculator-button").trigger("click");
      }

      $('select.shipping_method option[value="' + _this + '"]').prop('selected', true);
      $('select.shipping_method').trigger("change");
    }
  });
  /**
  ** After update checkout
  **/

  $(document.body).on('update_checkout, updated_checkout', function () {
    validate_checkout_required_fields();
    remove_checkout_info_overlay();
    remove_coupon_message(1500);
  });

  function validate_checkout_required_fields() {
    if (jQuery('select.shipping_method').val() == "wc_pickup_store") {
      $('#billing_state_field, #billing_city_field, #billing_address_1_field').find(".required").hide();
      $('#billing_state_field, #billing_city_field, #billing_address_1_field').removeClass("validate-required woocommerce-validated woocommerce-invalid woocommerce-invalid-required-field");
    } else {
      if ($('#billing_state_field .required, #billing_city_field .required, #billing_address_1_field .required').length < 1) {
        $('#billing_state_field label, #billing_city_field label, #billing_address_1_field label').append('<abbr class="required" title="obligatorio">*</abbr>');
      } else {
        $('#billing_state_field, #billing_city_field, #billing_address_1_field').find(".required").show();
      }

      $('#billing_state_field, #billing_city_field, #billing_address_1_field').addClass("validate-required");
    }
  }
});
"use strict";

jQuery(function ($) {
  $(window).on('load', function () {
    if ($('.woocommerce-cart-form input.qty').length) {
      check_input_qty();
    }

    if ($('.single-product').length) {
      check_stock($(".quantity input.qty"));
      check_max_attr($('.single-product input.qty'));
    }
  });
  $(document).on('click', '.quantity-up', function (e) {
    input_qty = $(this).parents('.input-container').find('.qty');

    if (check_stock(input_qty)) {
      input_qty.val(function (i, cur_val) {
        return ++cur_val;
      });
      input_qty.change();
    }

    return false;
  });
  $(document).on('click', '.quantity-down', function (e) {
    input_qty = $(this).parents('.input-container').find('.qty');
    input_qty.val(function (i, cur_val) {
      return cur_val > 1 ? --cur_val : 0;
    });
    input_qty.change();
    return false;
  });
  $(document).on('focus', 'input.qty', function () {
    $(this).select();
  });
  $(document).on('change blur keyup', 'input.qty', function () {
    var _this = $(this);

    setTimeout(function () {
      check_stock(_this);
    }, 50);
  });
  /* Update order-review table */

  $(document).on('update_cart_from_checkout', function () {
    if ($('#order_review tr.cart_item input.qty').length) {
      $('#order_review tr.cart_item').each(function () {
        setTimeout(function () {
          check_stock($(this).find('input.qty'));
        }, 50);
      });
    }
  });

  function enable_qty_input(pos, input) {
    input.parent().find('.quantity-' + pos).removeClass('quantity-disabled');
  }

  function disable_qty_input(pos, input) {
    input.parent().find('.quantity-' + pos).addClass('quantity-disabled');
  }

  function check_stock(_this) {
    var val = parseFloat(_this.val());
    var max_stock = parseFloat(_this.attr('max'));
    var min_stock = _this.attr('min') > 0 ? parseFloat(_this.attr('min')) : 1;
    var current = parseFloat(_this.data('qty'));

    if (val <= max_stock || val > min_stock) {
      enable_qty_input('down', _this);
      enable_qty_input('up', _this);
    }

    if (val == max_stock || isNaN(max_stock)) {
      disable_qty_input('up', _this);
    }

    if (val == 1) {
      disable_qty_input('down', _this);
    } else if (val > max_stock) {
      _this.val(min_stock);

      _this.trigger('change');

      _this.select();
    }

    return true;
  }
  /**
  ** Update cart amount
  **/


  $(document.body).on('updated_cart_totals', function () {
    check_input_qty();
  });
  check_input_qty();
  $(document).on("woocommerce_variation_select_change", ".variations_form", function () {
    setTimeout(function () {
      check_stock($(".quantity input.qty"));
    }, 80);
  });
  $(document).on('success_modal', function () {
    check_stock($(".xoo-qv-summary .quantity input.qty"));
  });

  function check_input_qty() {
    $(".woocommerce-cart-form input.qty").each(function () {
      var _this = $(this);

      _this.data("qty", _this.val());

      check_stock(_this);
      check_max_attr(_this);
    });
  }

  function check_max_attr(_this) {
    if (_this.attr('max') == "") {
      _this.attr('max', _this.val());
    }
  }
});
"use strict";

jQuery(function ($) {
  $(window).on('load', function () {
    var url = window.location.href;

    if (url.indexOf("#registro-btn") >= 0) {
      $("#login-woocommerce, #register-woocommerce").fadeToggle('slow').toggleClass('open');
    }
  });
  $("#registro-btn, #login-btn").click(function () {
    var site_content_top = $('.site-content').position().top;
    $("html, body").animate({
      scrollTop: site_content_top
    }, 300);
    $("#login-woocommerce, #register-woocommerce").fadeToggle('slow').toggleClass('open');
    window.history.pushState(null, "", "#" + $(this).attr("id"));
  });
  $(".toggle-account").click(function () {
    $(".menu-edit-account").toggle();
  });
});
"use strict";

jQuery(function ($) {
  /**
  ** Variations trigger
  **/
  $(document).on("woocommerce_variation_select_change", ".variations_form", function () {
    $(this).find('input.qty').val(1);
    setTimeout(function () {
      if ($(this).find('input.qty').attr('max') == "") {
        $(this).find('input.qty').attr('max', 1);
      }
    }, 80);
  });
  $(document).on('change blur keyup', 'input.qty', function () {
    var _this = $(this);

    var _val = parseFloat(_this.val());

    var _max = parseFloat(_this.attr('max'));

    var _min = _this.attr('min') > 0 ? parseFloat(_this.attr('min')) : 1;

    if (_val > _this.attr('max')) {
      _this.val(_min);

      _this.trigger('change');
    }

    if (_val <= _max) {
      enable_qty_input('down', _this);
      enable_qty_input('up', _this);
    }

    if (_val == _max) {
      disable_qty_input('up', _this);
    }

    if (_val == 1) {
      disable_qty_input('down', _this);
    }
  });

  function enable_qty_input(pos, input) {
    input.parent().find('.quantity-' + pos).removeClass('quantity-disabled');
  }

  function disable_qty_input(pos, input) {
    input.parent().find('.quantity-' + pos).addClass('quantity-disabled');
  } // Disabled click on variations


  jQuery('.variable-item').click(function () {
    console.log('Click disabled for theme, remove on select-variations.js');
    return false;
  });
});
"use strict";

jQuery(function ($) {
  /**
   ** Tricks
   **/

  /*
  $(window).on('load', function () {
      if ($('.woocommerce-breadcrumb').length ) {
          $('.woocommerce-breadcrumb').remove();
      }
  });
  */
});