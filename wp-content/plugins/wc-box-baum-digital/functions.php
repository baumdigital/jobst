<?php
/**
 * Load Languages
 */

/**
 * Include Favpress Framework
 */
require_once 'favpress-framework/bootstrap.php';

/**
 * Include Custom Data Sources
 */
/**
 * Load options, metaboxes, and shortcode generator array templates.
 */
// options
// metaboxes
$tmpl_mb1  = plugin_dir_path( __FILE__ ) . '/admin/metabox/wcbox_meta.php';
$tmpl_mb8  = plugin_dir_path( __FILE__ ) . '/admin/metabox/wcbox_meta_side.php';

/**
** Added by Baum Digital Team
** Metabox for carousel title
**/
include plugin_dir_path(__FILE__) . '/public/wc-taxonomy-fields.php';
include plugin_dir_path(__FILE__) . '/public/wcbox-by-baum.php';
include plugin_dir_path(__FILE__) . '/public/vc_wcbox-by-baum.php';

/**
 * Create instance of Options
 */

/**
 * Create instances of Metaboxes
 */
$mb1 = new FavPress_Metabox($tmpl_mb1);
$mb8 = new FavPress_Metabox($tmpl_mb8);

/*
 * EOF
 */