<?php
// Add Shortcode
add_shortcode('wcbox_slider', 'wcbox_shortcode_cat');
function wcbox_shortcode_cat($atts) {
	extract(shortcode_atts(array(
		// a few default values
		'id' => '',
		'cols_m' => '',
		'cols_t' => '',
		'cols_d' => '',
		'extra_id' => '',
		'title' => ''
	)
	, $atts));
	ob_start();
	global $post, $product;
	// query_posts('p=' . $id . '&post_type=wcbox');
	$query_args = array(
		'post_type' => 'wcbox',
		'post_status' => 'publish',
		'post__in' => array($id),
	);
	$query = new WP_Query($query_args);

	/*
	 *  * @Autor: Shirley Pérez <shirley@baum.digital>
	 */
	// Get the queried object and sanitize it
	$current_page = sanitize_post($GLOBALS['wp_the_query']->get_queried_object());
	// Get the page id
	$page_id = $current_page->ID;
	$terms = get_the_terms($page_id, 'product_cat');
	if ($terms && !is_wp_error($terms)) :
		foreach ($terms as $term) {
			$product_cat_id = $term->term_id;
			$product_cat_name = $term->name;
			$product_cat_slug = $term->slug;
			if($term->parent != 0) {
				$product_cat_id = $term->parent;
				$parent_cat = get_term_by('term_id', $product_cat_id, 'product_cat');
				$product_cat_slug = $parent_cat->slug;
			}
			$slug_complementary_cat = get_term_meta($product_cat_id, 'categoria_complementaria', true);
			$slug_related_cat = get_term_meta($product_cat_id, 'categoria_relacionada', true);
			
			if(!empty($slug_complementary_cat) || !empty($slug_related_cat))
				break;
		}
	endif;

	if($query->have_posts()):
		while ($query->have_posts()): $query->the_post();
			// metabox
			// Tab Options
			$animation_effect = favpress_metabox('wcbox_meta.wcbox_tav.0.wcbox_effect');
			$tab_ative_color = favpress_metabox('wcbox_meta.wcbox_tav.0.wcbox_cl_2');
			$tab_bg_active = favpress_metabox('wcbox_meta.wcbox_tav.0.wcbox_cl_1');
			$tab_txt_color = favpress_metabox('wcbox_meta.wcbox_tav.0.wcbox_ss_1');
			// Product Options
			$img_hover = favpress_metabox('wcbox_meta.wcbox_product_option.0.wcbox_cl_1');
			$hover_ico = favpress_metabox('wcbox_meta.wcbox_product_option.0.wcbox_hover_ico');
			$product_title_color = favpress_metabox('wcbox_meta.wcbox_product_option.0.wcbox_cl_2');
			$product_rating_color = favpress_metabox('wcbox_meta.wcbox_product_option.0.wcbox_cl_3');
			$show_title = favpress_metabox('wcbox_meta.wcbox_product_option.0.wcbox_rb_1');
			$show_rating = favpress_metabox('wcbox_meta.wcbox_product_option.0.wcbox_rb_2');
			$show_price = favpress_metabox('wcbox_meta.wcbox_product_option.0.wcbox_rb_3');
			$show_btn = favpress_metabox('wcbox_meta.wcbox_product_option.0.wcbox_rb_4');
			// Carousel Options
			$product_margin = favpress_metabox('wcbox_meta.wcbox_carousel_option.0.wcbox_product_margin');
			$product_loop = favpress_metabox('wcbox_meta.wcbox_carousel_option.0.wcbox_rb_1');
			$product_stage = favpress_metabox('wcbox_meta.wcbox_carousel_option.0.wcbox_padding_satge');
			$navgiation = favpress_metabox('wcbox_meta.wcbox_carousel_option.0.wcbox_nav_2');
			$pagination = favpress_metabox('wcbox_meta.wcbox_carousel_option.0.wcbox_dots_2');
			$wcbox_autoplay = favpress_metabox('wcbox_meta.wcbox_carousel_option.0.wcbox_autoplay');
			$wcbox_autoplayTimeout = favpress_metabox('wcbox_meta.wcbox_carousel_option.0.wcbox_autoplayTimeout');
			// Responsive Layout 
			$wcbox_colum = favpress_metabox('wcbox_meta.wcbox_carousel_option.0.wcbox_cl_1');
			$wcbox_colum_t = favpress_metabox('wcbox_meta.wcbox_carousel_option.0.wcbox_cl_2');
			$wcbox_colum_m = favpress_metabox('wcbox_meta.wcbox_carousel_option.0.wcbox_cl_3');
			$query_ppp = favpress_metabox('wcbox_meta.wcbox_get_q.0.wcbox_ppp_cat');
			$cat_ppp = favpress_metabox('wcbox_meta.wcbox_get_q.0.wcbox_categories_1');
			$tag_ppp = favpress_metabox('wcbox_meta.wcbox_get_q.0.wcbox_tags_1');
			//Filtter
			$wcbox_q = favpress_metabox('wcbox_meta.wcbox_get_q.0.filter_type');
			$wcbox_sidebar_slider = favpress_metabox('wcbox_widget_id.group.0.ss_check');
			$wcbox_sidebar = favpress_metabox('wcbox_widget_id.group.0.enable_widget');
			// metabox

			if ($wcbox_q == 'category') {
				$slides = favpress_metabox('wcbox_meta.wcbox_get_q.0.filter_category');
			} elseif ($wcbox_q == 'tags') {
				$slides = favpress_metabox('wcbox_meta.wcbox_get_q.0.filter_tag');
			} elseif ($wcbox_q == 'query') {
				$slides = favpress_metabox('wcbox_meta.wcbox_get_q.0.filter_query');
			} elseif ($wcbox_q == 'specific_id') {
				$slides = favpress_metabox('wcbox_meta.wcbox_get_q.0.filter_id');
			}

			/* Shortcode columns */
			$wcbox_colum = !empty($cols_d) ? $cols_d : $wcbox_colum;
			$wcbox_colum_t = !empty($cols_t) ? $cols_t : $wcbox_colum_t;
			$wcbox_colum_m = !empty($cols_m) ? $cols_m : $wcbox_colum_m;
			$extra_id = !empty($extra_id) ? $extra_id : '';

			?>
			<div class="carousel-container">
				<?php

				if ($wcbox_q == 'specific_id') {
					$slides = favpress_metabox('wcbox_meta.wcbox_get_q.0.filter_id');

					$args = array(
						'post__in' => $slides,
						'post_type' => 'product',
						'ignore_sticky_post' => 1
					);
					$the_query = new WP_Query($args);
					if ($the_query->have_posts()) :
						$title = empty($title) ? get_post_meta($id, 'wcbox_post_title', true) : $title;

						if(!empty($title)) :
							?>
							<div class="slider-header">
								<?php

								if(!empty($title))
									echo '<h3 class="wcbox-carousel-title">' . $title . '</h3>'
								?>
							</div>
							<?php
						endif;
					endif;

					require( plugin_dir_path(__FILE__) . 'wrapper_shortcode.php');
				}
				if ($wcbox_q == 'query') {
					foreach ($slides as $slide) {
						$wcbox_query = "";

						if($slide == 'related_products') {
							$wcbox_qf = array(
								'post_type' => 'product',
								'posts_per_page' => $query_ppp,
								'meta_query' => WC()->query->get_meta_query(),
								'order' => 'desc',
								'orderby' => 'date',
								'product_cat' => $product_cat_slug,
								'post__not_in' => array($page_id)
							);
							$wcbox_query = new WP_Query($wcbox_qf);
						}

						if($slide == 'recently_viewed_products') {
							$viewed_products = ! empty( $_COOKIE['woocommerce_recently_viewed'] ) ? (array) explode( '|', $_COOKIE['woocommerce_recently_viewed'] ) : array();
							$viewed_products = array_reverse( array_filter( array_map( 'absint', $viewed_products ) ) );
							$viewed_products = array_diff($viewed_products, [$page_id]); // Exclude current product				

							if ( empty( $viewed_products ) ) {
								return;
							}
							$query_args = array(
								'posts_per_page' => $query_ppp,
								'no_found_rows'  => 1,
								'post_status'    => 'publish',
								'post_type'      => 'product',
								'post__in'       => $viewed_products,
								'orderby' => 'rand'
							);

							if ( 'yes' === get_option( 'woocommerce_hide_out_of_stock_items' ) ) {
								$query_args['tax_query'] = array(
									array(
										'taxonomy' => 'product_visibility',
										'field'    => 'name',
										'terms'    => 'outofstock',
										'operator' => 'NOT IN',
									),
								);
							}

							$wcbox_query = new WP_Query($query_args);
						}

						if($slide == 'most_viewed_products') {
							$wcbox_qf = array(
								'post_type' => 'product',
								'posts_per_page' => '' . $query_ppp . '',
								'meta_key' => 'post_views_count',
								'orderby' => 'rand',
							);
							$wcbox_query = new WP_Query($wcbox_qf);
						}

						if($slide == 'featured_products') {
							$wcbox_qf = array(
								'post_type' => 'product',
								'posts_per_page' => '' . $query_ppp . '',
								'meta_key' => '_featured',
								'meta_value' => 'yes',
								'orderby' => 'date',
								'order' => 'desc'
							);
							$wcbox_query = new WP_Query($wcbox_qf);
						}
						
						if($slide == 'top_rated_products') {
							add_filter('posts_clauses', array(WC()->query, 'order_by_rating_post_clauses'));
							$query_args = array('posts_per_page' => $query_ppp, 'no_found_rows' => 1, 'post_status' => 'publish', 'post_type' => 'product');
							$query_args['meta_query'] = WC()->query->get_meta_query();
							$wcbox_query = new WP_Query($query_args);
						}

						if($slide == 'best_selling_products') {
							$wcbox_qf = array(
								'post_type' => 'product',
								'posts_per_page' => '' . $query_ppp . '',
								'meta_key' => 'total_sales',
								'orderby' => 'meta_value_num',
							);
							$wcbox_query = new WP_Query($wcbox_qf);
						}

						if($slide == 'sale_products') {
							$wcbox_qf = array(
								'posts_per_page' => $query_ppp,
								'post_status' => 'publish',
								'post_type' => 'product',
								'meta_query' => WC()->query->get_meta_query(),
								'post__in' => array_merge(array(0), wc_get_product_ids_on_sale())
							);
							$wcbox_query = new WP_Query($wcbox_qf);
						}

						if($slide == 'recent_products') {
							$wcbox_qf = array(
								'post_type' => 'product',
								'posts_per_page' => $query_ppp,
								'meta_query' => WC()->query->get_meta_query(),
								'order' => 'desc',
								'orderby' => 'date'
							);
							$wcbox_query = new WP_Query($wcbox_qf);
						}

						if($slide == 'related_products_cat') {
							$wcbox_qf = array(
								'post_type' => 'product',
								'posts_per_page' => $query_ppp,
								'meta_query' => WC()->query->get_meta_query(),
								'orderby' => 'rand',
								'post__not_in' => array($page_id)
							);
							$wcbox_qf['tax_query'] = array(
								array(
									'taxonomy' => 'product_cat',
									'field' => 'slug',
									'terms' => array_map( 'sanitize_title', explode( ',', $slug_related_cat ) ),
									'operator' => 'IN',
								)
							);

							$wcbox_query = new WP_Query($wcbox_qf);
						}

						if($slide == 'complementary_products') {
							$wcbox_qf = array(
								'post_type' => 'product',
								'posts_per_page' => $query_ppp,
								'meta_query' => WC()->query->get_meta_query(),
								'orderby' => 'rand',
								'post__not_in' => array($page_id)
							);
							$wcbox_qf['tax_query'] = array(
								array(
									'taxonomy' => 'product_cat',
									'field' => 'slug',
									'terms' => array_map( 'sanitize_title', explode( ',', $slug_complementary_cat ) ),
									'operator' => 'IN',
								)
							);

							$wcbox_query = new WP_Query($wcbox_qf);
						}

						if(!empty($wcbox_query) && $wcbox_query->have_posts()) {
							$title = empty($title) ? get_post_meta($id, 'wcbox_post_title', true) : $title;

							if(!empty($title)) :
								?>
								<div class="slider-header">
									<?php

									if(!empty($title))
										echo '<h3 class="wcbox-carousel-title">' . $title . '</h3>'
									?>
								</div>
								<?php
							endif;
						}

						require( plugin_dir_path(__FILE__) . 'wrapper_shortcode.php');
					}
				}
				if ($wcbox_q == 'category' || $wcbox_q == 'tags') {
					foreach ($slides as $slide) {
						if ($wcbox_q == 'category') {
							$cat_id_wcbox = wcbox_get_id('' . $slide . '');
							$args = array(
								'posts_per_page' => '' . $cat_ppp . '',
								'post_type' => 'product',
								'product_cat' => '' . $cat_id_wcbox . '',
								'ignore_sticky_post' => 1
							);
						} elseif ($wcbox_q == 'tags') {
							$args = array(
								'posts_per_page' => '' . $tag_ppp . '',
								'post_type' => 'product',
								'product_tag' => '' . $slide . '',
								'ignore_sticky_post' => 1
							);
						}
						$the_query = new WP_Query($args);
						if ($the_query->have_posts()) :
							$title = empty($title) ? get_post_meta($id, 'wcbox_post_title', true) : $title;

							if(!empty($title)) :
								?>
								<div class="slider-header">
									<?php

									if(!empty($title))
										echo '<h3 class="wcbox-carousel-title">' . $title . '</h3>'
									?>
								</div>
								<?php
							endif;
						endif;

						require( plugin_dir_path(__FILE__) . 'wrapper_shortcode.php');
					}
				}

				?>
			</div><!-- .carousel-container -->
			<?php

		endwhile;
	endif;
	wp_reset_query();

	return ob_get_clean();
}
