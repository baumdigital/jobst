<?php
switch ($wcbox_get_cat) {
	case 'most_viewed_products':
		if ($wcbox_query->have_posts()) :
			while ($wcbox_query->have_posts()) :
				$wcbox_query->the_post();
				$product = wc_get_product($wcbox_query->post->ID);
					// Output product information here									
				require 'product_shortcode.php';
			endwhile;
		endif;
		wp_reset_postdata();
	break;
	// Break
	case 'featured_products':
		if ($wcbox_query->have_posts()) :
			while ($wcbox_query->have_posts()) :
				$wcbox_query->the_post();
				$product = wc_get_product($wcbox_query->post->ID);
					// Output product information here									
				require 'product_shortcode.php';
			endwhile;
		endif;
		wp_reset_postdata();
	break;
	// Break
	case 'top_rated_products':
		if ($wcbox_query->have_posts()) {
			while ($wcbox_query->have_posts()) {
				$wcbox_query->the_post();
					// Output product information here									
				require 'product_shortcode.php';
			}
		}
		remove_filter('posts_clauses', array(WC()->query, 'order_by_rating_post_clauses'));
		wp_reset_postdata();
	break;
	//break
	case 'best_selling_products':
		if ($wcbox_query->have_posts()) :
			while ($wcbox_query->have_posts()) :
				$wcbox_query->the_post();
				$product = wc_get_product($wcbox_query->post->ID);
					// Output product information here									
				require 'product_shortcode.php';
			endwhile;
		endif;
		wp_reset_postdata();
	break;
	//break
	case 'sale_products':
		if ($wcbox_query->have_posts()) :
			while ($wcbox_query->have_posts()) :
				$wcbox_query->the_post();
				$product = wc_get_product($wcbox_query->post->ID);
					// Output product information here					
				require 'product_shortcode.php';
			endwhile;
		endif;
		wp_reset_postdata();
	break;
	//break
	case 'recent_products':
		if ($wcbox_query->have_posts()) :
			while ($wcbox_query->have_posts()) :
				$wcbox_query->the_post();
				$product = wc_get_product($wcbox_query->post->ID);
				// Output product information here					
				require 'product_shortcode.php';
			endwhile;
		endif;
		wp_reset_postdata();
	break;
	//break

	case 'related_products':
		$count_post = $wcbox_query->found_posts;
		if ($wcbox_query->have_posts()) :
			while ($wcbox_query->have_posts()) :
				$wcbox_query->the_post();
				$product = wc_get_product($wcbox_query->post->ID);
					// Output product information here	
				require 'product_shortcode.php';
			endwhile;
		endif;
		wp_reset_postdata();
	break;
	
	case 'related_products_cat':
		if ($wcbox_query->have_posts()) :
			while ($wcbox_query->have_posts()) :
				$wcbox_query->the_post();
				$product = wc_get_product($wcbox_query->post->ID);
				// Output product information here
				require 'product_shortcode.php';
			endwhile;
		endif;
		wp_reset_postdata();
	break;

	case 'complementary_products':
		if ($wcbox_query->have_posts()) :
			while ($wcbox_query->have_posts()) :
				$wcbox_query->the_post();
				$product = wc_get_product($wcbox_query->post->ID);
				// Output product information here
				require 'product_shortcode.php';
			endwhile;
		endif;
		wp_reset_postdata();
	break;
		
	case 'recently_viewed_products':
		if ($wcbox_query->have_posts()) :
			while ($wcbox_query->have_posts()) :
				$wcbox_query->the_post();
				$product = wc_get_product($wcbox_query->post->ID);
				require 'product_shortcode.php';
			endwhile;
		endif;
		wp_reset_postdata();
	break;
}