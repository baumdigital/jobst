<?php
$id        = 'wcbox_slider_' . $id;
$unique_id = ! empty( $extra_id ) ? $extra_id : $id
?>
<!-- - - - - - -  - - - HORIZONTAL TAB - - - - - -  - - - - - -->
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".<?php echo $unique_id; ?> .owlw").owlCarousel({
            loop:<?php echo $product_loop; ?>,
            dots: <?php echo $pagination; ?>,
            nav: <?php echo $navgiation; ?>,
            items:<?php
			if ( $wcbox_sidebar == 'yes' ) {
				if ( $wcbox_sidebar_slider == 'slider' ) {
					echo '1';
				} else {
					echo $wcbox_colum;
				}
			} else {
				echo $wcbox_colum;
			}
			?>,
            margin:<?php echo $product_margin; ?>,
            autoplay: <?php echo $wcbox_autoplay; ?>, // type false | if you don't want auto play
            autoplayTimeout: <?php echo $wcbox_autoplayTimeout; ?>, // 1 sec = 1000
            navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
            responsive: {
                0: {
                    items:<?php
					if ( $wcbox_sidebar == 'yes' ) {
						if ( $wcbox_sidebar_slider == 'slider' ) {
							echo '1';
						} else {
							echo $wcbox_colum;
						}
					} else {
						echo $wcbox_colum_m;
					}
					?>
                },
                480: {
                    items:<?php
					if ( $wcbox_sidebar == 'yes' ) {
						if ( $wcbox_sidebar_slider == 'slider' ) {
							echo '1';
						} else {
							echo $wcbox_colum;
						}
					} else {
						echo $wcbox_colum_m;
					}
					?>
                },
                768: {
                    items:<?php
					if ( $wcbox_sidebar == 'yes' ) {
						if ( $wcbox_sidebar_slider == 'slider' ) {
							echo '1';
						} else {
							echo $wcbox_colum;
						}
					} else {
						echo $wcbox_colum_t;
					}
					?>
                },
                980: {
                    items:<?php
					if ( $wcbox_sidebar == 'yes' ) {
						if ( $wcbox_sidebar_slider == 'slider' ) {
							echo '1';
						} else {
							echo $wcbox_colum;
						}
					} else {
						echo $wcbox_colum;
					}
					?>
                }
            },
        });
    });
</script>
<div class="<?php echo $unique_id; ?>">
    <!-- - - - - Tab Content - - - - - -->
    <div class="tt_container">
        <div class=" tt_tab">
            <div class="woocommerce">
				<?php if ( $wcbox_q == 'query' ) : ?>
                    <ul class="products <?php
					if ( $wcbox_sidebar == 'yes' ) {
						if ( $wcbox_sidebar_slider == 'slider' ) {
							echo 'owlw';
						} else {

						}
					} else {
						echo 'owlw';
					}
					?>">
						<?php
						$wcbox_get_cat = '' . $slide . '';
						// Product Details
						if ( $wcbox_sidebar == 'yes' ) {
							if ( $wcbox_sidebar_slider == 'slider' ) {
								require( plugin_dir_path( __FILE__ ) . 'wcbox_tab_shortcode.php' );
							} else {
								$wcbox_get_cat = '' . $slide . '';
								require( plugin_dir_path( __FILE__ ) . 'wcbox_tab_widget.php' );
							}
						} else {
							require( plugin_dir_path( __FILE__ ) . 'wcbox_tab_shortcode.php' );
						}
						?>
                    </ul>
				<?php elseif ( $wcbox_q == 'specific_id' ) : ?>
                    <ul class="products <?php
					if ( $wcbox_sidebar == 'yes' ) {
						if ( $wcbox_sidebar_slider == 'slider' ) {
							echo 'owlw';
						} else {

						}
					} else {
						echo 'owlw';
					}
					?>">
						<?php
						if ( $the_query->have_posts() ) :
							while ( $the_query->have_posts() ) : $the_query->the_post();
								// Product Details
								if ( $wcbox_sidebar == 'yes' ) {
									if ( $wcbox_sidebar_slider == 'slider' ) {
										require( plugin_dir_path( __FILE__ ) . 'product_shortcode.php' );
									} else {
										require( plugin_dir_path( __FILE__ ) . 'product_widgett.php' );
									}
								} else {
									require( plugin_dir_path( __FILE__ ) . 'product_shortcode.php' );
								}
							endwhile;
						endif;
						?>
                    </ul>
				<?php elseif ( $wcbox_q == 'category' || $wcbox_q == 'tags' ) : ?>
                    <ul class="products <?php
					if ( $wcbox_sidebar == 'yes' ) {
						if ( $wcbox_sidebar_slider == 'slider' ) {
							echo 'owlw';
						} else {

						}
					} else {
						echo 'owlw';
					}
					?>">
						<?php
						if ( $the_query->have_posts() ) :
							while ( $the_query->have_posts() ) : $the_query->the_post();
								require( plugin_dir_path( __FILE__ ) . 'product_shortcode.php' );
							endwhile;
						endif;
						?>
                    </ul>
				<?php endif; ?>
            </div>
        </div>
    </div>
</div>
