<?php global $product;
?>
<div class=" wcbox_product product woocommerce wcbox_carousel_product ">
	<div class="wcbox_product_img" >
			<?php
			// Product Image
				if ( has_post_thumbnail() ) {
					$wc_image_url=	 wp_get_attachment_image_src( get_post_thumbnail_id(),'shop_catalog');
				echo '<img src="'.$wc_image_url[0].'" alt="'.get_the_title().'"/>';
				} 
				else{ echo '<img src="'.wc_placeholder_img_src('woocommerce_get_image_size_shop_thumbnail').'" alt="" />'; }
				
			
			 //Quick View Desahabilitar pop up
			// echo '<span ezmodal-target="#quick_view_modal" data-href="' . $product->get_permalink() . '" data-original-title="' . esc_attr__( 'Quick View', 'wcbox' ) . '" class="wcbox-quick-view"></span>';
			 ?>
		</div>
		<div class="wcbox_product_details">
		<?php if ( $product->is_on_sale() ){
			echo apply_filters( 'woocommerce_sale_flash', '<a href="'.get_the_permalink().'" class="wcbox_box_onsale">' . __( 'Sale!', 'wc-box' ) . '</a>',  $product );
		 }
		 ?>
		<a href="<?php the_permalink(); ?>" class="wcbox_product_title">
			<?php if($show_title == 'true'){the_title();}
			
			if($show_rating == 'true'){
				 if($product->get_rating_html()){
					?>
					<span class="wcbox_product_rating"><?php echo $product->get_rating_html(); ?></span>
					<?php
				}
			}
			?>	
		</a>
			<span class="wcbox_product_title"><?php if($show_price == 'true'){echo $product->get_price_html();} ?></span>

                        
                                <?php
        /* start add wishlist shirley@baum.digital */
        if (!defined('ABSPATH')) {
            exit; // Exit if accessed directly.
        }
        ?>
        <input type="hidden" name="product_id" value="<?php echo esc_attr(version_compare(WC_VERSION, '3.0.0', '<') ? $product->id : ( $product->is_type('variation') ? $product->get_parent_id() : $product->get_id() ) ); ?>" />
        <?php if (version_compare(WC_VERSION, '3.0.0', '<') ? $product->variation_id : ( $product->is_type('variation') ? $product->get_id() : 0 )) { ?>
            <input type="hidden" name="variation_id" value="<?php echo esc_attr(version_compare(WC_VERSION, '3.0.0', '<') ? $product->variation_id : ( $product->is_type('variation') ? $product->get_id() : 0 ) ); ?>" />
        <?php } ?>
        <div class="tinv-wraper woocommerce tinv-wishlist <?php echo esc_attr($class_postion) ?>">
            <?php do_action('tinv_wishlist_addtowishlist_button'); ?>
            <?php do_action('tinv_wishlist_addtowishlist_dialogbox'); ?>
            <div class="tinvwl-tooltip"><?php esc_html_e('Add to Wishlist', 'ti-woocommerce-wishlist'); ?></div>
        </div>

        <?php /* end add wishlist shirley@baum.digital */ ?>
		
	</div>

</div>	