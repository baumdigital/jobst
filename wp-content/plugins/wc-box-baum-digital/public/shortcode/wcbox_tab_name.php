<?php
switch ($wcbox_get_cat_name) {
	case 'recent_products':
		echo 'Recent Products';
		break;
	case 'sale_products':
		echo 'Sales Products';
		break;
	case 'best_selling_products':
		echo 'Best Selling Products';
		break;
	case 'top_rated_products':
		echo 'Top Rated Products';
		break;
	case 'featured_products':
		echo 'Featured Products';
		break;
	case 'most_viewed_products':
		echo 'Most Viewed Products';
		break;
	case 'related_products':
		echo 'Related Products';
		break;
	case 'related_products_cat':
		echo 'Productos Relacionados (Baum Digital)';
		break;
	case 'complementary_products':
		echo 'Productos Complementarios (Baum Digital)';
		break;
	case 'recently_viewed_products':
		echo 'Productos Vistos Recientemente (Baum Digital)';
		break;
}