<?php
if(class_exists('WPBakeryShortCode')) {
	class VC_WCbox_Customizations extends WPBakeryShortCode {
		function __construct() {
			add_action('init', array($this, 'vc_wcbox_carousel_mapping'));
			add_shortcode('vc_wcbox_carousel', array($this, 'vc_wcbox_carousel_html'));
		}

		// Elements Mapping
		public function vc_wcbox_carousel_mapping() {
			// Stop all if VC is not enabled
			if (!defined('WPB_VC_VERSION')) { return; }

			$carousels = get_posts( 'post_type="wcbox"&numberposts=-1' );

			$wcbox = $vc_params = array();
			if ( $carousels ) {
				foreach ( $carousels as $carousel ) {
					$wcbox[$carousel->post_title] = $carousel->ID;
				}
			} else {
				$wcbox[ __( 'No carousels found', 'wcbox_baum' ) ] = 0;
			}

			$vc_params[] = array(
				'type' => 'dropdown',
				'heading' => __( 'Select carousel', 'wcbox_baum' ),
				'param_name' => 'id',
				'value' => $wcbox,
				'save_always' => true,
				'description' => __( 'Choose previously created carousel from the drop down list.', 'wcbox_baum' ),
			);

			$wcbox_widget = new Wcbox_Widgets();
			foreach ($wcbox_widget->columns_array() as $key => $column) {
				$vc_params[] = array(
					'type' => 'dropdown',
					'heading' => $column,
					'param_name' => $key,
					'value' => array(
						'Default' => 0,
						'2' => 2,
						'3' => 3,
						'4' => 4
					),
					'std' => 0,
					'save_always' => true,
					'description' => $column . ' override'
				); 
			}

			$vc_params[] = array(
				'type' => 'textfield',
				'heading' => __('Extra ID', 'wcbox_baum'),
				'param_name' => 'extra_id',
				'save_always' => true,
				'value' => '',
				'description' => __( 'Use extra ID if columns numbers are not defaults. Ex: wcbox_[title]', 'wcbox_baum' ),
			);

			$vc_params[] = array(
				'type' => 'textfield',
				'heading' => __('Sobreescribir título', 'wcbox_baum'),
				'param_name' => 'override_title',
				'save_always' => true,
				'description' => __( 'Override original title.', 'wcbox_baum' ),
			);

			// Map the block with vc_map()
			vc_map(
				array(
					'name' => __('Wcbox Carousel', 'wcbox'),
					'base' => 'vc_wcbox_carousel',
					'description' => __('Add carousel to the content', 'wcbox'),
					'category' => __('Content', 'wcbox'),
					'icon' => plugin_dir_url(__DIR__) . '/assets/images/placeholder.jpg',
					'params' => $vc_params,
				)
			);
		}

		// Elements HTML
		public function vc_wcbox_carousel_html($atts) {
			// Params extraction
			extract( $atts );
			$params = array();
			$params[] = !empty($atts['id']) ? 'id="' . esc_attr($atts['id']) . '"' : '';
			$params[] = !empty($atts['extra_id']) ? 'extra_id="' . esc_attr($atts['extra_id']) . '"' : '';
			$params[] = !empty($atts['override_title']) ? 'title="' . esc_attr($atts['override_title']) . '"' : '';

			$wcbox_widget = new Wcbox_Widgets();
			foreach ($wcbox_widget->columns_array() as $key => $column) {
				$params[] = ($atts[$key] != -1) ? str_replace('wcbox_', '', $key) . '="' . esc_attr($atts[$key]) . '"' : '';
			}
			ob_start();
			?>

			<div class="vc-carousel-wrap carousel-<?php echo $atts['id']; ?>">
				<?php
				echo apply_filters( 'vc_wcbox_carousel_shortcode', do_shortcode( '[wcbox_slider ' . implode(' ', $params) . ']' ) );
				?>
			</div>

			<?php
			// Fill $html var with data
			$html = ob_get_contents();
			ob_end_clean();
			return $html;
		}
	}
	new VC_WCbox_Customizations();
}
