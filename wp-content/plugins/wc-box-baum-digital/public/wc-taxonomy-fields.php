<?php
/**
** Add custom option to select complementary/related
** custom categories to use them in wcbox carousels
**/

function wcbox_baum_taxonomy_fields() {
	$arr = array(
		'categoria_complementaria' => __('Categoría Complementaria', 'wcbox_baum'),
		'categoria_relacionada' => __('Categoría Relacionada', 'wcbox_baum')
	);

	return $arr;
}

function wcbox_baum_product_cat_register_meta() {
	foreach (wcbox_baum_taxonomy_fields() as $key => $value) {
		register_meta('term', $key, 'wcbox_baum_sanitize_details');
	}
}
add_action('init', 'wcbox_baum_product_cat_register_meta');

function wcbox_baum_sanitize_details($key) {
	return wp_kses_post($key);
}

function wcbox_baum_product_cat_edit_details_meta($term) {
	wp_nonce_field( basename( __FILE__ ), 'wcbox_baum_product_cat_details_nonce' );
	?>
	<tr class="form-fields">
		<th colspan="2">
			<h3><?= __('Categorías', 'wcbox_baum') ?></h3>
			<p class="description"><?= __('Seleccione las categorías a usar en carruseles.', 'wcbox_baum') ?></p>
		</th>
	</tr>
	<?php
	foreach (wcbox_baum_taxonomy_fields() as $key => $value) {
		$field = (!empty(get_term_meta( $term->term_id, $key, true )) ? get_term_meta( $term->term_id, $key, true ) : array());
		$cats = get_categories(array('taxonomy' => 'product_cat', 'orderby' => 'name', 'exclude' => $term->term_id));
		// $cats = get_terms('product_cat', array('orderby' => 'name'));
	?>

		<tr class="form-field">
			<th scope="row" valign="top"><label for="<?= $key ?>"><?= $value ?></label></th>
			<td>
				<select class="select-multiple" name="<?php echo $key; ?>[]" id="<?php echo $key; ?>" multiple="multiple" style="width: 100%">
					<?php foreach ( $cats as $cat_list ) { ?>
						<option value="<?php echo $cat_list->cat_ID; ?>" <?php selected( true, in_array($cat_list->cat_ID, $field ) ); ?>><?php echo $cat_list->cat_name; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
	<?php
	}
}
add_action('product_cat_edit_form_fields', 'wcbox_baum_product_cat_edit_details_meta', 100);

function wcbox_baum_get_taxonomy_screen() {
    $current_screen = get_current_screen();
    if( $current_screen->id === "edit-product_cat" ) {
    ?>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$('.select-multiple').select2({
					placeholder: 'Select category/categories',
					allowClear: true,
					multiple:true,
				    width: 'resolve'
				});
			});
		</script>
    <?php
    }
}
// add_action('current_screen', 'wcbox_baum_get_taxonomy_screen');
add_action('admin_footer', 'wcbox_baum_get_taxonomy_screen');

function wcbox_baum_product_cat_details_meta_save( $term_id ) {
	if ( ! isset( $_POST['wcbox_baum_product_cat_details_nonce'] ) || ! wp_verify_nonce( $_POST['wcbox_baum_product_cat_details_nonce'], basename( __FILE__ ) ) ) {
		return;
	}

	foreach (wcbox_baum_taxonomy_fields() as $key => $value) {
		update_term_meta($term_id, $key, wcbox_baum_sanitize_details($_POST[$key]));
	}
}
add_action('edit_product_cat', 'wcbox_baum_product_cat_details_meta_save');