<?php
function wcbox_baum_carousel_scripts() {
	?>
	<script>
		jQuery(function($) {
			/**
			** Carousel nav
			**/
			$(document).on('ready load', show_carousel_arrows);
			$(window).resize(show_carousel_arrows);
			
			function show_carousel_arrows() {
				if($('.owl-carousel').length > 0) {
					$('.owl-carousel').each(function() {
						if($(this).find('.owl-stage').width() < $(window).width()) {
							$(this).find('.owl-nav').hide();
						} else {
							$(this).find('.owl-nav').show();
						}
					});
				}
			}
		});
	</script>
	<?php
}
add_action('wp_footer', 'wcbox_baum_carousel_scripts');

/**
** Recently Viewed products
**/
function wcbox_baum_track_product_view() {
	if ( ! is_singular( 'product' ) ) {
		return;
	}
	global $post;
	if ( empty( $_COOKIE['woocommerce_recently_viewed'] ) )
		$viewed_products = array();
	else
		$viewed_products = (array) explode( '|', $_COOKIE['woocommerce_recently_viewed'] );
	if ( ! in_array( $post->ID, $viewed_products ) ) {
		$viewed_products[] = $post->ID;
	}
	if ( sizeof( $viewed_products ) > 15 ) {
		array_shift( $viewed_products );
	}
	// Store for session only
	wc_setcookie('woocommerce_recently_viewed', implode( '|', $viewed_products ));
}
add_action('template_redirect', 'wcbox_baum_track_product_view', 20);

/**
** Metabox title
**/
function wcbox_item_title(){
	add_meta_box('wc-box-item-title', 'Slider Title', 'meta_box_wcbox_item_title', 'wcbox', 'side', 'high');
}
add_action('add_meta_boxes', 'wcbox_item_title');

function meta_box_wcbox_item_title() {
	$pid = get_the_ID();
	$slider_title = get_post_meta($pid, 'wcbox_post_title', true);
	wp_nonce_field('save_wcbox_item_title', 'wcbox_item_title_meta_box_nonce');
	?>
	<div class="wcbox_item_title">
		<p><input type="text" name="slider_title" class="large-text" value="<?php echo $slider_title; ?>" /></p>
	</div>
	<?php
}

function save_wcbox_item_title($post_id) {
	// Check if our nonce is set.
	if(!isset($_POST['wcbox_item_title_meta_box_nonce'])) { return; }

	// Verify that the nonce is valid.
	if(!wp_verify_nonce($_POST['wcbox_item_title_meta_box_nonce'], 'save_wcbox_item_title')) { return; }

	// Make sure that it is set.
	if(!isset($_POST['slider_title'])) { return; }

	// Sanitize user input.
	$slider_title = sanitize_text_field($_POST['slider_title']);

	// Update the meta field in the database.
	update_post_meta($post_id, 'wcbox_post_title', $slider_title);
}
add_action( 'save_post', 'save_wcbox_item_title' );

/**
** Widget WcBox
**/
function wc_box_load_widget() {
	register_widget('WcBox_Widgets');
}
add_action('widgets_init', 'wc_box_load_widget');

// Creating the widget 
class Wcbox_Widgets extends WP_Widget {
	function __construct() {
		parent::__construct(
			'wc_box_widget',
			__('WC Box by Baum', 'wc_box_widget'),
			array(
				'description' => __('Muestra los carruseles', 'wc_box_widget')
			)
		);
	}

	// Widget Backend 
	public function form( $instance ) {
		$wcbox_post_id = (isset($instance['wcbox_post_id'])) ? esc_attr($instance['wcbox_post_id']) : '';
		$wcbox_extra_id = (isset($instance['wcbox_extra_id']) && !empty($instance['wcbox_extra_id'])) ? esc_attr($instance['wcbox_extra_id']) : 'wcbox_ex_id_' . rand(100, 999);
		$wcbox_override_title = (isset($instance['wcbox_override_title'])) ? esc_attr($instance['wcbox_override_title']) : get_post_meta($wcbox_post_id, 'wcbox_post_title', true);
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'wcbox_post_id' ); ?>"> <?php echo __( 'Carousel to Display:', 'wcbox_baum' ) ?>
				<select class="widefat" id="<?php echo $this->get_field_id( 'wcbox_post_id' ); ?>" name="<?php echo $this->get_field_name( 'wcbox_post_id' ); ?>">
					<option value="-1"><?= __('None', 'wcbox_baum') ?></option>
					<?php
						$args = array( 'post_type' => 'wcbox', 'numberposts' => -1, 'order' => 'ASC' );
						$wcbox_posts = get_posts( $args );
						if ($wcbox_posts) {
							foreach( $wcbox_posts as $wcbox ) : setup_postdata( $wcbox );
								echo '<option value="' . $wcbox->ID . '"';
								if( $wcbox_post_id == $wcbox->ID ) {
									echo ' selected';
									$widget_extra_title = $wcbox->post_title;
								};
								echo '>' . $wcbox->post_title . '</option>';
							endforeach;
						} else {
							echo '<option value="">' . __( 'No carousels available', 'wcbox_baum' ) . '</option>';
						};
					?>
				</select>
			</label>
		</p>
		<?php foreach ($this->columns_array() as $key => $column) : ?>
			<p>
				<label for="<?php echo $this->get_field_id($key); ?>"> <?= $column ?>
					<select class="widefat" id="<?php echo $this->get_field_id($key); ?>" name="<?php echo $this->get_field_name($key); ?>">
						<option value="-1"><?= __('Default', 'wcbox_baum') ?></option>
						<option value="2" <?= selected($instance[$key], 2) ?>>2</option>
						<option value="3" <?= selected($instance[$key], 3) ?>>3</option>
						<option value="4" <?= selected($instance[$key], 4) ?>>4</option>
					</select>
				</label>
			</p>
		<?php endforeach; ?>
		<p>
			<label for="<?php echo $this->get_field_id('wcbox_extra_id'); ?>"><?php echo __('Extra ID', 'wcbox_baum') ?>
				
				<input type="text" name="<?php echo $this->get_field_name('wcbox_extra_id'); ?>" id="<?php echo $this->get_field_id('wcbox_extra_id'); ?>" value="<?php echo $wcbox_extra_id ?>" readonly>
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('wcbox_override_title'); ?>"><?php echo __('Sobreescribir título', 'wcbox_baum') ?>
				
				<input type="text" name="<?php echo $this->get_field_name('wcbox_override_title'); ?>" id="<?php echo $this->get_field_id('wcbox_override_title'); ?>" value="<?php echo $wcbox_override_title ?>" style="width: 100%;">
			</label>
		</p>
		<p>
			<?php
			echo '<a href="post.php?post=' . $wcbox_post_id . '&action=edit">' . __( 'Edit Carousel', 'wcbox_baum' ) . '</a>' ;
			?>
		</p>
		<input type="hidden" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?= !empty($widget_extra_title) ? $widget_extra_title : '' ?>" />
		<?php 
	}

	public function columns_array() {
		$columns = array(
			'wcbox_cols_d' => __('Columns Desktop', 'wcbox_baum'),
			'wcbox_cols_t' => __('Columns Tablet', 'wcbox_baum'),
			'wcbox_cols_m' => __('Columns Mobile', 'wcbox_baum')
		);

		return $columns;
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['wcbox_post_id'] = ( ! empty( $new_instance['wcbox_post_id'] ) ) ? strip_tags( $new_instance['wcbox_post_id'] ) : '';
		$instance['wcbox_extra_id'] = ( ! empty( $new_instance['wcbox_extra_id'] ) ) ? strip_tags( $new_instance['wcbox_extra_id'] ) : '';
		$instance['wcbox_override_title'] = ( ! empty( $new_instance['wcbox_override_title'] ) ) ? strip_tags( $new_instance['wcbox_override_title'] ) : '';

		foreach ($this->columns_array() as $key => $column) {
			$instance[$key] = ($new_instance[$key] != -1) ? strip_tags($new_instance[$key]) : '-1';
		}

		return $instance;
	}

	// Creating widget front-end
	public function widget( $args, $instance ) {
		$params = array();
		$params[] = !empty($instance['wcbox_post_id']) ? 'id="' . esc_attr($instance['wcbox_post_id']) . '"' : '';
		$params[] = !empty($instance['wcbox_extra_id']) ? 'extra_id="' . esc_attr($instance['wcbox_extra_id']) . '"' : '';
		$params[] = !empty($instance['wcbox_override_title']) ? 'title="' . esc_attr($instance['wcbox_override_title']) . '"' : '';

		foreach ($this->columns_array() as $key => $column) {
			$params[] = ($instance[$key] != -1) ? str_replace('wcbox_', '', $key) . '="' . esc_attr($instance[$key]) . '"' : '';
		}

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		echo do_shortcode('[wcbox_slider ' . implode(' ', $params) . ']');

		echo $args['after_widget'];
	}
}