<?php
/**
 * Adds new shortcode "baum-card-shortcode" and registers it to
 * the WPBakery Visual Composer plugin
 */

// If this file is called directly, abort
if ( ! defined( 'ABSPATH' ) ) {
	die ( 'Silly human what are you doing here' );
}

if ( ! class_exists( 'wbBaumCard' ) ) {
	class wbBaumCard {
		/**
		 * Main constructor
		 */
		public function __construct() {
			// Registers the shortcode in WordPress
			add_shortcode( 'baum-card-shortcode', array( 'wbBaumCard', 'output' ) );

			// Map shortcode to Visual Composer
			if ( function_exists( 'vc_lean_map' ) ) {
				vc_lean_map( 'baum-card-shortcode', array( 'wbBaumCard', 'map' ) );
			}
		}

		/**
		 * Map shortcode to VC
		 *
		 * This is an array of all your settings which become the shortcode attributes ($atts)
		 * for the output.
		 */
		public static function map() {
			return array(
				'name'        => esc_html__( 'Card', 'wb-baum' ),
				'description' => esc_html__( 'Add new card', 'wb-baum' ),
				'base'        => 'wb_baum_card',
				'category'    => __( 'Baum VC', 'wb-baum' ),
				'icon'        => plugin_dir_url( __FILE__ ) . 'img/card.png',
				'params'      => array(
					array(
						'type'        => 'attach_image',
						'holder'      => 'img',
						'heading'     => __( 'Image', 'wb-baum' ),
						'param_name'  => 'headerimg',
						// 'value' => __( 'Default value', 'wb-baum' ),
						'admin_label' => false,
						'weight'      => 0,
						'group'       => 'Card',
					),
					array(
						'type'        => 'textfield',
						'holder'      => 'h3',
						'class'       => 'title-class',
						'heading'     => __( 'Card Title', 'wb-baum' ),
						'param_name'  => 'title',
						'value'       => __( '', 'wb-baum' ),
						'admin_label' => false,
						'weight'      => 0,
						'group'       => 'Card',
					),
					array(
						'type'        => 'textarea_html',
						'holder'      => 'div',
						'class'       => 'wb-baum-text-class',
						'heading'     => __( 'Description', 'wb-baum' ),
						'param_name'  => 'content',
						'value'       => __( '', 'wb-baum' ),
						'description' => __( 'To add link highlight text or url and click the chain to apply hyperlink', 'wb-baum' ),
						// 'admin_label' => false,
						// 'weight' => 0,
						'group'       => 'Card',
					),
					array(
						'type'        => 'vc_link',
						'class'       => 'wb-baum-link-class',
						'heading'     => __( 'Link', 'wb-baum' ),
						'param_name'  => 'link',
						'value'       => '',
						'description' => __( 'Link to:', 'wb-baum' ),
						'group'       => 'Card',
					),
				),
			);
		}

		/**
		 * Shortcode output
		 */
		public static function output( $atts, $content = null ) {
			extract(
				shortcode_atts(
					array(
						'headerimg' => 'headerimg',
						'title'     => '',
						'link'      => ''
					),
					$atts
				)
			);

			$img_url = wp_get_attachment_image_src( $headerimg, [250, 160] );
			$url_link = vc_build_link( $link );

			// Fill $html var with data
			ob_start();
			?>
            <div class="wb-baum-card-wrap">
	            <?php if ( '' !== $url_link['url'] ) : ?>
                    <a href="<?php echo $url_link['url']; ?>" title="<?php echo $url_link['title']; ?>">
				<?php endif; ?>

                    <?php if ( $img_url ): ?>
                        <img class="wb-baum-card-image img-responsive" src="<?php echo esc_url( $img_url[0] ); ?>">
                    <?php endif; ?>

                    <?php if ( $title ): ?>
                        <h3 class="wb-baum-card-title"><?php echo esc_html( $title ); ?></h3>
                    <?php endif; ?>

                    <?php if ( $content ): ?>
                        <div class="wb-baum-card-text"><?php echo $content; ?> </div>
                    <?php endif; ?>

	            <?php if ( '' !== $url_link['url'] ) : ?>
                    </a>
	            <?php endif; ?>
            </div>
			<?php

			return ob_get_clean();
		}
	}
}

new wbBaumCard;
