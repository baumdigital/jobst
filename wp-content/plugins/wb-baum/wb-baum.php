<?php
/*
Plugin Name: Baum Wordpress Bakery
Plugin URI: https://baum.digital/
Description: An extension for Wordpress Bakery that display custom elements
Author: River Martínez
Version: 1.0.0
Author URI: https://baum.digital/
*/

// If this file is called directly, abort
if ( ! defined( 'ABSPATH' ) ) {
	die ( 'Silly human what are you doing here' );
}

// Before VC Init
add_action( 'vc_before_init', 'wb_baum_before_init_actions' );

function wb_baum_before_init_actions() {
	// Require new custom Element
	include( plugin_dir_path( __FILE__ ) . 'wb-baum-card-element.php' );
}

// Link directory stylesheet
function wb_baum_card_scripts() {
	wp_enqueue_style( 'wb_baum_card_stylesheet', plugin_dir_url( __FILE__ ) . 'css/card/card-element.css' );
}

add_action( 'wp_enqueue_scripts', 'wb_baum_card_scripts' );

// Link directory stylesheet admin
function wb_baum_card_admin_scripts() {
	wp_enqueue_style( 'wb_baum_card_stylesheet', plugin_dir_url( __FILE__ ) . 'css/card/admin-card-element.css' );
}

add_action( 'admin_enqueue_scripts', 'wb_baum_card_admin_scripts' );